//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 hohojo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <objc/message.h>
#import "BaseViewController.h"

@interface Finishprofilehavelisting : BaseViewController

@property(strong, nonatomic) IBOutlet UIImageView *listingcoverphoto;
@property(strong, nonatomic) IBOutlet UILabel *price;
@property(strong, nonatomic) IBOutlet UILabel *size;
@property(strong, nonatomic) IBOutlet UIButton *updatebutton;

@property(strong, nonatomic) IBOutlet UILabel *productname;
@property(strong, nonatomic) IBOutlet UILabel *likecount;

@end
