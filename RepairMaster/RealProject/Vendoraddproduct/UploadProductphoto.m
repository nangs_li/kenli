//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 hohojo. All rights reserved.
//

#import "Collectionviewcell.h"
#import "UploadProductphoto.h"
#import "addProductinfo.h"

@interface UploadProductphoto ()

@end
@implementation UploadProductphoto

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:YES];

  if (self.chosenImages == nil) {
    // [self.nextbutton  setTintColor:self.delegate.Greycolor];

    self.nextbutton.enabled = NO;

  } else {
    //[self.nextbutton  setTintColor:self.delegate.Greencolor];
    self.nextbutton.enabled = YES;
  }

  [self.CollectionView reloadData];
}

- (void)viewDidLoad {
  [super viewDidLoad];

  [self.CollectionView registerClass:[Collectionviewcell class]
          forCellWithReuseIdentifier:@"Collectionviewcell"];
  self.collectionviewdata = [[NSMutableArray alloc]
      initWithObjects:@"0", @"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8",
                      @"9", nil];

  self.CollectionView.scrollEnabled = NO;

  [self.CollectionView reloadData];

  self.nextbutton = [[UIBarButtonItem alloc]
      initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                           target:self
                           action:@selector(saveAction:)];

  self.navigationItem.rightBarButtonItem = self.nextbutton;
  self.navigationItem.title = @"photo";
}
#pragma mark - saveAction

- (void)saveAction:(id)sender {
  addProductinfo *UIViewController =
      [[addProductinfo alloc] initWithNibName:@"addProductinfo" bundle:nil];

  [self.navigationController pushViewController:UIViewController animated:YES];

  [AddProductByEidModel shared].chosenImages = self.chosenImages;
}

#pragma mark - UICollectionViewDataSource methods

- (NSInteger)collectionView:(UICollectionView *)theCollectionView
     numberOfItemsInSection:(NSInteger)theSectionIndex {
  return self.collectionviewdata.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  //   DDLogVerbose(@"collectionView
  //   cellForItemAtIndexPath%ld",(long)indexPath.row
  //   );

  Collectionviewcell *collectionView2 = (Collectionviewcell *)[collectionView
      dequeueReusableCellWithReuseIdentifier:@"Collectionviewcell"
                                forIndexPath:indexPath];

  collectionView2.layer.borderWidth = 1.0f;
  collectionView2.layer.borderColor = self.delegate.Greycolor.CGColor;
#pragma mark -   deletebutton
  [collectionView2.playingCardImageView
      setImage:self.chosenImages[indexPath.row]];

  collectionView2.playingCardImageView.contentMode =
      UIViewContentModeScaleAspectFill;
  collectionView2.playingCardImageView.clipsToBounds = YES;

  collectionView2.deletebutton.frame =
      CGRectMake(collectionView2.frame.size.width - 34, 0, 34, 34);
  [collectionView2.deletebutton addTarget:self
                                   action:@selector(RemovePrssd:)
                         forControlEvents:UIControlEventTouchUpInside];

  // collectionView2.addbutton.hidden=YES;
  if (self.chosenImages == nil) {
    if (indexPath.row == 0) {
      collectionView2.layer.borderColor = [UIColor colorWithRed:(60 / 255.f)
                                                          green:(218 / 255.f)
                                                           blue:(140 / 255.f)
                                                          alpha:1.0]
                                              .CGColor;
      collectionView2.cover.hidden = NO;
      [collectionView2.cover addTarget:self
                                action:@selector(CoverPrssd:)
                      forControlEvents:UIControlEventTouchUpInside];
    }
  } else {
    if ([self.chosenImages[indexPath.row]
            isEqual:[UIImage imageNamed:@"emptyphoto"]] ||
        [self.chosenImages[indexPath.row]
            isEqual:[UIImage imageNamed:@"collectionviewaddbutton"]]) {
      collectionView2.deletebutton.hidden = YES;
    } else {
      collectionView2.deletebutton.hidden = NO;
    }
  }

  return collectionView2;
}

- (void)collectionView:(UICollectionView *)collectionView
    didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
  DDLogVerbose(@"collectionViewindexPath=%@", indexPath);

  if ([self.chosenImages[indexPath.row]
          isEqual:[UIImage imageNamed:@"collectionviewaddbutton"]]) {
    DDLogVerbose(@"collectionviewaddbutton");
    [self CoverPrssd:self];
  }
}
#pragma mark -     RemovePrssd or CoverPrssd or UploadphotoPrssd
- (void)RemovePrssd:(id)sender {
  DDLogVerbose(@"before remove prssed self.chosenImages---->%@",
               self.chosenImages);
  UIView *senderButton = (UIView *)sender;
  Collectionviewcell *cell =
      (Collectionviewcell *)[[senderButton superview] superview];
  NSIndexPath *indexPath = [self.CollectionView
      indexPathForCell:(UICollectionViewCell *)[[senderButton superview]
                           superview]];

  [self.chosenImages replaceObjectAtIndex:indexPath.row
                               withObject:[UIImage imageNamed:@"emptyphoto"]];

  [cell.playingCardImageView setImage:[UIImage imageNamed:@"emptyphoto"]];
  cell.deletebutton.hidden = YES;
  DDLogVerbose(@"after remove prssed self.chosenImages---->%@",
               self.chosenImages);
}
- (void)CoverPrssd:(id)sender {
  self.CollectionViewAddbuttonexist = NO;

  DDLogVerbose(@"CoverPrssd");

  ELCImagePickerController *elcPicker =
      [[ELCImagePickerController alloc] initImagePicker];

  elcPicker.maximumImagesCount =
      10;  // Set the maximum number of images to select to 100
  elcPicker.returnsOriginalImage =
      YES;  // Only return the fullScreenImage, not the fullResolutionImage
  elcPicker.returnsImage = YES;  // Return UIimage if YES. If NO, only return
                                 // asset location information
  elcPicker.onOrder = YES;  // For multiple image selection, display and return
                            // order of selected images
  elcPicker.mediaTypes = @[
    (NSString *)kUTTypeImage,
    (NSString *)kUTTypeMovie
  ];  // Supports image and movie types

  elcPicker.imagePickerDelegate = self;

  CATransition *transition = [CATransition animation];
  transition.duration = ANIMATION_DURATION;
  transition.type = kCATransitionPush;
  transition.subtype = kCATransitionFromLeft;

  [self.navigationController.view.layer addAnimation:transition
                                              forKey:kCATransition];

  [self presentViewController:elcPicker animated:YES completion:nil];
}

#pragma mark - LXReorderableCollectionViewDelegateFlowLayout methods

- (void)collectionView:(UICollectionView *)collectionView
                              layout:
                                  (UICollectionViewLayout *)collectionViewLayout
    willBeginDraggingItemAtIndexPath:(NSIndexPath *)indexPath {
  NSLog(@"will begin drag");
}

- (void)collectionView:(UICollectionView *)collectionView
                             layout:
                                 (UICollectionViewLayout *)collectionViewLayout
    didBeginDraggingItemAtIndexPath:(NSIndexPath *)indexPath {
  NSLog(@"did begin drag");
}

- (void)collectionView:(UICollectionView *)collectionView
                            layout:
                                (UICollectionViewLayout *)collectionViewLayout
    willEndDraggingItemAtIndexPath:(NSIndexPath *)indexPath {
  NSLog(@"will end drag");
}

- (void)collectionView:(UICollectionView *)collectionView
                           layout:(UICollectionViewLayout *)collectionViewLayout
    didEndDraggingItemAtIndexPath:(NSIndexPath *)indexPath {
  NSLog(@"did end drag");
  [self.CollectionView reloadData];
}

#pragma mark - LXReorderableCollectionViewDataSource methods

- (void)collectionView:(UICollectionView *)collectionView
       itemAtIndexPath:(NSIndexPath *)fromIndexPath
   willMoveToIndexPath:(NSIndexPath *)toIndexPath {
  DDLogVerbose(@"before moveing self.chosenImages%@", self.chosenImages);
  DDLogVerbose(@"fromIndexPath.item%ld", (long)fromIndexPath.item);
  DDLogVerbose(@"toIndexPath.item%ld", (long)toIndexPath.item);

  UIImage *tempimage = self.chosenImages[fromIndexPath.item];
  [self.chosenImages removeObjectAtIndex:(long)fromIndexPath.item];

  [self.chosenImages insertObject:tempimage atIndex:toIndexPath.item];

  DDLogVerbose(@"after moveing self.chosenImages%@", self.chosenImages);
}

- (BOOL)collectionView:(UICollectionView *)collectionView
canMoveItemAtIndexPath:(NSIndexPath *)indexPath {
  return YES;
}

- (BOOL)collectionView:(UICollectionView *)collectionView
       itemAtIndexPath:(NSIndexPath *)fromIndexPath
    canMoveToIndexPath:(NSIndexPath *)toIndexPath {
  return YES;
}

#pragma mark ELCImagePickerControllerDelegate Methods

- (void)elcImagePickerController:(ELCImagePickerController *)picker
   didFinishPickingMediaWithInfo:(NSArray *)info {
  [self dismissViewControllerAnimated:NO completion:nil];

  NSLog(@"info%@", info);
  NSMutableArray *images = [[NSMutableArray alloc] initWithCapacity:9];
  for (NSDictionary *dict in info) {
    if ([dict objectForKey:UIImagePickerControllerMediaType] ==
        ALAssetTypePhoto) {
      if ([dict objectForKey:UIImagePickerControllerOriginalImage]) {
        UIImage *image =
            [dict objectForKey:UIImagePickerControllerOriginalImage];
        [images addObject:image];

      } else {
        DDLogVerbose(@"UIImagePickerControllerReferenceURL = %@", dict);
      }
    } else if ([dict objectForKey:UIImagePickerControllerMediaType] ==
               ALAssetTypeVideo) {
      if ([dict objectForKey:UIImagePickerControllerOriginalImage]) {
        UIImage *image =
            [dict objectForKey:UIImagePickerControllerOriginalImage];

        [images addObject:image];

      } else {
        DDLogVerbose(@"UIImagePickerControllerReferenceURL = %@", dict);
      }
    } else {
      DDLogVerbose(@"Uknown asset type");
    }
  }

  while (images.count < 10) {
    if (!self.CollectionViewAddbuttonexist) {
      [images addObject:[UIImage imageNamed:@"collectionviewaddbutton"]];

      self.CollectionViewAddbuttonexist = YES;
    } else {
      [images addObject:[UIImage imageNamed:@"emptyphoto"]];
    }
  }

  self.chosenImages = images;
  DDLogVerbose(@"chosenImages-->%@", self.chosenImages);
}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker {
  [self dismissViewControllerAnimated:NO completion:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:
    (UIInterfaceOrientation)toInterfaceOrientation {
  if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
    return YES;
  } else {
    return toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown;
  }
}

#pragma mark - Button
- (IBAction)btnBackpressed:(id)sender {
  [self btnBackPressed:sender];
}

@end