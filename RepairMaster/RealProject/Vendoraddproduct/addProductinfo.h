//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 hohojo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <objc/message.h>
#import "BaseViewController.h"

#import "TDDatePickerController.h"
#import "TDPicker.h"
@interface addProductinfo
    : BaseViewController<UITextFieldDelegate, UITextViewDelegate,
                         TDPickerDelegate>

#pragma mark house information part2
@property(strong, nonatomic) IBOutlet UITextField *price;
//@property (strong, nonatomic) IBOutlet UITextField *size;
//@property (strong, nonatomic) IBOutlet TDDatePickerController* datePickerView;
@property(strong, nonatomic) IBOutlet TDPicker *picker;

@property(strong, nonatomic) IBOutlet UITextField *phone;
@property(strong, nonatomic) IBOutlet UITextView *ProductDescription;
@property(strong, nonatomic) IBOutlet UITextField *advname;
@property(strong, nonatomic) NSDate *selectedDate;
@property(strong, nonatomic) IBOutlet UIButton *typebutton;

@property(strong, nonatomic) UIBarButtonItem *nextbutton;
@end
