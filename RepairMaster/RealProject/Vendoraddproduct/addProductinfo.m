//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 hohojo. All rights reserved.
//

#import "AFURLRequestSerialization.h"
#import "AFURLSessionManager.h"
#import "AddProductByEidModel.h"
#import "AddTermFromProductByEidModel.h"
#import "DBProductListPhoto.h"
#import "ListTermClass.h"
#import "ListTermModel.h"
#import "UploadProductphoto.h"
#import "addProductinfo.h"
@interface addProductinfo ()

@end
@implementation addProductinfo

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNi {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNi];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  _picker.pickerData = [[NSMutableArray alloc] init];
}
- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];

  [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:YES];
  self.price.delegate = self;

  self.price.delegate = self;
  self.advname.delegate = self;
  self.phone.delegate = self;

  self.ProductDescription.delegate = self;

  [[ListTermModel shared] listTerms:@"1"
      forPage:1
      success:^(NSMutableArray *serverReturnTermArray) {
        for (ListTermClass *listTermClass in serverReturnTermArray) {
          [_picker.pickerData addObject:listTermClass.name];
        }
        _picker.selectpickerdata = _picker.pickerData[0];
        [_picker.picker reloadAllComponents];
        if (serverReturnTermArray.count == 10) {
          [self listTerms:@"1" forPage:2];
        }

      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error,
                NSString *errorMessage, int errorStatus, NSString *alert,
                NSString *ok) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alert
                                                            message:errorMessage
                                                           delegate:nil
                                                  cancelButtonTitle:ok
                                                  otherButtonTitles:nil];
        [alertView show];

      }];

  self.picker.delegate = self;

  _picker.selectindex = 0;
}
- (void)viewDidLoad {
  [super viewDidLoad];
#pragma mark button border setting

  self.nextbutton = [[UIBarButtonItem alloc]
      initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                           target:self
                           action:@selector(saveAction:)];

  self.navigationItem.rightBarButtonItem = self.nextbutton;
  self.navigationItem.title = @"product detail";
  [self checkempty];
}
- (void)checkempty {
  if ([self.price.text isEqualToString:@""] ||
      [self.typebutton.titleLabel.text isEqualToString:@"類型"] ||
      [self.phone.text isEqualToString:@""] ||
      [self.advname.text isEqualToString:@""] ||
      [self.ProductDescription.text isEqualToString:@""]) {
    self.nextbutton.enabled = NO;

  } else {
    self.nextbutton.enabled = YES;
  }
}

#pragma marks -     \
    Call Server APi \
        Method-- ------------------------------------------------------------ -

- (void)listTerms:(NSString *)termID forPage:(int)page {
  [[ListTermModel shared] listTerms:termID
      forPage:page
      success:^(NSMutableArray *serverReturnTermArray) {

        for (ListTermClass *listTerm in serverReturnTermArray) {
          [_picker.pickerData addObject:listTerm.name];
        }
        [_picker.picker reloadAllComponents];

      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error,
                NSString *errorMessage, int errorStatus, NSString *alert,
                NSString *ok) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alert
                                                            message:errorMessage
                                                           delegate:nil
                                                  cancelButtonTitle:ok
                                                  otherButtonTitles:nil];
        [alertView show];

      }];
}
- (void)saveAction:(id)sender {
  self.nextbutton.enabled = NO;
  [AddProductByEidModel shared].selectedpickerData =
      _picker.pickerData[[_picker.picker selectedRowInComponent:0]];
  [AddProductByEidModel shared].selectedpickerrow =
      [_picker.picker selectedRowInComponent:0];
  self.nextbutton.enabled = NO;
  [[AddProductByEidModel shared]
      AddProductByEid:[[LoginRecordModel shared].myLoginRecord.eid stringValue]
      productDescription:self.ProductDescription.text
      phone:self.phone.text
      price:self.price.text
      advname:self.advname.text
      success:^(ProductDetailClass *myProductDetail) {

        [self addTermFromProductByEid:[[LoginRecordModel shared]
                                              .myLoginRecord.eid stringValue]];

      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error,
                NSString *errorMessage, int errorStatus, NSString *alert,
                NSString *ok) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alert
                                                            message:errorMessage
                                                           delegate:nil
                                                  cancelButtonTitle:ok
                                                  otherButtonTitles:nil];
        [alertView show];

      }];
}
- (void)addTermFromProductByEid:(NSString *)eid {
  [[AddTermFromProductByEidModel shared] AddTermFromProductByEid:eid
      success:^(ProductDetailClass *myProductDetail) {

        [self uploadAddProductPartChosenImagesToServer];

      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error,
                NSString *errorMessage, int errorStatus, NSString *alert,
                NSString *ok) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alert
                                                            message:errorMessage
                                                           delegate:nil
                                                  cancelButtonTitle:ok
                                                  otherButtonTitles:nil];
        [alertView show];

      }];
}
- (void)uploadAddProductPartChosenImagesToServer {
#pragma uploadphoto

  for (int i = 0; i < [[AddProductByEidModel shared].chosenImages count]; i++) {
    if ([[AddProductByEidModel shared]
                .chosenImages[i] isEqual:[UIImage imageNamed:@"emptyphoto"]] ||
        [[AddProductByEidModel shared]
                .chosenImages[i]
            isEqual:[UIImage imageNamed:@"collectionviewaddbutton"]]) {
      DDLogVerbose(@"emptyphoto");

    } else {
      DDLogVerbose(@"callmultipartfileupload");

      NSDictionary *parameters = @{
        @"eid" : [AddProductByEidModel shared].myProductDetail.eid,
        @"name" : [NSString
            stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]]
      };
      self.hud.labelText = @"loading";
      [self loadingAndStopLoadingAfterSecond:8];
      NSString *string = [NSString
          stringWithFormat:@"http://202.181.187.202:8080/advonews/fm/upload"];

      NSData *imageData = UIImageJPEGRepresentation(
          [AddProductByEidModel shared].chosenImages[i], 1);
      double j = 1;
      DDLogVerbose(@"imageData.length-->%lu", (unsigned long)imageData.length);
      while (imageData.length > 224000) {
        j = j - 0.05;
        imageData = UIImageJPEGRepresentation(
            [AddProductByEidModel shared].chosenImages[i], j);
      }
      DDLogVerbose(@"imageData.length-->%lu", (unsigned long)imageData.length);

      //    DDLogVerbose(@"parameters-->%@",parameters);
      NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer]
          multipartFormRequestWithMethod:@"POST"
                               URLString:string
                              parameters:parameters
               constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                 [formData appendPartWithFileData:imageData
                                             name:@"file"
                                         fileName:@"file"
                                         mimeType:@"image/jpeg"];
               }
                                   error:nil];

      AFURLSessionManager *manager = [[AFURLSessionManager alloc]
          initWithSessionConfiguration:[NSURLSessionConfiguration
                                           defaultSessionConfiguration]];
      NSProgress *progress = nil;

      NSURLSessionUploadTask *uploadTask = [manager
          uploadTaskWithStreamedRequest:request
                               progress:&progress
                      completionHandler:^(NSURLResponse *response,
                                          id responseObject, NSError *error) {
                        [MBProgressHUD hideHUDForView:self.view animated:YES];

                        [self.navigationController
                            popToRootViewControllerAnimated:YES];

                      }];

      [uploadTask resume];
    }
  }
}
#pragma marks -            \
    Call Server APi Method \
        Delegate-- ------------------------------------------------------------ -

#pragma marks - \
    textfield   \
        Delegate-- ------------------------------------------------------------ -
- (void)textFieldDidBeginEditing:(UITextField *)textField {
  [self dismissSemiModalViewController:_picker];
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
  return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
  DDLogVerbose(@"textFieldDidEndEditing");
  [self checkempty];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
  [textField resignFirstResponder];

  return NO;
}
#pragma mark textview
- (void)textViewDidBeginEditing:(UITextView *)textView {
  [self dismissSemiModalViewController:_picker];
}
- (void)textViewDidEndEditing:(UITextView *)textView {
  NSLog(@"textViewDidBeginEditing:");

  [self checkempty];
}
#pragma marks - \
    textfield   \
        Delegate-- ------------------------------------------------------------ -

#pragma mark -TDPicker Delegate------------------------------------------------------------------
- (IBAction)btntypepressed:(id)sender {
  [self.price resignFirstResponder];
  [self.ProductDescription resignFirstResponder];
  [self.phone resignFirstResponder];
  [self.advname resignFirstResponder];

  [_picker.picker reloadAllComponents];
  [self presentSemiModalViewController:_picker];
}
- (void)picker:(UIPickerView *)picker
  didSelectRow:(NSInteger)row
   inComponent:(NSInteger)component {
  _picker.selectpickerdata = _picker.pickerData[row];
  _picker.selectindex = [_picker.picker selectedRowInComponent:0];

  DDLogVerbose(@"_picker.selectpickerdata-->%@", _picker.pickerData[row]);
  DDLogVerbose(@"[_picker.picker selectedRowInComponent:0]-->%ld",
               (long)[_picker.picker selectedRowInComponent:0]);
}
- (void)pickerSetDate:(TDPicker *)viewController {
  DDLogVerbose(@"pickerSetDate");
  [self dismissSemiModalViewController:_picker];

  _picker.selectindex = [_picker.picker selectedRowInComponent:0];

  [self.typebutton setTitle:_picker.selectpickerdata
                   forState:UIControlStateNormal];
  self.typebutton.tag = _picker.selectindex;

  self.typebutton.titleLabel.text = _picker.selectpickerdata;
  [self checkempty];
}
- (void)pickerClearDate:(TDPicker *)viewController {
  [self dismissSemiModalViewController:_picker];

  _selectedDate = nil;
}

- (void)pickerCancel:(TDPicker *)viewController {
  [self dismissSemiModalViewController:_picker];
}
#pragma mark -TDPicker Delegate------------------------------------------------------------------

@end