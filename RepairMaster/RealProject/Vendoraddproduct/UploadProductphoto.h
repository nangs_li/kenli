//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 hohojo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>
#import <objc/message.h>
#import "BaseViewController.h"
#import "ELCImagePickerHeader.h"
#import "LXReorderableCollectionViewFlowLayout.h"
@interface UploadProductphoto
    : BaseViewController<LXReorderableCollectionViewDataSource,
                         LXReorderableCollectionViewDelegateFlowLayout,
                         ELCImagePickerControllerDelegate>

#pragma mark upload photo collectionview
@property(nonatomic, retain) NSMutableArray *collectionviewdata;
@property(nonatomic, retain) NSMutableArray *tableData;
@property(strong, nonatomic) IBOutlet UICollectionView *CollectionView;
@property(nonatomic, assign) BOOL CollectionViewAddbuttonexist;
@property(nonatomic, retain) NSIndexPath *checkedCell;
@property(nonatomic, retain) NSMutableArray *chosenImages;

@property(strong, nonatomic) UIBarButtonItem *nextbutton;
@end
