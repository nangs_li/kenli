//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 hohojo. All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>
#import "Finishprofilehavelisting.h"

#import "DBProductDetail.h"
#import "UploadProductphoto.h"
#import "VendorSignupPage.h"
#import "VendorlistProductsByEidModel.h"
@interface Finishprofilehavelisting ()
@end
@implementation Finishprofilehavelisting

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
  }
  return self;
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];

  self.navigationItem.title = @"Upload Advertisements";

  self.navigationItem.hidesBackButton = YES;

  if (![self IsEmpty:[AddProductByEidModel shared].myProductDetail] &&
      ![self IsEmpty:[LoginRecordModel shared].myLoginRecord.eid]) {
    [self vendorListProductsByEid];
  }

#pragma mark set data From [AddProductByEidModel shared].myProductDetail
  [self setUpDataFromAddProductByEidModel];
  [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:NO];
  DDLogVerbose(@" [AddProductByEidModel shared].myProductDetail-->%@",
               [[AddProductByEidModel shared].myProductDetail toJSONString]);
}
- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
}

- (void)viewDidLoad {
  //[self performSelector:@selector(layout) withObject:nil afterDelay:2.0];
  [super viewDidLoad];
}

- (IBAction)updloadAdvPressed:(id)sender {
#pragma mark setup DBLoginRecord from db

  DBResultSet *r = [[[DBLoginRecord query] limit:1] fetch];

  if (r.count == 0) {
    VendorSignupPage *UIViewController =
        [[VendorSignupPage alloc] initWithNibName:@"VendorSignupPage"
                                           bundle:nil];

    [self.navigationController pushViewController:UIViewController
                                         animated:YES];

  } else {
    UploadProductphoto *UIViewController =
        [[UploadProductphoto alloc] initWithNibName:@"UploadProductphoto"
                                             bundle:nil];

    [self.navigationController pushViewController:UIViewController
                                         animated:YES];
  }
}
- (void)vendorListProductsByEid {
  [[VendorlistProductsByEidModel shared]
      VendorlistProductsByEid:[[LoginRecordModel shared]
                                      .myLoginRecord.eid stringValue]
      success:^(NSMutableArray *serverReturnTermArray) {
        [self setUpDataFromAddProductByEidModel];

      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error,
                NSString *errorMessage, int errorStatus, NSString *alert,
                NSString *ok) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alert
                                                            message:errorMessage
                                                           delegate:nil
                                                  cancelButtonTitle:ok
                                                  otherButtonTitles:nil];
        [alertView show];

      }];
}
- (void)setUpDataFromAddProductByEidModel {
  if (![self IsEmpty:[AddProductByEidModel shared].myProductDetail.price]) {
    ProductListPhoto *firstPhotoFromProductPhotoList =
        [AddProductByEidModel shared].myProductDetail.photos.firstObject;
    self.price.text = [NSString
        stringWithFormat:@"價錢: %@",
                         [AddProductByEidModel shared].myProductDetail.price];
    if ([AddProductByEidModel shared].myProductDetail.terms.firstObject ==
        nil) {
      self.size.text = [NSString stringWithFormat:@"類別"];
    } else {
      NSDictionary *termsfirstdict =
          [AddProductByEidModel shared].myProductDetail.terms.firstObject;
      NSNumber *tid = [termsfirstdict objectForKey:@"tid"];

      if ([tid intValue] == 9) {
        self.size.text = [NSString stringWithFormat:@"類別: 裝修"];
      }
      if ([tid intValue] == 10) {
        self.size.text = [NSString stringWithFormat:@"類別: 冷氣維修"];
      }
    }
    self.productname.text =
        [NSString stringWithFormat:@"廣告名稱: %@",
                                   [AddProductByEidModel shared]
                                       .myProductDetail.qualification];
    self.likecount.text = [[[AddProductByEidModel shared]
                                .myProductDetail.likeCount
        objectForKeyedSubscript:@"count"] stringValue];

    [self.listingcoverphoto
         setImageWithURL:[NSURL
                             URLWithString:firstPhotoFromProductPhotoList.url]
        placeholderImage:nil];
  }
}

@end