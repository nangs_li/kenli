//
//  PlayingCardCell.m
//  LXRCVFL Example using Storyboard
//
//  Created by Stan Chang Khin Boon on 3/10/12.
//  Copyright (c) 2012 d--buzz. All rights reserved.
//

#import "Collectionviewcell.h"

@implementation Collectionviewcell

@synthesize deletebutton, playingCardImageView;

- (id)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    // Initialization code
    NSArray *arrayOfViews =
        [[NSBundle mainBundle] loadNibNamed:@"Collectionviewcell"
                                      owner:self
                                    options:nil];

    if ([arrayOfViews count] < 1) {
      return nil;
    }

    if (![[arrayOfViews objectAtIndex:0]
            isKindOfClass:[UICollectionViewCell class]]) {
      return nil;
    }

    self = [arrayOfViews objectAtIndex:0];
  }

  return self;
}

#pragma mark setGreenHighlighted
- (void)setHighlighted:(BOOL)highlighted {
  [super setHighlighted:highlighted];
  self.playingCardImageView.alpha = highlighted ? 0.75f : 1.0f;
}

@end
