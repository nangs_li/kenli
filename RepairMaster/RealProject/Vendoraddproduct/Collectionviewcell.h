//
//  PlayingCardCell.h
//  LXRCVFL Example using Storyboard
//
//  Created by Stan Chang Khin Boon on 3/10/12.
//  Copyright (c) 2012 d--buzz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Collectionviewcell : UICollectionViewCell

@property(strong, nonatomic) IBOutlet UIImageView *playingCardImageView;
@property(strong, nonatomic) IBOutlet UIButton *deletebutton;
@property(strong, nonatomic) IBOutlet UIButton *cover;

@end
