//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 hohojo. All rights reserved.
//

#import "Productfindbytermpage.h"

#import "BaseViewController.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import "DBLoginRecord.h"
#import "FindProductbyTaxModel.h"
#import "ProductDetailCell.h"
#import "ProductDetailClass.h"
#import "ProductDetailPage.h"
@interface Productfindbytermpage ()

@end
@implementation Productfindbytermpage

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];

  [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:NO];

  // [[[AppDelegate getAppDelegate]getTabBarController]setTabBarHidden:NO];
}
- (void)getProductFindByTermFromServer {
  self.ProductDetailClassArray = [[NSMutableArray alloc] init];
  [[FindProductbyTaxModel shared]
      findProductbyTaxModelBytaxId:[NSString
                                       stringWithFormat:@"%ld",
                                                        (long)self.buttonterm]
      success:^(NSMutableArray *productDetailClass) {
        self.ProductDetailClassArray = productDetailClass;
        self.tableview.hidden = NO;

        [self.tableview reloadData];
      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error,
                NSString *errorMessage, int errorStatus, NSString *alert,
                NSString *ok) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alert
                                                            message:errorMessage
                                                           delegate:nil
                                                  cancelButtonTitle:ok
                                                  otherButtonTitles:nil];
        [alertView show];

      }];
}
- (void)viewDidLoad {
  [super viewDidLoad];

  self.tableview.delegate = self;
  self.tableview.dataSource = self;

  [self getProductFindByTermFromServer];
  self.navigationItem.title = @"Product";
}

#pragma tableivewdelegate

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section

{
  return self.ProductDetailClassArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
  static NSString *simpleTableIdentifier = @"ProductDetailCell";

  ProductDetailCell *cell =
      [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

  if (cell == nil) {
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ProductDetailCell"
                                                 owner:self
                                               options:nil];
    cell = (ProductDetailCell *)[nib objectAtIndex:0];
  }
  ProductDetailClass *ProductDetailClass =
      [self.ProductDetailClassArray objectAtIndex:indexPath.row];

  cell.selectionStyle = UITableViewCellSelectionStyleNone;
  cell.price.text =
      [NSString stringWithFormat:@"$%@", ProductDetailClass.price];
  if (self.buttonterm == 9) {
    cell.type.text = [NSString stringWithFormat:@"類別: %@", @"裝修"];
  } else {
    cell.type.text = [NSString stringWithFormat:@"類別: %@", @"冷氣維修"];
  }
  cell.productname.text = [NSString
      stringWithFormat:@"廣告名稱: %@", ProductDetailClass.qualification];
  NSArray *photos = ProductDetailClass.photos;
  if (![self IsEmpty:photos]) {
    ProductListPhoto *productphoto = photos.firstObject;
    cell.productcoverphoto.contentMode = UIViewContentModeScaleToFill;
    [cell.productcoverphoto
        setImageWithURL:[NSURL URLWithString:productphoto.url]];
  }
  cell.likecount.text = [[ProductDetailClass.likeCount
      objectForKeyedSubscript:@"count"] stringValue];

  return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
    heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  return 236;
}
- (void)tableView:(UITableView *)tableView
    didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  // [self.navigationController pushViewController:UIViewController
  // animated:NO];
  ProductDetailClass *ProductDetailClass =
      [self.ProductDetailClassArray objectAtIndex:indexPath.row];
  ProductDetailPage *productdetailpage = [[ProductDetailPage alloc] init];
  productdetailpage.ProductDetail = ProductDetailClass;
  [productdetailpage setupProductDetailPageFromProductDetail];
  [self.navigationController pushViewController:productdetailpage animated:YES];

  DDLogVerbose(@"indexPath -->%@", indexPath);
}
- (IBAction)btnbackpressed:(id)sender {
  [self.navigationController popViewControllerAnimated:YES];
}
@end