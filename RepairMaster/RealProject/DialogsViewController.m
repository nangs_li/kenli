
#import "Chatroom.h"
#import "DialogsViewCell.h"
#import "DialogsViewController.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import "DBQBChatDialog.h"
#import "DBQBUUser.h"

#import "DBQBChatDialog.h"
#import "MMExampleDrawerVisualStateManager.h"
#import "RDVTabBarItem.h"

#import "Branch.h"
#import "ChatDialogModel.h"
@interface DialogsViewController ()<UITableViewDelegate, UITableViewDataSource,
                                    MMDrawerTouchDelegate,
                                    UISearchDisplayDelegate>

@property(nonatomic, weak) IBOutlet UITableView *dialogsTableView;

@end

@implementation DialogsViewController
//// init
- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}
//// ViewController lyfe cycle
- (void)viewDidLoad {
  [super viewDidLoad];

  self.dialogsTableViewOriginalFrame =
      CGRectMake(self.dialogsTableView.frame.origin.x,
                 self.dialogsTableView.frame.origin.y,
                 self.dialogsTableView.frame.size.width,
                 self.dialogsTableView.frame.size.height);
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  self.navigationController.navigationBar.hidden = YES;
  [[NSNotificationCenter defaultCenter]
      addObserver:self
         selector:@selector(chatRoomConnectToServerSuccess)
             name:kNotificationChatRoomConnectToServerSuccess
           object:nil];
  [[NSNotificationCenter defaultCenter]
      addObserver:self
         selector:@selector(chatDidAccidentallyDisconnectNotification)
             name:kNotificationChatDidAccidentallyDisconnect
           object:nil];
  [[NSNotificationCenter defaultCenter]
      addObserver:self
         selector:@selector(dialogsUpdatedNotification)
             name:kNotificationDialogsUpdated
           object:nil];
  [[NSNotificationCenter defaultCenter]
      addObserver:self
         selector:@selector(willEnterForegroundNotification)
             name:UIApplicationWillEnterForegroundNotification
           object:nil];
  /*
   [[NSNotificationCenter defaultCenter]
   addObserver:self
   selector:@selector(openChatroomFromDialogId:)
   name:@"openChatroomfromDailogId"
   object:nil];
   */
  if ([ChatService shared].connecttoserversuccess) {
    [self chatRoomConnectToServerSuccess];
  } else {
    [self chatDidAccidentallyDisconnectNotification];
  }

  [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:NO];
  DDLogInfo(@"viewWillAppear:(BOOL)animateddialogsviewcontroller");

  self.dialogsTableView.tableHeaderView =
      self.searchDisplayController.searchBar;
  self.searchDisplayController.searchBar.placeholder =
      JMOLocalizedString(@"ChatPageDailog.SearchBar.Search", nil);
  [self.searchDisplayController.searchResultsTableView
      setBackgroundColor:[UIColor clearColor]];
  [self.searchDisplayController.searchResultsTableView
      setSeparatorStyle:UITableViewCellSeparatorStyleNone];
  [self.dialogsTableView setFrame:self.dialogsTableViewOriginalFrame];

  ////get dialogs, dialogsagentprofile data from dbaccess

  [self.dialogsTableView reloadData];
  [[ChatDialogModel shared] updateChatBadgeNumber];
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];

  [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:NO];
}
- (void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];

  [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)setUpFixedLabelTextAccordingToSelectedLanguage {
  self.chatlabel.text = JMOLocalizedString(@"ChatPageDailog.Chat", nil);
  self.connectinglabel.text =
      JMOLocalizedString(@"ChatPageDailog.Connecting...", nil);
  self.connectinglabel.textAlignment = NSTextAlignmentCenter;
}

#pragma mark searchDisplayController Delegate(Search Bar)-----------------------------------------------------------------------------------------------

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller
    shouldReloadTableForSearchString:(NSString *)searchString {
  [self
      filterContentForSearchText:searchString
                           scope:[[self.searchDisplayController
                                         .searchBar scopeButtonTitles]
                                     objectAtIndex:
                                         [self.searchDisplayController.searchBar
                                                 selectedScopeButtonIndex]]];

  [self.dialogsTableView reloadData];
  return YES;
}
- (void)searchDisplayControllerWillBeginSearch:
    (UISearchDisplayController *)controller {
}
- (void)searchDisplayControllerDidEndSearch:
    (UISearchDisplayController *)controller {
  [self.dialogsTableView reloadData];
  self.dialogsTableView.hidden = NO;
}
//// filterContentForSearchText
- (void)filterContentForSearchText:(NSString *)searchText
                             scope:(NSString *)scope {
  self.searchDialogResultArray = [[NSMutableArray alloc] init];
  for (QBChatDialog *qbchatdialog in [ChatDialogModel shared].dialogs) {
    DBResultSet *r = [[[[DBQBUUser query]
        whereWithFormat:@"QBID = %@",
                        [NSString
                            stringWithFormat:@"%ld",
                                             (long)qbchatdialog.recipientID]]
        limit:1] fetch];
    for (DBQBUUser *user in r) {
      qbchatdialog.name = user.FullName;
    }
  }
  for (QBChatDialog *qbchatdialog in [ChatDialogModel shared].dialogs) {
    if (!([[qbchatdialog.name uppercaseString]
              rangeOfString:[searchText uppercaseString]]
              .location == NSNotFound)) {
      [self.searchDialogResultArray addObject:qbchatdialog];
    }
  }
}

- (void)searchDisplayController:(UISearchDisplayController *)controller
 willShowSearchResultsTableView:(UITableView *)tableView {
  [self.searchDisplayController.searchResultsTableView
      setFrame:self.dialogsTableViewOriginalFrame];
  self.dialogsTableView.hidden = YES;
}
- (void)searchDisplayController:(UISearchDisplayController *)controller
  didShowSearchResultsTableView:(UITableView *)tableView {
  [self findAndHideSearchBarShadowInView:self.searchDisplayController
                                             .searchResultsTableView];
}
- (void)searchDisplayController:(UISearchDisplayController *)controller
 willHideSearchResultsTableView:(UITableView *)tableView {
}
- (void)searchDisplayController:(UISearchDisplayController *)controller
  didHideSearchResultsTableView:(UITableView *)tableView {
}
- (void)findAndHideSearchBarShadowInView:(UIView *)view {
  NSString *usc = @"_";
  NSString *sb = @"UISearchBar";
  NSString *sv = @"ShadowView";
  NSString *s = [[usc stringByAppendingString:sb] stringByAppendingString:sv];

  for (UIView *v in view.subviews) {
    if ([v isKindOfClass:NSClassFromString(s)]) {
      v.alpha = 0.0f;
    }
    [self findAndHideSearchBarShadowInView:v];
  }
}

- (IBAction)invitePressed:(id)sender {
  /*

  NSMutableDictionary *params = [NSMutableDictionary dictionary];
  params[@"eid"] = [[LoginRecordModel shared].myLoginRecord.eid stringValue];
  params[@"QBID"] = [LoginRecordModel shared].myLoginRecord.QBID;
  params[@"username"] = [LoginRecordModel shared].myLoginRecord.username;

  [[Branch getInstance] getShortURLWithParams:params andChannel:@"sms"
  andFeature:BRANCH_FEATURE_TAG_SHARE andCallback:^(NSString *url, NSError
  *error) {
      if (!error){
          [self
  presentUIActivityViewControllerSendDownRealUrlLinkAndThenOpenChatRoom:url];

      }



  }];
  */

  [self presentUIActivityViewControllerSendDownRealUrlLinkAndThenOpenChatRoom:
            nil];
}

- (void)presentUIActivityViewControllerSendDownRealUrlLinkAndThenOpenChatRoom:
    (NSString *)url {
  NSString *textToShare = @"Make your friends smile in RepairMaster - the "
                          @"social network for advertisement";
  ;
  NSArray *objectsToShare = @[
    textToShare,
    [NSURL URLWithString:@"https://beta.itunes.apple.com/v1/invite/"
                         @"653d49d455b147d197455d8c88a6957e072fa494629e4d30912"
                         @"424f37f9c96903db344f4?ct=XD4BTJGD9B&pt=2003"]
  ];
  UIActivityViewController *activityVC =
      [[UIActivityViewController alloc] initWithActivityItems:objectsToShare
                                        applicationActivities:nil];
  [self presentViewController:activityVC animated:YES completion:nil];
}

#pragma mark dialogs Notifications(reload TableView)-----------------------------------------------------------------------------------------------------------------
- (void)dialogsUpdatedNotification {
  [self.dialogsTableView reloadData];
}
- (void)chatRoomConnectToServerSuccess {
  self.loading.hidden = YES;
  self.connectinglabel.hidden = YES;
  self.chatlabel.hidden = NO;
}
- (void)chatDidAccidentallyDisconnectNotification {
  [self.dialogsTableView reloadData];
  self.loading.hidden = NO;
  self.connectinglabel.hidden = NO;
  self.chatlabel.hidden = YES;
}
- (void)groupDialogJoinedNotification {
  [self.dialogsTableView reloadData];
}
- (void)willEnterForegroundNotification {
  [self.dialogsTableView reloadData];
}
- (void)btnEditPressed:(id)sender {
  DDLogInfo(@"btnEditPressed");
  if (self.dialogsTableView.editing == NO) {
    [_dialogsTableView setEditing:YES animated:YES];
  } else {
    [_dialogsTableView setEditing:NO animated:YES];
  }
}

#pragma mark UITableViewDelegate & UITableViewDataSource start--------------------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
  if (self.searchDisplayController.active) {
    return self.searchDialogResultArray.count;
  }

  return [ChatDialogModel shared].dialogs.count;
}
- (CGFloat)tableView:(UITableView *)tableView
    heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  return 64;
}
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  // reuse table view cell  if exist or create new one(ClinicTableViewCell)
  DDLogInfo(@"cellForRowAtIndexPath-->%ld", (long)indexPath.row);

  DialogsViewCell *cell = (DialogsViewCell *)[tableView
      dequeueReusableCellWithIdentifier:@"DialogsViewCell"];
  if (cell == nil) {
    // table view cell setting
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DialogsViewCell"
                                                 owner:self
                                               options:nil];

    cell = [nib objectAtIndex:0];
  }
  // NSMutableArray * dialogarray = [ChatService shared].dialogs;

  QBChatDialog *chatDialog = nil;
  chatDialog = [ChatDialogModel shared].dialogs[indexPath.row];
  if (self.searchDisplayController.active) {
    chatDialog = self.searchDialogResultArray[indexPath.row];
  }
  DDLogInfo(@"chatDialog.recipientID : %ld", (long)chatDialog.recipientID);
  NSNumber *tableViewCellQBID;
  NSNumber *occupantQBIDIndex0 = [chatDialog.occupantIDs objectAtIndex:0];
  if ([[occupantQBIDIndex0 stringValue]
          isEqualToString:[LoginRecordModel shared].myLoginRecord.QBID]) {
    tableViewCellQBID = [chatDialog.occupantIDs objectAtIndex:1];
  } else {
    tableViewCellQBID = occupantQBIDIndex0;
  }

  DBResultSet *r = [[[DBQBUUser query]
      whereWithFormat:@"QBID = %@", [tableViewCellQBID stringValue]] fetch];
  NSString *title;
  NSURL *url;
  for (DBQBUUser *dbqbuser in r) {
    title = dbqbuser.FullName;
  }

  [cell.date
      setText:[self stringForTimeIntervalDateTime:chatDialog.lastMessageDate
                                      NowDateTime:[NSDate date]]];
  [cell setupcellmessage:title subtitle:chatDialog.lastMessageText];
  [cell.personalimageview setImage:[UIImage imageNamed:@"ChatPersonalPhoto"]];

  [self.delegate setupcornerRadius:cell.personalimageview];

  if (chatDialog.unreadMessagesCount == 0) {
    cell.numberOfLastMessageLabel.hidden = YES;
    cell.numberOfLastMessageLabel.layer.cornerRadius = 10;
    cell.numberOfLastMessageLabel.clipsToBounds = YES;

  } else {
    cell.numberOfLastMessageLabel.hidden = NO;

    cell.numberOfLastMessageLabel.text = [NSString
        stringWithFormat:@"%li", (unsigned long)chatDialog.unreadMessagesCount];

    cell.numberOfLastMessageLabel.layer.cornerRadius = 10;
    cell.numberOfLastMessageLabel.clipsToBounds = YES;
  }

  //// create separator line

  UIColor *separatorBGColor = [UIColor colorWithRed:204.0 / 255.0
                                              green:204.0 / 255.0
                                               blue:204.0 / 255.0
                                              alpha:1.0];

  UIImageView *separator = [[UIImageView alloc]
      initWithFrame:CGRectMake(0.0f, cell.frame.size.height,
                               cell.frame.size.width,
                               (1.0 / [UIScreen mainScreen].scale))];
  separator.backgroundColor = separatorBGColor;

  [cell addSubview:separator];
  UIImageView *separator2 = [[UIImageView alloc]
      initWithFrame:CGRectMake(0.0f, cell.frame.origin.y, cell.frame.size.width,
                               (1.0 / [UIScreen mainScreen].scale))];
  separator2.backgroundColor = separatorBGColor;

  [cell addSubview:separator2];
  return cell;
}
- (void)tableView:(UITableView *)tableView
    didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  [tableView deselectRowAtIndexPath:indexPath animated:YES];

  QBChatDialog *chatDialog = [ChatDialogModel shared].dialogs[indexPath.row];

  if (self.searchDisplayController.active) {
    chatDialog = self.searchDialogResultArray[indexPath.row];
  }
  NSNumber *tableViewCellQBID;
  NSNumber *occupantQBIDIndex0 = [chatDialog.occupantIDs objectAtIndex:0];
  if ([[occupantQBIDIndex0 stringValue]
          isEqualToString:[LoginRecordModel shared].myLoginRecord.QBID]) {
    tableViewCellQBID = [chatDialog.occupantIDs objectAtIndex:1];
  } else {
    tableViewCellQBID = occupantQBIDIndex0;
  }
  //// UITableViewDelegate & UITableViewDataSource
  /// end--------------------------------------------------------------------------
  //// get DBQBUser from DB
  DBResultSet *dbresult = [[[DBQBUUser query]
      whereWithFormat:@" QBID = %@", [tableViewCellQBID stringValue]] fetch];
  Chatroom *vc = [Chatroom messagesViewController];
  for (DBQBUUser *qbuuser in dbresult) {
    vc.chatroomNameString = qbuuser.FullName;
  }
  vc.recipientID = [NSNumber numberWithInteger:chatDialog.recipientID];

  vc.dialog = chatDialog;
  self.navigationController.navigationBar.hidden = NO;
  [self.navigationController pushViewController:vc animated:YES];
}
//// stringForTimeIntervalSinceCreated

- (NSString *)stringForTimeIntervalDateTime:(NSDate *)dateTime
                                NowDateTime:(NSDate *)nowDateTime {
  //// only between day
  NSDateFormatter *Onlydayformat = [[NSDateFormatter alloc] init];
  [Onlydayformat setDateFormat:@"yyyy-MM-dd"];
  NSString *dateTimestring = [Onlydayformat stringFromDate:dateTime];
  NSString *nowDateTimestring = [Onlydayformat stringFromDate:nowDateTime];
  DDLogInfo(@"dateTimestring-->%@", dateTimestring);
  DDLogInfo(@"nowDateTimestring-->%@", nowDateTimestring);
  NSDate *dateTimeOnlydayformat = [Onlydayformat dateFromString:dateTimestring];
  NSDate *nowDateOnlydayformat =
      [Onlydayformat dateFromString:nowDateTimestring];
  NSCalendar *gregorianCalendar = [[NSCalendar alloc]
      initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
  NSDateComponents *components =
      [gregorianCalendar components:NSCalendarUnitDay
                           fromDate:dateTimeOnlydayformat
                             toDate:nowDateOnlydayformat
                            options:NSCalendarWrapComponents];
  DDLogInfo(@"components.day-->%ld", (long)components.day);
  //// only between time
  NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
  //  NSInteger DayInterval;
  // NSInteger DayModules;

  if (components.day >= 2) {
    [dateFormat setDateFormat:@"d/M/y"];
    return [dateFormat stringFromDate:dateTime];
  } else if (components.day == 1) {
    return @"Yesterday";
  }

  else {
    [dateFormat setDateFormat:@"h:mm a"];
    return [dateFormat stringFromDate:dateTime];
  }
}

@end