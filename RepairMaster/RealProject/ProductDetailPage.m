//
//  ViewController.m
//  TAPageControl
//
//  Created by Tanguy Aladenise on 2015-01-21.
//  Copyright (c) 2015 Tanguy Aladenise. All rights reserved.
//

// Controllers
#import "ProductDetailPage.h"
// Views

#import <SDWebImage/UIImageView+WebCache.h>
#import "CustomerSignupPage.h"
#import "GetQBIDbyVendorEidModel.h"
#import "LikeProductModel.h"
#import "MMDrawerController.h"
#import "PressChatButtonModel.h"
#import "Productfindbytermpage.h"
#import "TAPageControl.h"
@interface ProductDetailPage ()<UIScrollViewDelegate, TAPageControlDelegate>

@property(weak, nonatomic) IBOutlet TAPageControl *customStoryboardPageControl;
@property(strong, nonatomic) TAPageControl *customPageControl2;
@property(strong, nonatomic) TAPageControl *customPageControl3;

@property(strong, nonatomic) IBOutletCollection(UIScrollView)
    NSArray *scrollViews;

@property(weak, nonatomic) IBOutlet UIScrollView *scrollView1;

@property(strong, nonatomic) NSMutableArray *imagesData;

@end

@implementation ProductDetailPage

#pragma mark - Lifecycle

- (void)viewDidLoad {
  [super viewDidLoad];

  self.navigationItem.title = @"Search Detail";
}
- (void)setupProductDetailPageFromProductDetail {
  self.imagesData = [[NSMutableArray alloc] init];

  for (ProductListPhoto *photo in self.ProductDetail.photos) {
    [self.imagesData addObject:photo.url];
  }

  [self setupScrollViewImages];

  for (UIScrollView *scrollView in self.scrollViews) {
    scrollView.delegate = self;
  }

  // TAPageControl from storyboard
  self.customStoryboardPageControl.numberOfPages = self.imagesData.count;
  ProductDetailClass *ProductDetailClass = self.ProductDetail;

  self.price.text =
      [NSString stringWithFormat:@"$%@", ProductDetailClass.price];
  self.type.text = [NSString stringWithFormat:@"類別: %@", @"冷氣維修"];
  self.productName.text = [NSString
      stringWithFormat:@"廣告名稱: %@", ProductDetailClass.qualification];
  self.likecount.text =
      [[ProductDetailClass.likeCount objectForKey:@"count"] stringValue];
  self.productDescription.text =
      [NSString stringWithFormat:@"詳細簡介: %@", ProductDetailClass.desc];
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:YES];
  [self getChatIDbyVendorEidModel];
  [self setupProductDetailPageFromProductDetail];
  [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:NO];
}

#pragma mark - ScrollView delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
  NSInteger pageIndex =
      scrollView.contentOffset.x / CGRectGetWidth(scrollView.frame);

  //   DDLogVerbose(@"pageIndex-->%ld",pageIndex);
  self.customStoryboardPageControl.currentPage = pageIndex;
}

#pragma mark - Utils

- (void)setupScrollViewImages {
  self.automaticallyAdjustsScrollViewInsets = NO;
  for (UIScrollView *scrollView in self.scrollViews) {
    [self.imagesData enumerateObjectsUsingBlock:^(NSString *urlstring,
                                                  NSUInteger idx, BOOL *stop) {

      UIImageView *imageView = [[UIImageView alloc]
          initWithFrame:CGRectMake(CGRectGetWidth(scrollView.frame) * idx, 0,
                                   CGRectGetWidth(scrollView.frame),
                                   CGRectGetHeight(scrollView.frame))];
      imageView.contentMode = UIViewContentModeScaleAspectFill;
      [imageView setImageWithURL:[NSURL URLWithString:urlstring]];

      [scrollView addSubview:imageView];
    }];
  }

  for (UIScrollView *scrollView in self.scrollViews) {
    scrollView.contentSize =
        CGSizeMake(CGRectGetWidth(scrollView.frame) * self.imagesData.count,
                   scrollView.frame.size.height);
  }
}
- (IBAction)btnLikePressed:(id)sender {
  if ([self IsEmpty:[LoginRecordModel shared].myLoginRecord.eid]) {
    CustomerSignupPage *UIViewController =
        [[CustomerSignupPage alloc] initWithNibName:@"CustomerSignupPage"
                                             bundle:nil];

    [self.navigationController pushViewController:UIViewController
                                         animated:YES];

  } else {
    [[LikeProductModel shared]
        likeProductModelbyTaxModelByProductEid:[self.ProductDetail
                                                       .eid stringValue]
        success:^(id responseObject) {
          UIAlertView *alert =
              [[UIAlertView alloc] initWithTitle:@"提示"
                                         message:@"讚已成功"
                                        delegate:nil
                               cancelButtonTitle:@"OK"
                               otherButtonTitles:nil];
          [alert show];
        }
        failure:^(AFHTTPRequestOperation *operation, NSError *error,
                  NSString *errorMessage, int errorStatus, NSString *alert,
                  NSString *ok) {
          [MBProgressHUD hideHUDForView:self.view animated:YES];
          UIAlertView *alertView =
              [[UIAlertView alloc] initWithTitle:alert
                                         message:errorMessage
                                        delegate:nil
                               cancelButtonTitle:ok
                               otherButtonTitles:nil];
          [alertView show];
        }];
  }
}

- (IBAction)btnChatPressed:(id)sender {
  if ([self IsEmpty:self.ProductDetail.QBID]) {
    return;
  }
  if ([self IsEmpty:[LoginRecordModel shared].myLoginRecord.eid]) {
    CustomerSignupPage *UIViewController =
        [[CustomerSignupPage alloc] initWithNibName:@"CustomerSignupPage"
                                             bundle:nil];

    [self.navigationController pushViewController:UIViewController
                                         animated:YES];

  } else {
    [self loadingAndStopLoadingAfterSecond:4];
    [[PressChatButtonModel shared]
        pressChatButtonModelByQBID:self.ProductDetail.QBID
        username:@"Vendor"
        success:^(Chatroom *chatRoom) {

          [self.navigationController pushViewController:chatRoom animated:YES];

        }
        failure:^(AFHTTPRequestOperation *operation, NSError *error,
                  NSString *errorMessage, RequestErrorStatus errorStatus,
                  NSString *alert, NSString *ok) {

          [MBProgressHUD hideHUDForView:self.view animated:YES];

          UIAlertView *alertView =
              [[UIAlertView alloc] initWithTitle:alert
                                         message:errorMessage
                                        delegate:nil
                               cancelButtonTitle:ok
                               otherButtonTitles:nil];
          [alertView show];
        }];
  }
}

- (IBAction)btnPhoneCallPressed:(id)sender {
  NSString *phoneCallUrlString =
      [NSString stringWithFormat:@"tel:%@", self.ProductDetail.telNo];

  [[UIApplication sharedApplication]
      openURL:[NSURL URLWithString:phoneCallUrlString]];
}
- (void)getChatIDbyVendorEidModel {
  [[GetQBIDbyVendorEidModel shared]
      getQBIDbyVendorEid:self.ProductDetail.vendorId
      success:^(NSString *QBID) {

        self.ProductDetail.QBID = QBID;

      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error,
                NSString *errorMessage, int errorStatus, NSString *alert,
                NSString *ok) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alert
                                                            message:errorMessage
                                                           delegate:nil
                                                  cancelButtonTitle:ok
                                                  otherButtonTitles:nil];
        [alertView show];
      }];
}
@end
