//
//  ViewController.m
//  TAPageControl
//
//  Created by Tanguy Aladenise on 2015-01-21.
//  Copyright (c) 2015 Tanguy Aladenise. All rights reserved.
//

// Controllers
#import "SearchProductPage.h"
// Views

#import "MMDrawerController.h"
#import "Productfindbytermpage.h"
#import "TAPageControl.h"

@interface SearchProductPage ()<UIScrollViewDelegate, TAPageControlDelegate>

@property(weak, nonatomic) IBOutlet TAPageControl *customStoryboardPageControl;
@property(strong, nonatomic) TAPageControl *customPageControl2;
@property(strong, nonatomic) TAPageControl *customPageControl3;

@property(weak, nonatomic) IBOutlet UIScrollView *scrollView1;

@property(strong, nonatomic) NSArray *imagesData;

@end

@implementation SearchProductPage

#pragma mark - Lifecycle

- (void)viewDidLoad {
  [super viewDidLoad];

  self.imagesData = @[
    @"image3.jpg",
    @"image2.jpg",
    @"image1.jpg",
    @"image4.jpg",
    @"image5.jpg",
    @"image6.jpg"
  ];

  [self setupScrollViewImages];

  self.scrollView1.delegate = self;

  // TAPageControl from storyboard
  self.customStoryboardPageControl.numberOfPages = self.imagesData.count;

  self.navigationItem.title = @"Search";
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:NO];
}
- (void)viewDidLayoutSubviews {
  [super viewDidLayoutSubviews];

  self.scrollView1.contentSize =
      CGSizeMake(CGRectGetWidth(self.scrollView1.frame) * self.imagesData.count,
                 CGRectGetHeight(self.scrollView1.frame));
}

#pragma mark - ScrollView delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
  NSInteger pageIndex =
      scrollView.contentOffset.x / CGRectGetWidth(scrollView.frame);

  //   DDLogVerbose(@"pageIndex-->%ld",pageIndex);
  self.customStoryboardPageControl.currentPage = pageIndex;
}

#pragma mark - Utils

- (void)setupScrollViewImages {
  self.automaticallyAdjustsScrollViewInsets = NO;

  [self.imagesData enumerateObjectsUsingBlock:^(NSString *imageName,
                                                NSUInteger idx, BOOL *stop) {
    UIImageView *imageView = [[UIImageView alloc]
        initWithFrame:CGRectMake(self.scrollView1.frame.size.width * idx, 0,
                                 CGRectGetWidth(self.scrollView1.frame),
                                 CGRectGetHeight(self.scrollView1.frame))];
    // imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.image = [UIImage imageNamed:imageName];
    [self.scrollView1 addSubview:imageView];
  }];
}

- (IBAction)btnnextpagepressed:(id)sender {
  UIButton *button = sender;
  Productfindbytermpage *productfindbytermpage =
      [[Productfindbytermpage alloc] init];
  productfindbytermpage.buttonterm = button.tag;
  [productfindbytermpage getProductFindByTermFromServer];
  [self.navigationController pushViewController:productfindbytermpage
                                       animated:YES];
}
@end
