//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "MyCustomLogFormatter.h"

@implementation MyCustomLogFormatter

- (NSString *)formatLogMessage:(DDLogMessage *)logMessage {
  NSString *logLevel;
  switch (logMessage->_flag) {
    case DDLogFlagError:
      logLevel = @"Error  ";
      break;
    case DDLogFlagWarning:
      logLevel = @"Warning";
      break;
    case DDLogFlagInfo:
      logLevel = @"Log    ";
      break;
    case DDLogFlagDebug:
      logLevel = @"Debug  ";
      break;
    case DDLogFlagVerbose:
      logLevel = @"Server ";
      break;
    default:
      logLevel = @"Log    ";
      break;
  }
  NSString *dateString =
      [NSDateFormatter localizedStringFromDate:logMessage.timestamp
                                     dateStyle:NSDateFormatterNoStyle
                                     timeStyle:NSDateFormatterMediumStyle];

  NSMutableString *logMessagefileName =
      [NSMutableString stringWithString:logMessage.fileName];
  if ([logMessage.fileName length] < 25) {
    NSInteger length = 25 - [logMessage.fileName length];
    for (int i = 0; i < length; i++) {
      [logMessagefileName appendFormat:@" "];
    }
  }
  NSMutableString *logMessagefunction =
      [NSMutableString stringWithString:logMessage.function];
  if ([logMessage.function length] < 80) {
    NSInteger length = 80 - [logMessage.function length];
    for (int i = 0; i < length; i++) {
      [logMessagefunction appendFormat:@" "];
    }
  }
  NSMutableString *logMessageline = [NSMutableString
      stringWithString:[NSString
                           stringWithFormat:@"%li",
                                            (unsigned long)logMessage.line]];
  if ([logMessageline length] < 4) {
    NSInteger length = 4 - [logMessageline length];
    for (int i = 0; i < length; i++) {
      [logMessageline appendFormat:@" "];
    }
  }
  return [NSString stringWithFormat:@"%@  |  %@ | %@ | %@ | line : %@ | %@",
                                    dateString, logLevel, logMessagefileName,
                                    logMessagefunction, logMessageline,
                                    logMessage.message];
}

@end