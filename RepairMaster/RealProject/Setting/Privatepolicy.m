//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Privatepolicy.h"

@interface Privatepolicy ()

@end
@implementation Privatepolicy

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];

  self.reportaproblemtextview.delegate = self;

  [[self.delegate getTabBarController] setTabBarHidden:YES];
}

- (void)viewDidLoad {
  [super viewDidLoad];
  self.navigationItem.title = @"PrivatePolicy";
}
#pragma mark - Button
- (IBAction)closebutton:(id)sender {
  [self.navigationController popViewControllerAnimated:YES];
}
#pragma marks - textview Delegate
- (void)textViewDidEndEditing:(UITextView *)textView {
  if ([textView.text isEqualToString:@""]) {
    textView.text = @"Report Some Problem Here...";
    textView.textColor = [UIColor lightGrayColor];  // optional
  }
  [textView resignFirstResponder];
}
- (void)textViewDidBeginEditing:(UITextView *)textView {
  if ([textView.text isEqualToString:@"Report Some Problem Here..."]) {
    textView.text = @"";
    textView.textColor = [UIColor lightGrayColor];  // optional
  }
  [textView becomeFirstResponder];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
  return YES;
}
- (BOOL)textView:(UITextView *)textView
    shouldChangeTextInRange:(NSRange)range
            replacementText:(NSString *)text {
  DDLogInfo(@"shouldChangeTextInRange");

  if ([text isEqualToString:@"\n"]) {
    [textView resignFirstResponder];

    return NO;
  }

  return YES;
}

@end