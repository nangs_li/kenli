//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Reportaproblem.h"

@interface Reportaproblem ()<UITableViewDataSource, UITableViewDelegate>

@end
@implementation Reportaproblem

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];

  self.reportaproblemtextview.delegate = self;

  [[self.delegate getTabBarController] setTabBarHidden:YES];
}

- (void)viewDidLoad {
  [super viewDidLoad];
}
#pragma mark - Button
- (IBAction)closebutton:(id)sender {
  [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)nextbuttonpressed:(id)sender {
  if ([self.delegate networkConnection]) {
    [self loadingAndStopLoadingAfterSecond:10];
  }
  /*
NSString *string = [NSString
    stringWithFormat:@"%@%@", [AppDelegate getAppDelegate].testServerAddress,
                     @"/Operation.asmx/ReportProblem"];
NSString *parameters =
    [NSString stringWithFormat:@"{\"eid\":%@,\"AccessToken\":\"%@\","
                               @"\"ProblemType\":\"%d\",\"Toeid\":%d,"
                               @"\"ToListingID\":%d,\"Description\":\"%@\"}",
                               self.delegate.myLoginRecord.eid,
                               self.delegate.myLoginRecord.AccessToken, 1, 0, 0,
                               self.reportaproblemtextview.text, nil];

[self.api ReportProblem:string:@{ @"InputJson" : parameters }];
   */
}
#pragma marks - textview Delegate
- (void)textViewDidEndEditing:(UITextView *)textView {
  if ([textView.text isEqualToString:@""]) {
    textView.text = @"Report Some Problem Here...";
    textView.textColor = [UIColor lightGrayColor];  // optional
  }
  [textView resignFirstResponder];
  [self checkEmptyToChangeSaveButtonColor];
}
- (void)textViewDidBeginEditing:(UITextView *)textView {
  if ([textView.text isEqualToString:@"Report Some Problem Here..."]) {
    textView.text = @"";
    textView.textColor = [UIColor lightGrayColor];  // optional
  }
  [textView becomeFirstResponder];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
  return YES;
}
- (BOOL)textView:(UITextView *)textView
    shouldChangeTextInRange:(NSRange)range
            replacementText:(NSString *)text {
  DDLogInfo(@"shouldChangeTextInRange");

  if ([text isEqualToString:@"\n"]) {
    [textView resignFirstResponder];

    return NO;
  }

  return YES;
}
- (void)checkEmptyToChangeSaveButtonColor {
  if ([self.reportaproblemtextview.text length] > 0) {
    [self.nextbutton setBackgroundColor:self.delegate.Greencolor];
    self.nextbutton.userInteractionEnabled = YES;

  } else {
    [self.nextbutton setBackgroundColor:self.delegate.Greycolor];
    self.nextbutton.userInteractionEnabled = NO;
  }
}
#pragma mark - APIEngine Delegate  Server Return String
- (void)requestDone:(NSMutableDictionary *)dict {
  NSString *dstring = [dict objectForKey:@"d"];
  NSError *err = nil;
  self.HandleServerReturnString =
      [[HandleServerReturnString alloc] initWithString:dstring error:&err];
  NSString *APIName = self.HandleServerReturnString.APIName;

  if ([APIName isEqualToString:@"ReportProblem"]) {
    [[[UIAlertView alloc] initWithTitle:@"Notice"
                                message:@"Report Problem Successful"
                               delegate:self
                      cancelButtonTitle:@"OK!"
                      otherButtonTitles:nil] show];

    [MBProgressHUD hideHUDForView:self.view animated:YES];
  }
}
#pragma mark - APIEngine Delegate  Server Return Fail String
- (void)requestFail:(NSMutableDictionary *)dict {
  [MBProgressHUD hideHUDForView:self.view animated:YES];
}

@end