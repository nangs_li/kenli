//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <objc/message.h>
#import "BaseViewController.h"

@interface Aboutthisversion : BaseViewController<UITextViewDelegate>

@property(strong, nonatomic) IBOutlet UIButton *nextbutton;
@property(strong, nonatomic) IBOutlet UITextView *reportaproblemtextview;
- (IBAction)closebutton:(id)sender;
@end
