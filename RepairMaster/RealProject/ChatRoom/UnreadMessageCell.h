//
//  ChatMessageTableViewCell.h
//  sample-chat
//
//  Created by Igor Khomenko on 10/19/13.
//  Copyright (c) 2013 Igor Khomenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnreadMessageCell : UITableViewCell
@property(strong, nonatomic) IBOutlet UILabel *unreadlabel;
#pragma mark Message SetUp Method
+ (CGFloat)heightForCellWithMessage:(DBQBChatMessage *)message;
- (void)configureCellWithMessage:(DBQBChatMessage *)message;

@end
