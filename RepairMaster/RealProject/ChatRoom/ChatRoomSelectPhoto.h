//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>
#import <objc/message.h>
#import "BaseViewController.h"
#import "ELCImagePickerHeader.h"
#import "LXReorderableCollectionViewFlowLayout.h"
@interface ChatRoomSelectPhoto
    : BaseViewController<LXReorderableCollectionViewDataSource,
                         LXReorderableCollectionViewDelegateFlowLayout,
                         ELCImagePickerControllerDelegate>

#pragma mark upload photo collectionview
@property(nonatomic, retain) NSMutableArray *collectionviewdata;
@property(nonatomic, retain) NSMutableArray *tableData;
@property(strong, nonatomic) IBOutlet UICollectionView *CollectionView;
@property(nonatomic, assign) BOOL CollectionViewAddbuttonexist;
@property(nonatomic, retain) NSIndexPath *checkedCell;
@property(nonatomic, retain) NSMutableArray *chatroomchosenImages;

@property(strong, nonatomic) IBOutlet UIButton *nextbutton;

@property(strong, nonatomic) IBOutlet UIImageView *selectedimage;

@property(nonatomic, strong) NSIndexPath *selectedcoverimageindexpath;
@property(nonatomic, strong) UIView *coveruiview;
@property(nonatomic, assign) BOOL firsttimeselectphoto;
@end
