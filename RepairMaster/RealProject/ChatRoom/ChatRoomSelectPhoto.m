//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "ChatRoomSelectPhoto.h"
#import "ChatRoomSendMessageModel.h"
#import "Collectionviewcell.h"
@interface ChatRoomSelectPhoto ()

@end
@implementation ChatRoomSelectPhoto

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  // self.chatroomchosenImages=[[NSMutableArray alloc]init];
}
- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:YES];

  if (self.chatroomchosenImages == nil) {
    self.nextbutton.userInteractionEnabled = NO;

  } else {
    self.nextbutton.userInteractionEnabled = YES;
  }

  [self.CollectionView reloadData];
}
- (void)viewDidLoad {
  [super viewDidLoad];
  self.coveruiview = [[UIView alloc] init];
  self.coveruiview.tag = 50;
  [self.coveruiview setFrame:self.view.frame];
  [self.coveruiview setBackgroundColor:[UIColor whiteColor]];
  [self.view addSubview:self.coveruiview];
  self.firsttimeselectphoto = YES;
  [self.CollectionView registerClass:[Collectionviewcell class]
          forCellWithReuseIdentifier:@"Collectionviewcell"];
  self.collectionviewdata = [[NSMutableArray alloc]
      initWithObjects:@"0", @"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8",
                      @"9", nil];

  self.CollectionView.scrollEnabled = NO;

  [self.CollectionView reloadData];

  [self CoverPrssd:self];
}

//// -
/// Button------------------------------------------------------------------------------------------------------------------------------
- (IBAction)btnbackpress:(id)sender {
  [ChatRoomSendMessageModel shared].chatroomchosenImages = nil;

  [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)nextbuttonpressed:(id)sender {
  [ChatRoomSendMessageModel shared].chatroomchosenImages =
      self.chatroomchosenImages;

  [self btnBackPressed:sender];
}
//// -
/// Button------------------------------------------------------------------------------------------------------------------------------

//// - UICollectionViewDataSource
/// methods--------------------------------------------------------------------------------------------------
- (NSInteger)collectionView:(UICollectionView *)theCollectionView
     numberOfItemsInSection:(NSInteger)theSectionIndex {
  return self.collectionviewdata.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  //   DDLogInfo(@"collectionView cellForItemAtIndexPath%ld",(long)indexPath.row
  //   );

  Collectionviewcell *collectionView2 = (Collectionviewcell *)[collectionView
      dequeueReusableCellWithReuseIdentifier:@"Collectionviewcell"
                                forIndexPath:indexPath];

  collectionView2.layer.borderWidth = 1.0f;
  if ([self.selectedcoverimageindexpath isEqual:indexPath]) {
    collectionView2.layer.borderColor = [UIColor colorWithRed:(60 / 255.f)
                                                        green:(218 / 255.f)
                                                         blue:(140 / 255.f)
                                                        alpha:1.0]
                                            .CGColor;
  } else {
    collectionView2.layer.borderColor = self.delegate.Greycolor.CGColor;
  }
  //// -   deletebutton

  [collectionView2.playingCardImageView
      setImage:self.chatroomchosenImages[indexPath.row]];

  collectionView2.playingCardImageView.contentMode =
      UIViewContentModeScaleAspectFill;
  collectionView2.playingCardImageView.clipsToBounds = YES;

  collectionView2.deletebutton.frame =
      CGRectMake(collectionView2.frame.size.width - 34, 0, 34, 34);
  [collectionView2.deletebutton addTarget:self
                                   action:@selector(RemovePrssd:)
                         forControlEvents:UIControlEventTouchUpInside];

  // collectionView2.addbutton.hidden=YES;
  if (self.chatroomchosenImages == nil) {
    if (indexPath.row == 0) {
      [collectionView2.playingCardImageView
          setImage:[UIImage imageNamed:@"collectionviewaddbutton"]];
    }
  } else {
    if ([self.chatroomchosenImages[indexPath.row]
            isEqual:[UIImage imageNamed:@"emptyphoto"]] ||
        [self.chatroomchosenImages[indexPath.row]
            isEqual:[UIImage imageNamed:@"collectionviewaddbutton"]]) {
    } else {
    }
  }

  return collectionView2;
}

- (void)collectionView:(UICollectionView *)collectionView
    didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
  DDLogInfo(@"collectionViewindexPath=%@", indexPath);

  Collectionviewcell *collectionView2 =
      (Collectionviewcell *)[self.CollectionView
          cellForItemAtIndexPath:indexPath];

  if ([collectionView2.playingCardImageView.image
          isEqual:[UIImage imageNamed:@"collectionviewaddbutton"]]) {
    DDLogInfo(@"collectionviewaddbutton");
    [self CoverPrssd:self];
  } else {
    if ([collectionView2.playingCardImageView.image
            isEqual:[UIImage imageNamed:@"emptyphoto"]]) {
    } else {
      self.selectedcoverimageindexpath = indexPath;

      DDLogInfo(@"self.selectedcoverimageindexpath-->%@",
                self.selectedcoverimageindexpath);
      self.selectedimage.contentMode = UIViewContentModeScaleAspectFit;
      self.selectedimage.clipsToBounds = YES;
      CATransition *transition = [CATransition animation];
      transition.duration = 0.5f;
      transition.timingFunction = [CAMediaTimingFunction
          functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
      transition.type = kCATransitionFade;

      [self.selectedimage.layer addAnimation:transition forKey:nil];

      self.selectedimage.image = collectionView2.playingCardImageView.image;
      [self.CollectionView reloadData];
    }
  }
}
//// -     RemovePrssd or CoverPrssd or UploadphotoPrssd
- (void)RemovePrssd:(id)sender {
  DDLogInfo(@"before remove prssed self.chatroomchosenImages---->%@",
            self.chatroomchosenImages);
  UIView *senderButton = (UIView *)sender;
  Collectionviewcell *cell =
      (Collectionviewcell *)[[senderButton superview] superview];
  NSIndexPath *indexPath = [self.CollectionView
      indexPathForCell:(UICollectionViewCell *)[[senderButton superview]
                           superview]];

  [self.chatroomchosenImages
      replaceObjectAtIndex:indexPath.row
                withObject:[UIImage imageNamed:@"emptyphoto"]];

  [cell.playingCardImageView setImage:[UIImage imageNamed:@"emptyphoto"]];
  cell.deletebutton.hidden = YES;
  DDLogInfo(@"after remove prssed self.chatroomchosenImages---->%@",
            self.chatroomchosenImages);
}
- (void)CoverPrssd:(id)sender {
  self.CollectionViewAddbuttonexist = NO;

  DDLogInfo(@"CoverPrssd");

  ELCImagePickerController *elcPicker =
      [[ELCImagePickerController alloc] initImagePicker];

  elcPicker.maximumImagesCount =
      10;  // Set the maximum number of images to select to 100
  elcPicker.returnsOriginalImage =
      YES;  // Only return the fullScreenImage, not the fullResolutionImage
  elcPicker.returnsImage = YES;  // Return UIimage if YES. If NO, only return
  // asset location information
  elcPicker.onOrder = YES;  // For multiple image selection, display and return
  // order of selected images
  elcPicker.mediaTypes = @[
    (NSString *)kUTTypeImage,
    (NSString *)kUTTypeMovie
  ];  // Supports image and movie types

  elcPicker.imagePickerDelegate = self;

  [self presentViewController:elcPicker animated:YES completion:nil];
}
//// - UICollectionViewDataSource
/// methods--------------------------------------------------------------------------------------------------

//// - LXReorderableCollectionViewDelegateFlowLayout
/// methods-------------------------------------------------------------------------------
- (void)collectionView:(UICollectionView *)collectionView
                              layout:
                                  (UICollectionViewLayout *)collectionViewLayout
    willBeginDraggingItemAtIndexPath:(NSIndexPath *)indexPath {
  DDLogInfo(@"will begin drag");
}

- (void)collectionView:(UICollectionView *)collectionView
                             layout:
                                 (UICollectionViewLayout *)collectionViewLayout
    didBeginDraggingItemAtIndexPath:(NSIndexPath *)indexPath {
  DDLogInfo(@"did begin drag");
}

- (void)collectionView:(UICollectionView *)collectionView
                            layout:
                                (UICollectionViewLayout *)collectionViewLayout
    willEndDraggingItemAtIndexPath:(NSIndexPath *)indexPath {
  DDLogInfo(@"will end drag");
}

- (void)collectionView:(UICollectionView *)collectionView
                           layout:(UICollectionViewLayout *)collectionViewLayout
    didEndDraggingItemAtIndexPath:(NSIndexPath *)indexPath {
  DDLogInfo(@"did end drag");
  [self.CollectionView reloadData];
}

//// - LXReorderableCollectionViewDataSource methods

- (void)collectionView:(UICollectionView *)collectionView
       itemAtIndexPath:(NSIndexPath *)fromIndexPath
   willMoveToIndexPath:(NSIndexPath *)toIndexPath {
  DDLogInfo(@"before moveing self.chatroomchosenImages%@",
            self.chatroomchosenImages);
  DDLogInfo(@"fromIndexPath.item%ld", (long)fromIndexPath.item);
  DDLogInfo(@"toIndexPath.item%ld", (long)toIndexPath.item);

  UIImage *tempimage = self.chatroomchosenImages[fromIndexPath.item];
  [self.chatroomchosenImages removeObjectAtIndex:(long)fromIndexPath.item];

  [self.chatroomchosenImages insertObject:tempimage atIndex:toIndexPath.item];

  DDLogInfo(@"after moveing self.chatroomchosenImages%@",
            self.chatroomchosenImages);
}

- (BOOL)collectionView:(UICollectionView *)collectionView
canMoveItemAtIndexPath:(NSIndexPath *)indexPath {
  return YES;
}

- (BOOL)collectionView:(UICollectionView *)collectionView
       itemAtIndexPath:(NSIndexPath *)fromIndexPath
    canMoveToIndexPath:(NSIndexPath *)toIndexPath {
  return YES;
}
//// - LXReorderableCollectionViewDelegateFlowLayout
/// methods-------------------------------------------------------------------------------

//// ELCImagePickerControllerDelegate
/// Methods----------------------------------------------------------------------------------------------
- (void)elcImagePickerController:(ELCImagePickerController *)picker
   didFinishPickingMediaWithInfo:(NSArray *)info {
  NSArray *viewsToRemove = [self.view subviews];
  for (UIView *v in viewsToRemove) {
    if (v.tag == 50) {
      [v removeFromSuperview];
    }
  }
  self.firsttimeselectphoto = NO;
  [self dismissViewControllerAnimated:NO completion:nil];

  DDLogInfo(@"info%@", info);
  NSMutableArray *images = [[NSMutableArray alloc] initWithCapacity:10];
  for (NSDictionary *dict in info) {
    if ([dict objectForKey:UIImagePickerControllerMediaType] ==
        ALAssetTypePhoto) {
      if ([dict objectForKey:UIImagePickerControllerOriginalImage]) {
        UIImage *image =
            [dict objectForKey:UIImagePickerControllerOriginalImage];
        [images addObject:image];

      } else {
        DDLogInfo(@"UIImagePickerControllerReferenceURL = %@", dict);
      }
    } else if ([dict objectForKey:UIImagePickerControllerMediaType] ==
               ALAssetTypeVideo) {
      if ([dict objectForKey:UIImagePickerControllerOriginalImage]) {
        UIImage *image =
            [dict objectForKey:UIImagePickerControllerOriginalImage];

        [images addObject:image];

      } else {
        DDLogInfo(@"UIImagePickerControllerReferenceURL = %@", dict);
      }
    } else {
      DDLogInfo(@"Uknown asset type");
    }
  }

  while (images.count < 10) {
    if (!self.CollectionViewAddbuttonexist) {
      [images addObject:[UIImage imageNamed:@"collectionviewaddbutton"]];

      self.CollectionViewAddbuttonexist = YES;
    } else {
      [images addObject:[UIImage imageNamed:@"emptyphoto"]];
    }
  }

  self.chatroomchosenImages = images;
  DDLogInfo(@"chatroomchosenImages-->%@", self.chatroomchosenImages);

  if (![self IsEmpty:self.chatroomchosenImages]) {
    self.selectedcoverimageindexpath =
        [NSIndexPath indexPathForRow:0 inSection:0];
    [self.selectedimage setImage:[self.chatroomchosenImages firstObject]];
    self.selectedimage.contentMode = UIViewContentModeScaleAspectFit;
    self.selectedimage.clipsToBounds = YES;
  }
}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker {
  if (self.firsttimeselectphoto) {
    self.firsttimeselectphoto = NO;

    [self dismissViewControllerAnimated:NO completion:nil];

    [self.navigationController popViewControllerAnimated:YES];

  } else {
    [self dismissViewControllerAnimated:NO completion:nil];
  }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:
    (UIInterfaceOrientation)toInterfaceOrientation {
  if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
    return YES;
  } else {
    return toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown;
  }
}
//// ELCImagePickerControllerDelegate
/// Methods----------------------------------------------------------------------------------------------

@end