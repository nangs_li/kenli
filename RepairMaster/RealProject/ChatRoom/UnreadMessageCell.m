//
//  ChatMessageTableViewCell.m
//  sample-chat
//
//  Created by Igor Khomenko on 10/19/13.
//  Copyright (c) 2013 Igor Khomenko. All rights reserved.
//

#import "UnreadMessageCell.h"

#define padding 20

@implementation UnreadMessageCell

static NSDateFormatter *messageDateFormatter;
static UIImage *orangeBubble;
static UIImage *aquaBubble;
- (void)awakeFromNib {
  // Initialization code
}
+ (void)initialize {
  [super initialize];
}
#pragma mark Message SetUp Method
+ (CGFloat)heightForCellWithMessage:(DBQBChatMessage *)message {
  return 30.0f;
}

- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString *)reuseIdentifier

{
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  if (self) {
  }

  return self;
}

- (void)configureCellWithMessage:(DBQBChatMessage *)message {
  self.unreadlabel.text =
      [NSString stringWithFormat:@"%@ UNREAD MESSAGES", message.Text];

  //// check is unread label

  // if ([msg.text  rangeOfString:@"UNREAD MESSAGES"].location == NSNotFound) {

  /*

   if (![self IsEmpty:[[ChatService shared]
   .messages objectForKey:self.dialog.ID]]) {

   if ([[ChatService shared] messagsForDialogId:self.dialog.ID].count >
   indexPath.item) {

   DBQBChatMessage *message =
   [[[ChatService shared] messagsForDialogId:self.dialog.ID]
   objectAtIndex:indexPath.item];

   NSURL *url =
   [NSURL URLWithString:[message.CustomParameters
   objectForKey:@"photourl"]];
   CGSize size ;
   if ([UIDevice currentDevice].userInterfaceIdiom ==
   UIUserInterfaceIdiomPad) {
   size =CGSizeMake(315.0f, 225.0f);
   }else{

   size =CGSizeMake(210.0f, 150.0f);
   }
   id<JSQMessageMediaData> messageMedia = [msg media];

   UIImageView *imageView = [[UIImageView alloc] init];
   // imageView.frame = CGRectMake(0.0f, 0.0f, size.width, size.height);
   //  imageView.contentMode = UIViewContentModeScaleAspectFill;
   // imageView.clipsToBounds = YES;
   [imageView setImageWithURL:url];



   cell.mediaView= [messageMedia mediaView];
   }

   }
   */

  /*
   }else{


   self.unreadlabelview = [[UIView alloc]init];
   [self.unreadlabelview setFrame:cell.bounds];
   [ self.unreadlabelview setBackgroundColor:[UIColor colorWithWhite: 0.95
   alpha:1]];
   cell.datelabel = [[UILabel alloc] init];
   [cell.datelabel setFrame:cell.bounds];
   [cell.datelabel
   setText:msg.text];
   [cell.datelabel setFont:[UIFont systemFontOfSize:18]];
   [cell.datelabel setTextAlignment:NSTextAlignmentCenter];
   [cell.datelabel setTextColor:[UIColor grayColor]];
   [self.unreadlabelview addSubview:cell.datelabel];
   self.unreadlabelview.tag=50;
   [cell addSubview:self.unreadlabelview];
   self.unreadlabelcell=cell;
   }
   */

  //  ChatRoomCoverAgentView* myViewObject = [[[NSBundle mainBundle]
  //  loadNibNamed:@"ChatRoomCoverAgentView" owner:self options:nil]
  //  objectAtIndex:0];
  // myViewObject.follower.text=@"534543";
  //   [cell addSubview:myViewObject];

  /*
   cell.datelabel = [[UILabel alloc] init];
   cell.datelabel.tag = 50;
   [cell.datelabel
   setText:[self.messageDateLabelFormatter stringFromDate:jsqmessage.date]];
   [cell.datelabel setFont:[UIFont systemFontOfSize:11]];

   //// set up rightsidemessagedatelabel
   if (isOutgoingMessage) {
   [cell.datelabel
   setFrame:CGRectMake(230, cell.frame.size.height - 20, 60, 10)];
   cell.datelabel.textAlignment = NSTextAlignmentRight;
   if (cell.mediaView != nil) {
   // [cell.datelabel setFrame:CGRectMake(240, cell.frame.size.height-30,
   50,
   // 10)];
   [cell.datelabel setFrame:CGRectMake(0, 0, 50, 10)];

   [cell.datelabel setTextColor:[UIColor blackColor]];

   UIView *overlay = [[UIView alloc]
   initWithFrame:CGRectMake(240,
   cell.frame.size.height - 30, 40, 10)];
   [overlay
   setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1
   alpha:0.1]];
   overlay.tag = 50;
   [overlay addSubview:cell.datelabel];
   [cell addSubview:overlay];
   } else {
   [cell addSubview:cell.datelabel];
   [cell.datelabel setTextColor:[UIColor blackColor]];
   }
   //// set up rightsidemessage tickimageview

   cell.tickimageview = [[UIImageView alloc] init];
   cell.tickimageview.tag = 50;
   if (cell.mediaView != nil) {
   [cell.tickimageview
   setFrame:CGRectMake(290, cell.frame.size.height - 30, 10, 10)];

   } else {
   [cell.tickimageview
   setFrame:CGRectMake(290, cell.frame.size.height - 20, 10, 10)];
   }
   [cell addSubview:cell.tickimageview];

   if (jsqmessage.ReadStatus) {
   [cell.tickimageview setImage:[UIImage imageNamed:@"doublegreentick"]];
   } else {
   if (jsqmessage.DidDeliverStatus) {
   [cell.tickimageview setImage:[UIImage imageNamed:@"greytick"]];
   } else {
   if (jsqmessage.NumberofDeliverStatus > 1) {
   [cell.tickimageview setImage:[UIImage
   imageNamed:@"onegreytick2"]];
   } else {
   if (jsqmessage.ErrorStatus) {
   } else {
   }
   }
   }
   }

   } else {
   //// set up leftidemessagedatelabel
   [cell.datelabel
   setFrame:CGRectMake(10, cell.frame.size.height - 20, 60, 10)];
   cell.datelabel.textAlignment = NSTextAlignmentRight;
   if (cell.mediaView != nil) {
   // [cell.datelabel setFrame:CGRectMake(20, cell.frame.size.height -
   30,
   // 50, 10)];
   [cell.datelabel setFrame:CGRectMake(0, 0, 60, 10)];
   [cell.datelabel setTextColor:[UIColor blackColor]];

   UIView *overlay = [[UIView alloc]
   initWithFrame:CGRectMake(10, cell.frame.size.height
   - 30, 40, 10)];
   [overlay
   setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1
   alpha:0.1]];
   overlay.tag = 50;
   [overlay addSubview:cell.datelabel];

   [cell addSubview:overlay];
   // [cell addSubview:cell.datelabel];
   } else {
   [cell addSubview:cell.datelabel];
   [cell.datelabel setTextColor:[UIColor blackColor]];
   }
   }
   */
}

@end
