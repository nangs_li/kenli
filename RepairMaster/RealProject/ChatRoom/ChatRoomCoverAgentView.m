//
//  TableViewCell.m
//  S18
//
//  Created by ios Developer 5 on 4/8/14.
//  Copyright (c) 2014 ios Developer 5. All rights reserved.
//

#import "ChatRoomCoverAgentView.h"

#import <QuartzCore/QuartzCore.h>

@implementation ChatRoomCoverAgentView

- (void)baseInit {
}

- (id)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    [self baseInit];
  }
  return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
  if ((self = [super initWithCoder:aDecoder])) {
    [self baseInit];
  }
  return self;
}
#pragma mark Message SetUp Method
- (void)setupperonalimagelayer {
  DDLogInfo(@"setupperonalimagelayer");
  self.personalimage.layer.cornerRadius =
      self.personalimage.frame.size.width / 2;
  self.personalimage.clipsToBounds = YES;
}

@end
