//
//  TableViewCell.m
//  S18
//
//  Created by ios Developer 5 on 4/8/14.
//  Copyright (c) 2014 ios Developer 5. All rights reserved.
//

#import "CellDateLabelView.h"

#import <QuartzCore/QuartzCore.h>
#import "JSQMessage.h"
@implementation CellDateLabelView

- (void)baseInit {
}

- (id)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    [self baseInit];
  }
  return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
  if ((self = [super initWithCoder:aDecoder])) {
    [self baseInit];
  }
  return self;
}
#pragma mark Message SetUp Method
- (void)setupTickImageView:(JSQMessage *)jsqmessage {
  if (jsqmessage.ReadStatus &&
      [self IsEmpty:[jsqmessage.CustomParameters
                        objectForKey:@"chatroomshowreadreceipts"]]) {
    [self.tickimageview setImage:[UIImage imageNamed:@"doublegreentick"]];
  } else {
    if (jsqmessage.DidDeliverStatus) {
      [self.tickimageview setImage:[UIImage imageNamed:@"greytick"]];
    } else {
      if (jsqmessage.NumberofDeliverStatus > 1) {
        [self.tickimageview setImage:[UIImage imageNamed:@"onegreytick2"]];
      } else {
        if (jsqmessage.ErrorStatus) {
        } else {
        }
      }
    }
  }
}
////check check isempty
- (BOOL)IsEmpty:(id)thing {
  return thing == nil || ([thing respondsToSelector:@selector(length)] &&
                          [(NSData *)thing length] == 0) ||
         ([thing respondsToSelector:@selector(count)] &&
          [(NSArray *)thing count] == 0);
}
@end
