
//
//  AppDelegate.m
//  FBLoginUIControlSample
//
//  Created by Luz Caballero on 9/17/13.
//  Copyright (c) 2013 Facebook Inc. All rights reserved.
//
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Fabric/Fabric.h>
#import <Fabric/Fabric.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <TwitterKit/TwitterKit.h>
#import "AppDelegate.h"
#import "BaseViewController.h"
#import "Chatroom.h"
#import "DBQBChatDialog.h"
#import "DBQBChatMessage.h"
#import "DialogsViewController.h"
#import "Finishprofilehavelisting.h"
#import "MMDrawerController.h"
#import "RDVTabBarController.h"
#import "RDVTabBarItem.h"
#import "RDVTabBarItem.h"
#import "Reachability.h"
#import "Setting.h"
#import "UIView+Borders.h"
#import "WeiboSDK.h"
#import "WeiboUser.h"

#import <Branch/Branch.h>
#import "DeepLinkActionHanderModel.h"
#import "Finishprofilehavelisting.h"
#import "PressChatButtonModel.h"
#import "SearchProductPage.h"
@interface AppDelegate ()

@end
@implementation AppDelegate

#pragma mark ios application method start--------------------------------------------------------------------------------------------
+ (AppDelegate *)getAppDelegate {
  return (AppDelegate *)[UIApplication sharedApplication].delegate;
}
- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  self.testServerAddress = serveraddress;

#pragma mark DDASLLogger tool
  setenv("XcodeColors", "YES", 0);
  [[DDASLLogger sharedInstance]
      setLogFormatter:[[MyCustomLogFormatter alloc] init]];
  [[DDTTYLogger sharedInstance]
      setLogFormatter:[[MyCustomLogFormatter alloc] init]];
  [DDLog addLogger:[DDASLLogger sharedInstance]];
  [DDLog addLogger:[DDTTYLogger sharedInstance]];
  [[DDTTYLogger sharedInstance] setColorsEnabled:YES];
  UIColor *pink = [UIColor colorWithRed:(255 / 255.0)
                                  green:(58 / 255.0)
                                   blue:(159 / 255.0)
                                  alpha:1.0];
  [[DDTTYLogger sharedInstance] setForegroundColor:pink
                                   backgroundColor:nil
                                           forFlag:DDLogFlagDebug];
  UIColor *gray = [UIColor colorWithRed:(156 / 255.0)
                                  green:(158 / 255.0)
                                   blue:(161 / 255.0)
                                  alpha:1.0];
  [[DDTTYLogger sharedInstance] setForegroundColor:gray
                                   backgroundColor:nil
                                           forFlag:DDLogFlagVerbose];
  UIColor *yellow = [UIColor colorWithRed:(176 / 255.0)
                                    green:(178 / 255.0)
                                     blue:(16 / 255.0)
                                    alpha:1.0];
  [[DDTTYLogger sharedInstance] setForegroundColor:yellow
                                   backgroundColor:nil
                                           forFlag:DDLogFlagInfo];
  [QBSettings setLogLevel:QBLogLevelInfo];
#pragma ORM DBAccess Database
  [DBAccess setDelegate:self];
  [DBAccess openDatabaseNamed:@"myApplication"];

  UILocalNotification *notification = [launchOptions
      objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
  if (notification) {
    DDLogInfo(@"app recieved notification from remote%@", notification);
    [self application:application
        didReceiveRemoteNotification:(NSDictionary *)notification];

  } else {
    DDLogInfo(@"app did not recieve notification");
  }

#pragma ORM Quickblox setup

  [QBSettings setAccountKey:@"wnw7kHtdBbdd8vRL8VVw"];

  [QBSettings setApplicationID:17438];

  [QBSettings setAuthKey:@"hZHB5pMwz2BcauL"];
  [QBSettings setAuthSecret:@"tUD6SrX-dyEaP5k"];
  [QBSettings setAutoReconnectEnabled:YES];

#pragma ORM DBAccess Database
  [DBAccess setDelegate:self];
  [DBAccess openDatabaseNamed:@"myApplication"];

  DBResultSet *r;
#pragma mark setup DBLoginRecord from db

  r = [[[DBLoginRecord query] limit:1] fetch];

  for (DBLoginRecord *p in r) {
    [LoginRecordModel shared].myLoginRecord =
        [NSKeyedUnarchiver unarchiveObjectWithData:p.LoginRecrod];
    DDLogVerbose(@"DBLoginRecord-->%@",
                 [LoginRecordModel shared].myLoginRecord);

    [[LoginRecordModel shared]
        LoginApiByEmail:[LoginRecordModel shared].myLoginRecord.username
        Password:[LoginRecordModel shared].myLoginRecord.password
        success:^(LoginRecord *myLoginRecord) {

        }
        failure:^(AFHTTPRequestOperation *operation, NSError *error,
                  NSString *errorMessage, int errorStatus, NSString *alert,
                  NSString *ok) {

          UIAlertView *alertView =
              [[UIAlertView alloc] initWithTitle:alert
                                         message:errorMessage
                                        delegate:nil
                               cancelButtonTitle:ok
                               otherButtonTitles:nil];
          [alertView show];

        }];
  }
  if (![self IsEmpty:[LoginRecordModel shared].myLoginRecord.QBID]) {
    [[ChatService shared] quickBloxToTalLoginOnly];
  }

  r = [[[[[DBProductDetail query]
      whereWithFormat:@"eid = %@",
                      [NSString
                          stringWithFormat:@"%@", [LoginRecordModel shared]
                                                      .myLoginRecord.eid]]
      orderByDescending:@"Date"] limit:1] fetch];

  for (DBProductDetail *p in r) {
    [AddProductByEidModel shared].myProductDetail =
        [NSKeyedUnarchiver unarchiveObjectWithData:p.ProductDetail];
    DDLogVerbose(@"DBProductDetail-->%@",
                 [AddProductByEidModel shared].myProductDetail);
  }

  DDLogVerbose(@"self.myLoginRecord.eid-->%@",
               [LoginRecordModel shared].myLoginRecord.eid);
  [self setWindow:[[UIWindow alloc]
                      initWithFrame:[[UIScreen mainScreen] bounds]]];

  [self setupViewControllers];
  [self customizeInterface];
  [self.window setBackgroundColor:[UIColor whiteColor]];

  [self.window setRootViewController:[self getTabBarController]];

  [self.window makeKeyAndVisible];

  [Fabric with:@[ TwitterKit ]];
  /*
    Branch *branch = [Branch getInstance];
    [branch
     initSessionWithLaunchOptions:launchOptions
     andRegisterDeepLinkHandler:^(NSDictionary *params, NSError *error) {
         // params are the deep linked params associated with the link that
         // the
         // user clicked before showing up.
         DDLogDebug(@"deep link data: %@", [params description]);
         if (![self IsEmpty:[params objectForKey:@"QBID"]]) {
             if ([self IsEmpty:[LoginRecordModel shared]
                  .myLoginRecord.QBID]) {
                 [DeepLinkActionHanderModel shared].QBID =
                 [params objectForKey:@"QBID"];
                 [DeepLinkActionHanderModel shared].username =
                 [params objectForKey:@"username"];

             } else {
                 [self reLoadTabBarControllerAndOpenChatRoomFromQBID:
                  [params objectForKey:
                   @"QBID"] username:[params
                                        objectForKey:
                                        @"username"]];
             }
         }
     }];
*/
  return [[FBSDKApplicationDelegate sharedInstance] application:application
                                  didFinishLaunchingWithOptions:launchOptions];
}
- (void)applicationWillTerminate:(UIApplication *)application {
  [[NSNotificationCenter defaultCenter]
      postNotificationName:kNotificationChatDidAccidentallyDisconnect
                    object:nil];

  [[ChatService shared] logout];
}
- (void)applicationDidEnterBackground:(UIApplication *)application {
  // Use this method to release shared resources, save user data, invalidate
  // timers, and store enough application state information to restore your
  // application to its current state in case it is terminated later.
  // If your application supports background execution, this method is called
  // instead of applicationWillTerminate: when the user quits.

  // Logout from chat
  //

  [[NSNotificationCenter defaultCenter]
      postNotificationName:kNotificationChatDidAccidentallyDisconnect
                    object:nil];

  [[ChatService shared] logout];
}
- (void)applicationWillEnterForeground:(UIApplication *)application {
  // Called as part of the transition from the background to the inactive state;
  // here you can undo many of the changes made on entering the background.

  // Login to QuickBlox Chat
  //

  [[ChatService shared] loginWithUser:[ChatService shared].currentUser
                      completionBlock:^{

                      }];
}
#pragma mark push Notification
- (void)application:(UIApplication *)application
    didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
  self.devicetoken =
      [[[deviceToken description] stringByReplacingOccurrencesOfString:@"<"
                                                            withString:@""]
          stringByReplacingOccurrencesOfString:@">"
                                    withString:@""];
  DDLogInfo(@"self.devicetoken-->%@", self.devicetoken);
  // Subscribe to push notifications
  //

  NSString *deviceIdentifier =
      [[[UIDevice currentDevice] identifierForVendor] UUIDString];
  //
  [QBRequest registerSubscriptionForDeviceToken:deviceToken
      uniqueDeviceIdentifier:deviceIdentifier
      successBlock:^(QBResponse *response, NSArray *subscriptions) {

      }
      errorBlock:^(QBError *error){

      }];
}
- (void)application:(UIApplication *)application
    didRegisterUserNotificationSettings:
        (UIUserNotificationSettings *)notificationSettings {
  // register to receive notifications
  [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application
    handleActionWithIdentifier:(NSString *)identifier
         forRemoteNotification:(NSDictionary *)userInfo
             completionHandler:(void (^)())completionHandler {
  DDLogInfo(@"notificationopen");
  // handle the actions
  if ([identifier isEqualToString:@"declineAction"]) {
  } else if ([identifier isEqualToString:@"answerAction"]) {
  }
}
- (void)applicationDidBecomeActive:(UIApplication *)application {
  [FBSDKAppEvents activateApp];
  application.applicationIconBadgeNumber = 0;
}
- (void)application:(UIApplication *)application
    didReceiveRemoteNotification:(NSDictionary *)userInfo
          fetchCompletionHandler:
              (void (^)(UIBackgroundFetchResult))completionHandler {
  [UIApplication sharedApplication].applicationIconBadgeNumber =
      [UIApplication sharedApplication].applicationIconBadgeNumber++;
}

- (void)application:(UIApplication *)application
    didReceiveRemoteNotification:(NSDictionary *)userInfo {
}
- (void)application:(UIApplication *)application
    didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
  DDLogInfo(@"didFailToRegisterForRemoteNotificationsWithError-->%@", error);
}
#pragma mark - openURL
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
  return YES;
}
#pragma mark ios application method   end----------------------------------------------------------------------------------------------

#pragma mark - #getTabBarController--------------------------------------------------------------
- (RDVTabBarController *)getTabBarController {
  return (RDVTabBarController *)self.viewController;
}
#pragma mark - TabBarControllerMethods
- (void)setupViewControllers {
#pragma mark setup addresssearch page
  UIViewController *firstNavigationController = [[UINavigationController alloc]
      initWithRootViewController:[[SearchProductPage alloc] init]];

#pragma mark setup DialogsViewController
  [ChatService shared];

  DialogsViewController *dialogsViewController =
      [[DialogsViewController alloc] init];

  UIViewController *thirdNavigationController = [[UINavigationController alloc]
      initWithRootViewController:dialogsViewController];

  UIViewController *fourNavigationController = [[UINavigationController alloc]
      initWithRootViewController:[[Finishprofilehavelisting alloc] init]];

  BaseViewController *fireViewController = [[Setting alloc] init];
  // UIViewController *fireViewController = [[Filterpage alloc] init];
  UIViewController *fireNavigationController = [[UINavigationController alloc]
      initWithRootViewController:fireViewController];

  RDVTabBarController *tabBarController = [[RDVTabBarController alloc] init];
  [tabBarController setViewControllers:@[
    firstNavigationController,
    thirdNavigationController,
    fourNavigationController,
    fireNavigationController
  ]];

  self.viewController = tabBarController;

  [self customizeTabBarForController:tabBarController];
}
- (void)customizeTabBarForController:(RDVTabBarController *)tabBarController {
  UIImage *finishedImage = [UIImage imageNamed:@"tabbar_selected_bg"];
  UIImage *unfinishedImage = [UIImage imageNamed:@"tabbar_normal_bg"];
  NSArray *tabBarItemImages =
      @[ @"nav-search", @"nav-inbox", @"nav-profile", @"nav-setting" ];

  NSInteger index = 0;
  for (RDVTabBarItem *item in [[tabBarController tabBar] items]) {
    [item setBackgroundSelectedImage:finishedImage
                 withUnselectedImage:unfinishedImage];
    UIImage *selectedimage = [UIImage
        imageNamed:[NSString
                       stringWithFormat:@"%@-hover", [tabBarItemImages
                                                         objectAtIndex:index]]];
    UIImage *unselectedimage = [UIImage
        imageNamed:[NSString stringWithFormat:@"%@", [tabBarItemImages
                                                         objectAtIndex:index]]];
    [item setFinishedSelectedImage:selectedimage
        withFinishedUnselectedImage:unselectedimage];

    index++;
  }
}
- (void)customizeInterface {
  UINavigationBar *navigationBarAppearance = [UINavigationBar appearance];

  UIImage *backgroundImage = nil;
  NSDictionary *textAttributes = nil;

  if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
    backgroundImage = [UIImage imageNamed:@"navigationbar_background_tall"];

    textAttributes = @{
      NSFontAttributeName : [UIFont boldSystemFontOfSize:18],
      NSForegroundColorAttributeName : [UIColor blackColor],
    };
  } else {
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_7_0
    backgroundImage = [UIImage imageNamed:@"navigationbar_background"];

    textAttributes = @{
      UITextAttributeFont : [UIFont boldSystemFontOfSize:18],
      UITextAttributeTextColor : [UIColor blackColor],
      UITextAttributeTextShadowColor : [UIColor clearColor],
      UITextAttributeTextShadowOffset :
          [NSValue valueWithUIOffset:UIOffsetZero],
    };
#endif
  }

  [navigationBarAppearance setBackgroundImage:backgroundImage
                                forBarMetrics:UIBarMetricsDefault];
  [navigationBarAppearance setTitleTextAttributes:textAttributes];
}
#pragma mark - #getTabBarController--------------------------------------------------------------

- (UIWindow *)getuiview {
  return self.window;
}
#pragma mark CGcolor
- (UIColor *)Greycolor {
  return [UIColor colorWithRed:(173 / 255.f)
                         green:(175 / 255.f)
                          blue:(177 / 255.f)
                         alpha:1.0];
}
- (UIColor *)Greencolor {
  return [UIColor colorWithRed:(20 / 255.f)
                         green:(226 / 255.f)
                          blue:(160 / 255.f)
                         alpha:1.0];
}
#pragma mark Futurafont
- (UIFont *)Futurafont:(CGFloat)size {
  return [UIFont fontWithName:@"Futura" size:size];
}
#pragma mark Setborder
- (void)setborder:(UIView *)uiview width:(CGFloat)width {
  [uiview addLeftBorderWithWidth:width andColor:[self Greycolor]];
  [uiview addTopBorderWithHeight:width andColor:[self Greycolor]];
  [uiview addRightBorderWithWidth:width andColor:[self Greycolor]];
  [uiview addBottomBorderWithHeight:width andColor:[self Greycolor]];
}

#pragma mark checkisnumber
- (BOOL)checkisnumber:(UITextField *)textfield {
  if ([[NSScanner scannerWithString:textfield.text] scanFloat:NULL]) {
    return true;
  } else {
    return false;
  }
}

#pragma didReceiveWeiboResponse
- (NSString *)checkisnsnumberchangetonsstring:(id)number {
  if ([number isKindOfClass:[NSNumber class]]) {
    return [number stringValue];
  } else {
    return number;
  }
  return [number stringValue];
}

#pragma setupcornerRadius
- (void)setupcornerRadius:(UIImageView *)personalimage {
  personalimage.layer.cornerRadius = personalimage.frame.size.width / 2;
  personalimage.clipsToBounds = YES;

  DDLogInfo(@"cornerRadius");
}
#pragma check empty
- (BOOL)IsEmpty:(id)thing {
  return thing == nil || ([thing respondsToSelector:@selector(length)] &&
                          [(NSData *)thing length] == 0) ||
         ([thing respondsToSelector:@selector(count)] &&
          [(NSArray *)thing count] == 0);
}
- (BOOL)networkConnection {
  Reachability *networkReachability =
      [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
  if (networkStatus == NotReachable) {
    UIAlertView *alert =
        [[UIAlertView alloc] initWithTitle:@"Alert"
                                   message:@"Not Network,Please Turn On Wifi"
                                  delegate:nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];

    [alert show];

    DDLogInfo(@"There IS NO internet connection");
    return NO;
  } else {
    DDLogInfo(@"There IS internet connection");
    return YES;
  }
}

#pragma mark chatPart----------------------------------------------------------------------------------------
#pragma mark setupspokenlanguagelist
#pragma quickbloxregisterpushnotification
- (void)quickbloxregisterpushnotification {
  [self registerForRemoteNotifications];
}
- (void)registerForRemoteNotifications {
#pragma mark push  Notification
  if ([[UIApplication sharedApplication]
          respondsToSelector:@selector(registerUserNotificationSettings:)]) {
    [[UIApplication sharedApplication]
        registerUserNotificationSettings:
            [UIUserNotificationSettings
                settingsForTypes:(UIUserNotificationTypeSound |
                                  UIUserNotificationTypeAlert |
                                  UIUserNotificationTypeBadge)
                      categories:nil]];

  } else {
    [[UIApplication sharedApplication]
        registerForRemoteNotificationTypes:(UIUserNotificationTypeBadge |
                                            UIUserNotificationTypeSound |
                                            UIUserNotificationTypeAlert)];
  }
}

#pragma mark handle quickblox error
- (void (^)(QBResponse *))handleError {
  return ^(QBResponse *response) {

    DDLogError(@"[QBRequest handleError %@", response);

  };
}

#pragma mark chatPart----------------------------------------------------------------------------------------

- (void)reLoadTabBarControllerAndOpenChatRoomFromQBID:(NSString *)QBID
                                             username:(NSString *)username {
  for (id obj in [self.window subviews]) {
    [obj removeFromSuperview];
  }
  self.viewController = nil;
  [self setupViewControllers];
  [self.window setRootViewController:self.viewController];
  [self.window makeKeyAndVisible];
  [[self getTabBarController] setSelectedIndex:2];

  [self openChatRoomFromQBID:QBID username:username];
}

- (void)openChatRoomFromQBID:(NSString *)QBID username:(NSString *)username {
  [[PressChatButtonModel shared] pressChatButtonModelByQBID:QBID
      username:username
      success:^(Chatroom *chatRoom) {
        [DeepLinkActionHanderModel shared].QBID = nil;
        [DeepLinkActionHanderModel shared].username = nil;

        UINavigationController *selectedViewControllerUINavigationController =
            (UINavigationController *)[self getTabBarController]
                .selectedViewController;
        [selectedViewControllerUINavigationController
            pushViewController:chatRoom
                      animated:YES];

      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error,
                NSString *errorMessage, RequestErrorStatus errorStatus,
                NSString *alert, NSString *ok) {
        [DeepLinkActionHanderModel shared].QBID = nil;
        [DeepLinkActionHanderModel shared].username = nil;

        [self
            handleModelReturnErrorShowUIAlertViewByView:self.viewController.view
                                           errorMessage:errorMessage
                                            errorStatus:errorStatus
                                                  alert:alert
                                                     ok:ok];
      }];
}
- (void)handleModelReturnErrorShowUIAlertViewByView:(UIView *)view
                                       errorMessage:(NSString *)errorMessage
                                        errorStatus:(int)errorStatus
                                              alert:(NSString *)alert
                                                 ok:(NSString *)ok {
  [MBProgressHUD hideHUDForView:view animated:YES];

  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alert
                                                      message:errorMessage
                                                     delegate:nil
                                            cancelButtonTitle:ok
                                            otherButtonTitles:nil];
  [alertView show];
}
@end
