//
//  AppDelegate.h
//  FBLoginUIControlSample
//
//  Created by Luz Caballero on 9/17/13.
//  Copyright (c) 2013 Facebook Inc. All rights reserved.
//

// ash provide color
// Green - #14e2a0    20  226 160
// Grey - #adafb1    173  175 177

#import <UIKit/UIKit.h>
#import "RDVTabBarController.h"
#import "SlidingViewManager.h"

#import <DBAccess/DBAccess.h>
#import "HandleServerReturnString.h"
#import "MBProgressHUD.h"
#import "WBHttpRequest.h"
#import "WeiboSDK.h"

#import "DBLoginRecord.h"
#import "DBProductDetail.h"
#import "DBProductListPhoto.h"
#import "LoginRecord.h"
#import "ProductDetailClass.h"
#import "ProductDetailClass.h"
#import "ProductListPhoto.h"
#import "ProductListPhoto.h"
@class LoginUIViewController;
@class Finishprofile;
@class Chatroom;
@interface AppDelegate : UIResponder<UIApplicationDelegate, DBDelegate>
#pragma mark LoginPage-----------------------------------------------------------------------------------------------
@property(strong, nonatomic) LoginUIViewController *loginuiviewcontroller;
@property(strong, nonatomic) UIWindow *window;
+ (AppDelegate *)getAppDelegate;
#pragma mark api[AppDelegate getAppDelegate].testServerAddress
@property(strong, nonatomic) NSString *testServerAddress;
#pragma mark quickbloxlogin,password
@property(strong, nonatomic) NSString *quickbloxlogin;
@property(strong, nonatomic) NSString *quickbloxpassword;
@property(nonatomic, strong) NSString *devicetoken;
- (void)quickbloxregisterpushnotification;
- (void)registerForRemoteNotifications;
#pragma mark LoginPage end ------------------------------------------------------------------------------------------

#pragma mark TabBarController----------------------------------------------------------------------------------------
- (UIWindow *)getuiview;
- (RDVTabBarController *)getTabBarController;
@property(strong, nonatomic) UIViewController *viewController;
#pragma customizeTabBarForController;
- (void)customizeTabBarForController:(RDVTabBarController *)tabBarController;
- (void)setupViewControllers;
- (void)customizeInterface;
#pragma mark TabBarController-------------------------------------------------------------------------------------

#pragma mark CallServerAPIEngine-------------------------------------------------------------------------------------
#pragma networkconnection
- (BOOL)networkConnection;
#pragma mark CallServerAPIEngine------------------------------------------------------------------------------------

#pragma mark SupportMethod-------------------------------------------------------------------------------------------
#pragma mark Futura Font
- (UIFont *)Futurafont:(CGFloat)size;
#pragma mark CGcolor
- (UIColor *)Greycolor;
- (UIColor *)Greencolor;
#pragma mark Setborder
- (void)setborder:(UIView *)uiview width:(CGFloat)width;
#pragma mark TextFieldsetleftinset
- (void)textfieldsetleftinset:(UITextField *)textfield;
#pragma mark buttonsetleftinset
- (void)buttonsetleftinset:(UIButton *)button;
#pragma mark checkisnumber
- (BOOL)checkisnumber:(UITextField *)textfield;
- (NSString *)checkisnsnumberchangetonsstring:(id)number;
#pragma mark hud
@property(strong, nonatomic) MBProgressHUD *hud;
#pragma mark set delegate store posting data to nil
- (void)resetPostingDelegateData;
#pragma mark checkIsEmpty
- (BOOL)IsEmpty:(id)thing;
#pragma setupcornerRadius
- (void)setupcornerRadius:(UIImageView *)personalimage;
#pragma mark SupportMethod-------------------------------------------------------------------------------------------

@end
