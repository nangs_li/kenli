//
//  ViewController.h
//  TAPageControl
//
//  Created by Tanguy Aladenise on 2015-01-21.
//  Copyright (c) 2015 Tanguy Aladenise. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "ProductDetailClass.h"
@interface ProductDetailPage : BaseViewController
@property(strong, nonatomic) ProductDetailClass *ProductDetail;
@property(strong, nonatomic) IBOutlet UILabel *productName;
@property(strong, nonatomic) IBOutlet UILabel *price;
@property(strong, nonatomic) IBOutlet UILabel *type;
@property(strong, nonatomic) IBOutlet UILabel *likecount;
@property(strong, nonatomic) IBOutlet UITextView *productDescription;

- (void)setupProductDetailPageFromProductDetail;

@end
