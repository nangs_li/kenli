//
//  d.h
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Facebook Inc. All rights reserved.
//

#ifndef abc_d_h
#define abc_d_h

#endif

#import <DBAccess/DBAccess.h>

@interface DBQBChatMessage : DBObject
//+ (d *)shared;
@property(strong, nonatomic) NSString *MessageID;
@property(strong, nonatomic) NSString *DialogID;
@property(strong, nonatomic) NSString *QBID;

// send message type =0, receive type =1;
@property(assign, nonatomic) int MessageType;
@property(assign, nonatomic) int eid;
@property(nonatomic, assign) NSUInteger RecipientID;
@property(nonatomic, assign) NSUInteger SenderID;
@property(strong, nonatomic) NSDate *DateSent;
@property(strong, nonatomic) NSDate *InsertDBDateTime;
@property(strong, nonatomic) NSDate *SendToServerDate;
@property(strong, nonatomic) NSData *QBChatHistoryMessage;
@property(strong, nonatomic) NSString *PhotoURL;
@property(strong, nonatomic) NSString *Text;
@property(strong, nonatomic) NSMutableDictionary *CustomParameters;
@property(nonatomic, retain) NSArray *Attachments;
@property(assign, nonatomic) BOOL PhotoType;
// New ==0 ,Sending ==1,Sent==2,Error=3
@property(assign, nonatomic) int NumberofDeliverStatus;
// Error Status

@property(assign, nonatomic) BOOL DidDeliverStatus;
@property(assign, nonatomic) BOOL ErrorStatus;
@property(assign, nonatomic) BOOL DrawStatus;
@property(assign, nonatomic) BOOL ReadStatus;
@property(assign, nonatomic) int Retrycount;

@end
