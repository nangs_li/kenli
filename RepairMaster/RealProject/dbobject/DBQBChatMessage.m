//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Facebook Inc. All rights reserved.
//

#import "DBQBChatMessage.h"

@implementation DBQBChatMessage
@dynamic MessageID, DialogID, QBID, MessageType, eid, RecipientID, SenderID,
    DateSent, InsertDBDateTime, SendToServerDate, QBChatHistoryMessage,
    PhotoURL, Text, CustomParameters, Attachments, PhotoType,
    NumberofDeliverStatus, DidDeliverStatus, ErrorStatus, DrawStatus,
    ReadStatus, Retrycount;

+ (DBIndexDefinition*)indexDefinitionForEntity {
  /* create an index definition object */
  DBIndexDefinition* idx = [DBIndexDefinition new];

  /* now specify which properties are going to be indexed */
  [idx addIndexForProperty:@"MessageID"
             propertyOrder:DBIndexSortOrderAscending];
  [idx addIndexForProperty:@"DialogID" propertyOrder:DBIndexSortOrderAscending];
  [idx addIndexForProperty:@"PhotoType"
             propertyOrder:DBIndexSortOrderAscending];

  return idx;
}

@end