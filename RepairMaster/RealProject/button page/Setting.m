//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "Aboutthisversion.h"
#import "BaseViewController.h"
#import "Privatepolicy.h"
#import "Reportaproblem.h"
#import "Setting.h"
#import "Termsofservice.h"
@interface Setting ()

@end
@implementation Setting

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];

  [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:NO];
}
- (void)viewDidLoad {
  [super viewDidLoad];

  self.accounttableview.delegate = self;
  self.accounttableview.dataSource = self;
  [self.accounttableview reloadData];
  self.accounttableview.scrollEnabled = NO;
#pragma mark init supportTableiVew
  _supportData = [[NSMutableArray alloc]
      initWithObjects:@"Language", @"Private Policy", @"Terms of Service",
                      @"About This Version", nil];

  self.supporttableview.delegate = self;
  self.supporttableview.dataSource = self;
  [self.supporttableview reloadData];
  self.supporttableview.scrollEnabled = NO;
  [self.delegate setborder:self.Logoutbutton width:1.f];
  self.navigationItem.title = @"Setting";
}

#pragma mark UITableViewDelegate & UITableViewDataSource start--------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section

{
  return [_supportData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
  static NSString *simpleTableIdentifier = @"UITableViewCell";

  UITableViewCell *cell =
      [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

  if (cell == nil) {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                  reuseIdentifier:simpleTableIdentifier];
  }

  cell.textLabel.text = [_supportData objectAtIndex:indexPath.row];

  cell.textLabel.textColor = self.delegate.Greycolor;
  cell.selectionStyle = UITableViewCellSelectionStyleNone;
  return cell;
}
- (NSString *)tableView:(UITableView *)tableView
titleForHeaderInSection:(NSInteger)section

{
  return @"Setting";
}
- (void)tableView:(UITableView *)tableView
    willDisplayHeaderView:(UIView *)view
               forSection:(NSInteger)section {
  // Background color
  view.tintColor = [UIColor whiteColor];
  // Text Color
  UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
  [header.textLabel setTextColor:self.delegate.Greycolor];
}
- (void)tableView:(UITableView *)tableView
    didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  BaseViewController *UIViewController =
      [[Termsofservice alloc] initWithNibName:@"Termsofservice" bundle:nil];

  switch (indexPath.row) {
    case 0:

      break;
    case 1:
      UIViewController =
          [[Privatepolicy alloc] initWithNibName:@"Privatepolicy" bundle:nil];

      break;

    case 2:
      UIViewController =
          [[Termsofservice alloc] initWithNibName:@"Termsofservice" bundle:nil];

      break;
    case 3:
      UIViewController =
          [[Aboutthisversion alloc] initWithNibName:@"Aboutthisversion"
                                             bundle:nil];

      break;

    default:
      break;
  }

  [self.navigationController pushViewController:UIViewController animated:YES];
}
#pragma mark UITableViewDelegate & UITableViewDataSource end--------------------------------------------------------------------------

- (IBAction)logoutpress:(id)sender {
  [[[DBLoginRecord query] fetch] removeAll];
  [AddProductByEidModel shared].myProductDetail = nil;
  [LoginRecordModel shared].myLoginRecord = nil;

  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                                  message:@"Logout Success"
                                                 delegate:nil
                                        cancelButtonTitle:@"OK"
                                        otherButtonTitles:nil];
  [alert show];
}
@end