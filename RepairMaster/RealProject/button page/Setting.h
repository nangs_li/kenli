//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface Setting
    : BaseViewController<UITableViewDataSource, UITableViewDelegate>
@property(nonatomic, strong) IBOutlet UITableView *accounttableview;
@property(nonatomic, strong) IBOutlet UITableView *supporttableview;
@property(nonatomic, retain) NSMutableArray *accountData;
@property(nonatomic, retain) NSMutableArray *supportData;
@property(nonatomic, strong) IBOutlet UIButton *Logoutbutton;
@property(nonatomic, strong) IBOutlet UIButton *testpushbtn;

@end
