//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 hohojo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <Foundation/Foundation.h>
@interface VendorLoginPage : BaseViewController
@property(strong, nonatomic) IBOutlet UITextField *email;
@property(strong, nonatomic) IBOutlet UITextField *password;

@end
