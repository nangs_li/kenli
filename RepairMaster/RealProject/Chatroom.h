
//  Created by Luz Caballero on 9/17/13.
//  Copyright (c) 2013 Ken All rights reserved.
//

#import "JSQMessages.h"

#import "MWPhotoBrowser.h"

#import <AudioToolbox/AudioServices.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "ChatRoomCoverAgentView.h"
#import "ChatRoomSelectPhoto.h"
#import "ChatRoomSelectPhoto.h"
#import "DBQBChatDialog.h"
#import "DBQBChatMessage.h"
#import "RDVTabBarController.h"
#import "UnreadMessageCell.h"

#import "CellDateLabelView.h"

@class Chatroom;

@protocol JSQDemoViewControllerDelegate<NSObject>

- (void)didDismissJSQDemoViewController:(Chatroom *)vc;

@end

@interface Chatroom
    : JSQMessagesViewController<UIActionSheetDelegate, MBProgressHUDDelegate,
                                MWPhotoBrowserDelegate>
@property(strong, nonatomic) AppDelegate *delegate;
@property(nonatomic, strong) QBChatDialog *dialog;
@property(strong, nonatomic) MBProgressHUD *hud;
#pragma mark messageDateFormat
@property(nonatomic, strong) NSString *messageCellTopLabelDate;
@property(nonatomic, strong) NSDateFormatter *messageTopLabelDateFormatter;
@property(nonatomic, strong) NSDateFormatter *messageDateLabelFormatter;
@property(nonatomic, strong) NSDateFormatter *Onlydayformat;
#pragma mark Top Bar titleView
@property(nonatomic, strong) UIView *titleView;
@property(nonatomic, strong) UILabel *titleLabel;
@property(nonatomic, strong) UILabel *typingLabel;
#pragma mark Top Bar connectingView
@property(nonatomic, strong) UIView *connectingServerView;
@property(nonatomic, strong) UILabel *connectingLabel;
@property(nonatomic, strong) UIActivityIndicatorView *connectLoading;
#pragma mark checkFirstCreateDialogbyDialogNotTargetAgentQBID
@property(nonatomic, assign)
    BOOL checkHaveNotCountOfDialogsWithOccupants_IdsAllInDialogs;
@property(strong, nonatomic) NSString *chatroomNameString;

#pragma mark photoBrowers
@property(strong, nonatomic) SDWebImageManager *sdWebImageManager;
@property(strong, nonatomic) MWPhotoBrowser *mwPhotoBrowser;
@property(nonatomic, strong) NSMutableArray *mwPhotosBrowerPhotoArray;

#pragma mark firstTimeEnterChatRoom
@property(nonatomic, assign) BOOL firstTimeEnterChatRoom;
#pragma mark selectederrorchatmessage
@property(strong, nonatomic) DBQBChatMessage *selectedErrorChatMessage;
#pragma mark getRecipienterMethod
@property(strong, nonatomic) NSNumber *recipientID;
@property(strong, nonatomic) NSString *recipienterLastLoginTimeString;
@property(strong, nonatomic) NSDate *recipienterLastLoginTime;
- (void)getRecipienterFromQBServer;
@property(weak, nonatomic) id<JSQDemoViewControllerDelegate> delegateModal;
- (BOOL)IsEmpty:(id)thing;

- (void)getImageFromDBQBChatMessageToMWPhotoBrower;
- (void)chatRoomCollectionViewReloadOnly;
- (void)chatRoomCollectionViewscrollToBottomYesAnimated;
- (void)readAllReceivceMessage:(int)totalReadReceivceMessageCount;

#pragma mark UnReadMessage
/*
 //// set remove unreadlabel
 - (void)removeunreadlabel;
 */
////// unReadLabel
@property(strong, nonatomic) NSMutableArray *unReadMessageIDArray;
/*
 @property(strong, nonatomic) UIView *unReadlabelView;
 @property(strong, nonatomic) JSQMessagesCollectionViewCell *unreadlabelcell;
 */
@end
