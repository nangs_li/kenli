//
//  SecondViewController.h
//  sample-chat
//
//  Created by Igor Khomenko on 10/16/13.
//  Copyright (c) 2013 Igor Khomenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "MMDrawerController.h"
@interface DialogsViewController : BaseViewController

@property(strong, nonatomic) QBChatDialog *createdDialog;
#pragma mark swipe animation core
@property(strong, nonatomic) IBOutlet UILabel *connectinglabel;
@property(strong, nonatomic) IBOutlet UILabel *chatlabel;
@property(strong, nonatomic) IBOutlet UIActivityIndicatorView *loading;
#pragma mark dialogsTableViewOriginalFrame
@property(assign, nonatomic) CGRect dialogsTableViewOriginalFrame;
#pragma mark searchDialogResultArray
@property(nonatomic, strong) NSMutableArray *searchDialogResultArray;
@end
