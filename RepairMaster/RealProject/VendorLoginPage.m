//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 hohojo. All rights reserved.
//

#import "VendorLoginPage.h"
#import "VendorSignupPage.h"

#import "UploadProductphoto.h"
#import "GetQBIDbyVendorEidModel.h"
@interface VendorLoginPage ()

@end
@implementation VendorLoginPage

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:YES];
  self.navigationItem.title = @"VendorLogin";
}
- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
}
- (void)viewDidLoad {
  [super viewDidLoad];
}

- (IBAction)btnLoginPressed:(id)sender {
  if ([self IsEmpty:self.email.text] && [self IsEmpty:self.password.text]) {
    UIAlertView *alert =
        [[UIAlertView alloc] initWithTitle:@"Alert"
                                   message:@"Email and Password can not empty"
                                  delegate:nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
    [alert show];
  } else if ([self IsEmpty:self.email.text]) {
    UIAlertView *alert =
        [[UIAlertView alloc] initWithTitle:@"Alert"
                                   message:@"Email can not empty"
                                  delegate:nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
    [alert show];
  } else if ([self IsEmpty:self.password.text]) {
    UIAlertView *alert =
        [[UIAlertView alloc] initWithTitle:@"Alert"
                                   message:@"Password can not empty"
                                  delegate:nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
    [alert show];

  } else {
    [self loadingAndStopLoadingAfterSecond:8];
      [LoginRecordModel shared].myLoginRecord.username =self.email.text;
      [LoginRecordModel shared].myLoginRecord.password =self.password.text;
    [[LoginRecordModel shared] LoginApiByEmail:self.email.text
        Password:self.password.text
        success:^(LoginRecord *myLoginRecord) {
        
          [[ChatService shared] quickBloxToTalLoginOnly];
          UIAlertView *alert =
              [[UIAlertView alloc] initWithTitle:@"Success"
                                         message:@"Login Success"
                                        delegate:nil
                               cancelButtonTitle:@"OK"
                               otherButtonTitles:nil];
          [alert show];
          [self.navigationController popToRootViewControllerAnimated:YES];
        }
        failure:^(AFHTTPRequestOperation *operation, NSError *error,
                  NSString *errorMessage, int errorStatus, NSString *alert,
                  NSString *ok) {
          [MBProgressHUD hideHUDForView:self.view animated:YES];
          UIAlertView *alertView =
              [[UIAlertView alloc] initWithTitle:alert
                                         message:errorMessage
                                        delegate:nil
                               cancelButtonTitle:ok
                               otherButtonTitles:nil];
          [alertView show];

        }];
  }
}
- (IBAction)btnSigupPressed:(id)sender {
  VendorSignupPage *UIViewController =
      [[VendorSignupPage alloc] initWithNibName:@"VendorSignupPage" bundle:nil];
  [self.navigationController pushViewController:UIViewController animated:YES];
}
@end