//
//  TableViewCell.h
//  S18
//
//  Created by ios Developer 5 on 4/8/14.
//  Copyright (c) 2014 ios Developer 5. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DialogsViewCell : UITableViewCell

@property(weak, nonatomic) IBOutlet UILabel *title;
@property(weak, nonatomic) IBOutlet UILabel *subtitle;
@property(weak, nonatomic) IBOutlet UILabel *date;
@property(weak, nonatomic) IBOutlet UILabel *numberOfLastMessageLabel;
@property(strong, nonatomic) IBOutlet UIImageView *personalimageview;
#pragma mark Message SetUp Method
- (void)setupcellmessage:(NSString *)title subtitle:(NSString *)subtitle;
@end
