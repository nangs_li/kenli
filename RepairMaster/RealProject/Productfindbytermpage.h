//
//  MemberPerformanceViewController.h
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 hohojo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "ProductDetailClassArray.h"
@interface Productfindbytermpage
    : BaseViewController<UITableViewDataSource, UITableViewDelegate>
@property(strong, nonatomic) IBOutlet UITableView *tableview;

@property(nonatomic, strong) NSMutableArray *ProductDetailClassArray;
@property(nonatomic, assign) NSInteger buttonterm;

- (void)getProductFindByTermFromServer;
@end
