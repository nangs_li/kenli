//
//
//  abc
//
//  Created by Li Nang Shing on 23/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "AFHTTPRequestOperation.h"
@interface GetQBIDbyVendorEidModel : BaseModel
+ (GetQBIDbyVendorEidModel *)shared;

/**
 *  callNewsFeedApisuccess
 *
 *  @param success errorStatus=0
 *  @param failure
 errorStatus=1 Not NetWork Connection
 errorStatus=2 Server Not Reponse
 errorStatus=3 Duplicate record

 */
- (void)getQBIDbyVendorEid:(NSString *)eid
                   success:(void (^)(NSString *QBID))success
                   failure:(void (^)(AFHTTPRequestOperation *operation,
                                     NSError *error, NSString *errorMessage,
                                     int errorStatus, NSString *alert,
                                     NSString *ok))failure;

@end
