//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "GetQBIDbyVendorEidModel.h"

#import "HandleServerReturnString.h"

#import "AFNetworkReachabilityManager.h"
#import "AFNetworking.h"
@implementation GetQBIDbyVendorEidModel

static GetQBIDbyVendorEidModel *sharedGetQBIDbyVendorEidModel;

+ (GetQBIDbyVendorEidModel *)shared {
  @synchronized(self) {
    if (!sharedGetQBIDbyVendorEidModel) {
      sharedGetQBIDbyVendorEidModel = [[GetQBIDbyVendorEidModel alloc] init];
      [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    }
    return sharedGetQBIDbyVendorEidModel;
  }
}

- (void)getQBIDbyVendorEid:(NSString *)eid
                   success:(void (^)(NSString *QBID))success
                   failure:(void (^)(AFHTTPRequestOperation *operation,
                                     NSError *error, NSString *errorMessage,
                                     int errorStatus, NSString *alert,
                                     NSString *ok))failure {
  AFHTTPRequestOperationManager *manager =
      [AFHTTPRequestOperationManager manager];

  if (![self networkConnection]) {
    failure(nil, nil, @"Not NetWork Connection", 1, @"Alert", @"OK");
  }

  NSString *urllink = [NSString
      stringWithFormat:
          @"http://202.181.187.202:8080/advonews/vendor/%@/getChatID", eid];

  DDLogVerbose(@"apiurlstring-->%@", urllink);
  [manager POST:urllink
      parameters:nil
      success:^(AFHTTPRequestOperation *operation, id responseObject) {

        success([responseObject objectForKey:@"chatID"]);

      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if ([operation.response statusCode] == 417) {
          failure(nil, nil, @"Duplicate Record", 3, @"Alert", @"OK");
        } else {
          NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];
          success(operation.responseString);
          DDLogError(@"operation.responseString-->%@",
                     operation.responseString);
          DDLogError(@"operation.request.HTTPBody-->%@", newStr);
          DDLogError(@"response-->Error: %@", error);
          // failure(nil, nil, @"Server Not Reponse", 2, @"Alert", @"OK");
        }

      }];
}

#pragma check empty
- (BOOL)IsEmpty:(id)thing {
  return thing == nil || ([thing respondsToSelector:@selector(length)] &&
                          [(NSData *)thing length] == 0) ||
         ([thing respondsToSelector:@selector(count)] &&
          [(NSArray *)thing count] == 0);
}
@end