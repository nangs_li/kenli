//
//  d.h
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Facebook Inc. All rights reserved.
//

#ifndef abc_d_h
#define abc_d_h

#endif
#import "JSONModel.h"

#import "ListTermClass.h"
@interface ListTermClass : JSONModel
//+ (d *)shared;
+ (ListTermClass *)shared;

@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *language;
@property(strong, nonatomic) NSNumber *tid;
@property(assign, nonatomic) int vid;

- (ListTermClass *)initwithjsonstring:(NSString *)content:(int)error;
- (ListTermClass *)initwithNSDictionary:(NSDictionary *)content:(int)error;
@end
