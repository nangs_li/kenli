//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Facebook Inc. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "ListTermClass.h"

@implementation ListTermClass
static ListTermClass *shareListTermClass;

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
  return YES;
}

+ (ListTermClass *)shared {
  @synchronized(self) {
    if (!shareListTermClass) {
      shareListTermClass = [[ListTermClass alloc] init];
    }
    return shareListTermClass;
  }
}

- (ListTermClass *)initwithjsonstring:(NSString *)content:(int)error {
  shareListTermClass = nil;

  NSError *err = nil;
  shareListTermClass =
      [[ListTermClass alloc] initWithString:content error:&err];
  return shareListTermClass;
}
- (ListTermClass *)initwithNSDictionary:(NSDictionary *)content:(int)error {
  shareListTermClass = nil;

  NSError *err = nil;
  shareListTermClass =
      [[ListTermClass alloc] initWithDictionary:content error:&err];
  return shareListTermClass;
}

@end