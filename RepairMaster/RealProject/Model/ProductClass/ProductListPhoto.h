//
//  d.h
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Facebook Inc. All rights reserved.
//

#ifndef abc_d_h
#define abc_d_h

#endif
#import "JSONModel.h"

@protocol ProductListPhoto
@end
@interface ProductListPhoto : JSONModel
//+ (d *)shared;

@property(strong, nonatomic) NSNumber* eid;
@property(strong, nonatomic) NSNumber* aid;
@property(strong, nonatomic) NSString* apath;
@property(strong, nonatomic) NSString* url;
@property(strong, nonatomic) NSString* baseurl;
- (ProductListPhoto*)initwithjsonstring:(NSString*)content:(int)error;
- (ProductListPhoto*)initwithNSDictionary:(NSDictionary*)content:(int)error;
@end
