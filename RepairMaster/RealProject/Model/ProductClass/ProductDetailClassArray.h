//
//  d.h
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Facebook Inc. All rights reserved.
//

#ifndef abc_d_h
#define abc_d_h

#endif
#import "JSONModel.h"

#import "ProductDetailClass.h"
@interface ProductDetailClassArray : JSONModel
//+ (d *)shared;

@property(strong, nonatomic)
    NSMutableArray<ProductDetailClass> *ProductDetailClassArray;

- (ProductDetailClassArray *)initwithjsonstring:(NSString *)content:(int)error;
- (ProductDetailClassArray *)initwithNSDictionary:(NSDictionary *)
                                          content:(int)error;
@end
