//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Facebook Inc. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "ProductDetailClassArray.h"

@implementation ProductDetailClassArray
static ProductDetailClassArray *shareCLS_LOGinInfo;

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
  return YES;
}

+ (ProductDetailClassArray *)shared {
  @synchronized(self) {
    if (!shareCLS_LOGinInfo) {
      shareCLS_LOGinInfo = [[ProductDetailClassArray alloc] init];
    }
    return shareCLS_LOGinInfo;
  }
}

- (ProductDetailClassArray *)initwithjsonstring:(NSString *)content:(int)error {
  shareCLS_LOGinInfo = nil;

  NSError *err = nil;
  shareCLS_LOGinInfo =
      [[ProductDetailClassArray alloc] initWithString:content error:&err];
  return shareCLS_LOGinInfo;
}
- (ProductDetailClassArray *)initwithNSDictionary:(NSDictionary *)
                                          content:(int)error {
  shareCLS_LOGinInfo = nil;

  NSError *err = nil;
  shareCLS_LOGinInfo =
      [[ProductDetailClassArray alloc] initWithDictionary:content error:&err];
  return shareCLS_LOGinInfo;
}

@end