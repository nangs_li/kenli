//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Facebook Inc. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "ProductListPhoto.h"

@implementation ProductListPhoto
static ProductListPhoto *shareCLS_LOGinInfo;

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
  return YES;
}

+ (ProductListPhoto *)shared {
  @synchronized(self) {
    if (!shareCLS_LOGinInfo) {
      shareCLS_LOGinInfo = [[ProductListPhoto alloc] init];
    }
    return shareCLS_LOGinInfo;
  }
}

- (ProductListPhoto *)initwithjsonstring:(NSString *)content:(int)error {
  shareCLS_LOGinInfo = nil;

  NSError *err = nil;
  shareCLS_LOGinInfo =
      [[ProductListPhoto alloc] initWithString:content error:&err];
  return shareCLS_LOGinInfo;
}
- (ProductListPhoto *)initwithNSDictionary:(NSDictionary *)content:(int)error {
  shareCLS_LOGinInfo = nil;

  NSError *err = nil;
  shareCLS_LOGinInfo =
      [[ProductListPhoto alloc] initWithDictionary:content error:&err];
  return shareCLS_LOGinInfo;
}

@end