//
//  d.h
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Facebook Inc. All rights reserved.
//

#ifndef abc_d_h
#define abc_d_h

#endif
#import "JSONModel.h"

#import "ProductListPhoto.h"
@protocol ProductDetailClass
@end

@interface ProductDetailClass : JSONModel
//+ (d *)shared;
@property(strong, nonatomic) NSNumber* eid;
@property(strong, nonatomic) NSNumber* tstype;
@property(strong, nonatomic) NSString* price;
@property(strong, nonatomic) NSString* telNo;
@property(strong, nonatomic) NSString* size;
@property(strong, nonatomic) NSString* minserviceprice;
@property(strong, nonatomic) NSString* qualification;
@property(strong, nonatomic) NSString* vendorId;
@property(strong, nonatomic) NSNumber* selectedterm;
@property(strong, nonatomic) NSString* desc;
@property(strong, nonatomic) NSArray<ProductListPhoto>* photos;
@property(strong, nonatomic) NSDictionary* likeCount;
@property(strong, nonatomic) NSArray* terms;
@property(strong, nonatomic) NSString* QBID;
- (ProductDetailClass*)initwithjsonstring:(NSString*)content:(int)error;
- (ProductDetailClass*)initwithNSDictionary:(NSDictionary*)content:(int)error;
@end
