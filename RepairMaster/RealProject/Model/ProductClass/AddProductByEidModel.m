//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "AddProductByEidModel.h"

#import "AFNetworkReachabilityManager.h"
#import "AFNetworking.h"
#import "HandleServerReturnString.h"
@implementation AddProductByEidModel

static AddProductByEidModel *sharedAddProductByEidModel;

+ (AddProductByEidModel *)shared {
  @synchronized(self) {
    if (!sharedAddProductByEidModel) {
      sharedAddProductByEidModel = [[AddProductByEidModel alloc] init];
      [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    }
    return sharedAddProductByEidModel;
  }
}

- (void)AddProductByEid:(NSString *)eid
     productDescription:(NSString *)productDescription
                  phone:(NSString *)phone
                  price:(NSString *)price
                advname:(NSString *)advname
                success:(void (^)(ProductDetailClass *myProductDetail))success
                failure:(void (^)(AFHTTPRequestOperation *operation,
                                  NSError *error, NSString *errorMessage,
                                  int errorStatus, NSString *alert,
                                  NSString *ok))failure {
  AFHTTPRequestOperationManager *manager =
      [AFHTTPRequestOperationManager manager];

  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.responseSerializer = [AFJSONResponseSerializer serializer];

  if (![self networkConnection]) {
    failure(nil, nil, @"Not NetWork Connection", 1, @"Alert", @"OK");
  }

  NSString *urllink = [NSString
      stringWithFormat:
          @"http://202.181.187.202:8080/advonews/vendor/%@/addProduct2",
          [LoginRecordModel shared].myLoginRecord.eid];

  DDLogVerbose(@"apiurlstring-->%@", urllink);
  [manager POST:urllink
      parameters:@{
        @"description" : productDescription,
        @"telNo" : phone,
        @"size" : @"500",
        @"price" : price,
        @"minserviceprice" : @"100",
        @"qualification" : advname
      }
      success:^(AFHTTPRequestOperation *operation, id responseObject) {

        int error = 0;
        self.myProductDetail =
            [[ProductDetailClass alloc] initwithNSDictionary:
                                              responseObject:error];

#pragma mark save DBLogInfo into database
        [[[DBProductDetail query] fetch] removeAll];
        DBProductDetail *dbproductdetail = [DBProductDetail new];
        dbproductdetail.eid = [LoginRecordModel shared].myLoginRecord.eid;
        dbproductdetail.Date = [NSDate date];
        dbproductdetail.ProductDetail = [NSKeyedArchiver
            archivedDataWithRootObject:[self.myProductDetail copy]];

        [dbproductdetail commit];

        success(self.myProductDetail);

      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if ([operation.response statusCode] == 417) {
          failure(nil, nil, @"Duplicate Record", 3, @"Alert", @"OK");
        } else {
          NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];

          DDLogError(@"operation.request.HTTPBody-->%@", newStr);
          DDLogError(@"response-->Error: %@", error);
          DDLogError(@"operation.responseString-->Error: %@",
                     operation.responseString);
          failure(nil, nil, @"Server Not Reponse", 2, @"Alert", @"OK");
        }

      }];
}

#pragma check empty
- (BOOL)IsEmpty:(id)thing {
  return thing == nil || ([thing respondsToSelector:@selector(length)] &&
                          [(NSData *)thing length] == 0) ||
         ([thing respondsToSelector:@selector(count)] &&
          [(NSArray *)thing count] == 0);
}
@end