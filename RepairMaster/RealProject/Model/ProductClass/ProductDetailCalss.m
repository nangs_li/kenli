//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Facebook Inc. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "ProductDetailClass.h"

@implementation ProductDetailClass
static ProductDetailClass *shareCLS_LOGinInfo;

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
  return YES;
}

+ (ProductDetailClass *)shared {
  @synchronized(self) {
    if (!shareCLS_LOGinInfo) {
      shareCLS_LOGinInfo = [[ProductDetailClass alloc] init];
    }
    return shareCLS_LOGinInfo;
  }
}

- (ProductDetailClass *)initwithjsonstring:(NSString *)content:(int)error {
  shareCLS_LOGinInfo = nil;

  NSError *err = nil;
  shareCLS_LOGinInfo =
      [[ProductDetailClass alloc] initWithString:content error:&err];
  return shareCLS_LOGinInfo;
}
- (ProductDetailClass *)initwithNSDictionary:(NSDictionary *)
                                     content:(int)error {
  shareCLS_LOGinInfo = nil;

  NSError *err = nil;
  shareCLS_LOGinInfo =
      [[ProductDetailClass alloc] initWithDictionary:content error:&err];
  return shareCLS_LOGinInfo;
}

@end