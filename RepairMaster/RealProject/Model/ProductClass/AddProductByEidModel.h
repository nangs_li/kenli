//
//
//  abc
//
//  Created by Li Nang Shing on 23/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "AFHTTPRequestOperation.h"
#import "BaseModel.h"
@interface AddProductByEidModel : BaseModel
@property(strong, nonatomic) ProductDetailClass *myProductDetail;
@property(nonatomic, strong) NSString *selectedpickerData;
@property(nonatomic, assign) NSInteger selectedpickerrow;
@property(nonatomic, retain) NSMutableArray *chosenImages;
+ (AddProductByEidModel *)shared;
/**
 *  callNewsFeedApisuccess
 *
 *  @param success errorStatus=0
 *  @param failure
 errorStatus=1 Not NetWork Connection
 errorStatus=2 Server Not Reponse
 errorStatus=3 Duplicate record

 */
- (void)AddProductByEid:(NSString *)eid
     productDescription:(NSString *)productDescription
                  phone:(NSString *)phone
                  price:(NSString *)price
                advname:(NSString *)advname
                success:(void (^)(ProductDetailClass *myProductDetail))success
                failure:(void (^)(AFHTTPRequestOperation *operation,
                                  NSError *error, NSString *errorMessage,
                                  int errorStatus, NSString *alert,
                                  NSString *ok))failure;

@end
