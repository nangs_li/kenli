//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "BaseModel.h"
#import "NSData+Base64.h"
#import "Reachability.h"
#import "StringEncryption.h"
@interface BaseModel ()
@end

@implementation BaseModel

#pragma mark check empty
- (BOOL)IsEmpty:(id)thing {
  return thing == nil || ([thing respondsToSelector:@selector(length)] &&
                          [(NSData *)thing length] == 0) ||
         ([thing respondsToSelector:@selector(count)] &&
          [(NSArray *)thing count] == 0);
}
#pragma mark AES encrypt
- (NSString *)stringToAESEncrypt:(NSString *)string {
  NSData *encryptedData = [[StringEncryption alloc]
      encrypt:[string dataUsingEncoding:NSUTF8StringEncoding]
          key:@"03ac674216f3e15c761ee1a5e255f067"
           iv:@"mS++sv+0xEL0YwM="];

  return [encryptedData base64EncodingWithLineLength:0];
}
//// AES decrypt
- (NSString *)stringToAESDecrypt:(NSString *)string {
  NSData *encryptedData = [[StringEncryption alloc]
      decrypt:[[NSData alloc] initWithBase64EncodedString:string options:0]
          key:@"03ac674216f3e15c761ee1a5e255f067"
           iv:@"mS++sv+0xEL0YwM="];

  return [[NSString alloc] initWithData:encryptedData
                               encoding:NSUTF8StringEncoding];
}
- (BOOL)networkConnection {
  Reachability *networkReachability =
      [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
  if (networkStatus == NotReachable) {
    /*
     UIAlertView *alert =
     [[UIAlertView alloc] initWithTitle:@"Alert"
     message:@"Not Network,Please Turn On Wifi"
     delegate:nil
     cancelButtonTitle:@"OK"
     otherButtonTitles:nil];

     [alert show];
     */
    DDLogInfo(@"There IS NO internet connection");
    return NO;
  } else {
    DDLogInfo(@"There IS internet connection");
    return YES;
  }
}

- (BOOL)checkAccessError:(NSString *)serverReturnString {
  if ([serverReturnString rangeOfString:@"\"ErrorCode\":4,"].location !=
      NSNotFound) {
    return YES;
  }
  return NO;
}

@end