//
//
//  abc
//
//  Created by Li Nang Shing on 23/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "AFHTTPRequestOperation.h"

#define kFirechatNS @"https://intense-heat-7640.firebaseio.com/Chat"
#define kFireuserNS @"https://intense-heat-7640.firebaseio.com/User"

typedef NS_ENUM(NSUInteger, RequestErrorStatus) {
  NotUseStatus = 0,
  NotNetWorkConnection,
  RealServerNotReponse,
  AccessErrorOtherUserLoginThisAccount,
  NotRecordFind,
  GoogleServerNotReponse,
  GoogleReturnEmptyResult,
};

@interface BaseModel : NSObject
#define ServerAddress @"http://api.real.co"
#pragma mark check empty
- (BOOL)IsEmpty:(id)thing;
#pragma mark AES encrypt
- (NSString *)stringToAESEncrypt:(NSString *)string;
- (NSString *)stringToAESDecrypt:(NSString *)string;
#pragma mark CallServer chec,Error-------------------------------------------------------------------------------------
- (BOOL)checkAccessError:(NSString *)serverReturnString;
- (BOOL)networkConnection;
@end
