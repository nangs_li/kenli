//
//
//  abc
//
//  Created by Li Nang Shing on 23/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "AFHTTPRequestOperation.h"
@interface ListTermModel : BaseModel
@property(nonatomic, strong) NSMutableArray *serverReturnTermArray;

+ (ListTermModel *)shared;
/**
 *  callNewsFeedApisuccess
 *
 *  @param success errorStatus=0
 *  @param failure
 errorStatus=1 Not NetWork Connection
 errorStatus=2 Server Not Reponse
 errorStatus=3 Duplicate record

 */
- (void)listTerms:(NSString *)termID
          forPage:(int)page
          success:(void (^)(NSMutableArray *serverReturnTermArray))success
          failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error,
                            NSString *errorMessage, int errorStatus,
                            NSString *alert, NSString *ok))failure;

@end
