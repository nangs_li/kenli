//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "FireBaseChatLoginModel.h"

#import "HandleServerReturnString.h"

#import "AFNetworkReachabilityManager.h"
#import "AFNetworking.h"
#import "SaveQBIDbyEidModel.h"
@implementation FireBaseChatLoginModel

static FireBaseChatLoginModel *sharedFireBaseChatLoginModel;

+ (FireBaseChatLoginModel *)shared {
  @synchronized(self) {
    if (!sharedFireBaseChatLoginModel) {
      sharedFireBaseChatLoginModel = [[FireBaseChatLoginModel alloc] init];
      [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        
    }
      
    return sharedFireBaseChatLoginModel;
  }
}
-(void)sigupUserUserName:(NSString *)username{
    
    self.firebase = [[Firebase alloc] initWithUrl:kFireuserNS];
    [[self.firebase childByAutoId] setValue:@{@"userName" : username}];
   
    
}

- (void)loginByQuickBloxOrSignUpQuickBlox {
    
    
    //// QBSignup
    if ([self IsEmpty:[LoginRecordModel shared].myLoginRecord.QBID]) {
        
        
        
        [self  sigupUserUserName:[LoginRecordModel shared].myLoginRecord.username];
        [self.firebase observeEventType:FEventTypeChildAdded withBlock:^(FDataSnapshot *snapshot) {
            
            [self saveChatIDbyQBID:snapshot.key];
            
            
        }];
        
        
        
     
        
    }
}


- (void)saveChatIDbyQBID:(NSString *)QBID {
    
    [[SaveQBIDbyEidModel shared] saveQBID:QBID
     
                                  success:^(id responseObject) {
                                      [LoginRecordModel shared].myLoginRecord.QBID =
                                      QBID;
                                      [[[DBLoginRecord query] fetch] removeAll];
                                      DBLoginRecord *dbLoginRecord = [DBLoginRecord new];
                                      dbLoginRecord.Date = [NSDate date];
                                      
                                      dbLoginRecord.LoginRecrod = [NSKeyedArchiver
                                                                   archivedDataWithRootObject:[[LoginRecordModel shared]
                                                                                               .myLoginRecord copy]];
                                      dbLoginRecord.eid = [LoginRecordModel shared].myLoginRecord.eid;
                                      [dbLoginRecord commit];
                                  }
     
                                  failure:^(AFHTTPRequestOperation *operation, NSError *error,
                                            NSString *errorMessage, int errorStatus, NSString *alert,
                                            NSString *ok) {
                                     
                                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alert
                                                                                          message:errorMessage
                                                                                         delegate:nil
                                                                                cancelButtonTitle:ok
                                                                                otherButtonTitles:nil];
                                      [alertView show];
                                      
                                  }];
}

#pragma check empty
- (BOOL)IsEmpty:(id)thing {
  return thing == nil || ([thing respondsToSelector:@selector(length)] &&
                          [(NSData *)thing length] == 0) ||
         ([thing respondsToSelector:@selector(count)] &&
          [(NSArray *)thing count] == 0);
}
@end