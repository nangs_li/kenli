//
//  d.h
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Facebook Inc. All rights reserved.
//

#ifndef abc_d_h
#define abc_d_h

#endif
#import "JSONModel.h"

@interface LoginRecord : JSONModel
//+ (d *)shared;

@property(strong, nonatomic) NSNumber* eid;

@property(strong, nonatomic) NSString* username;
@property(strong, nonatomic) NSString* name;
@property(strong, nonatomic) NSString* PhotoURL;
@property(strong, nonatomic) NSString* QBID;
@property(strong, nonatomic) NSString* password;
- (LoginRecord*)initwithjsonstring:(NSString*)content:(int)error;
- (LoginRecord*)initwithNSDictionary:(NSDictionary*)content:(int)error;
@end
