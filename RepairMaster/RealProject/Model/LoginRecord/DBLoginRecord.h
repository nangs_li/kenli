//
//  d.h
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Facebook Inc. All rights reserved.
//

#ifndef abc_d_h
#define abc_d_h

#endif

#import <DBAccess/DBAccess.h>

@interface DBLoginRecord : DBObject
//+ (d *)shared;
@property(assign, nonatomic) NSNumber* eid;
@property(strong, nonatomic) NSData* LoginRecrod;
@property(strong, nonatomic) NSDate* Date;

@end
