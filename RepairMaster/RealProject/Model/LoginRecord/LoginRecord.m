//
//  d.m
//  abc
//
//  Created by Li Nang Shing on 27/4/2015.
//  Copyright (c) 2015年 Facebook Inc. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "LoginRecord.h"

@implementation LoginRecord
static LoginRecord *shareCLS_LOGinInfo;

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
  return YES;
}

+ (LoginRecord *)shared {
  @synchronized(self) {
    if (!shareCLS_LOGinInfo) {
      shareCLS_LOGinInfo = [[LoginRecord alloc] init];
    }
    return shareCLS_LOGinInfo;
  }
}

- (LoginRecord *)initwithjsonstring:(NSString *)content:(int)error {
  shareCLS_LOGinInfo = nil;

  NSError *err = nil;
  shareCLS_LOGinInfo = [[LoginRecord alloc] initWithString:content error:&err];
  return shareCLS_LOGinInfo;
}
- (LoginRecord *)initwithNSDictionary:(NSDictionary *)content:(int)error {
  shareCLS_LOGinInfo = nil;

  NSError *err = nil;
  shareCLS_LOGinInfo =
      [[LoginRecord alloc] initWithDictionary:content error:&err];
  return shareCLS_LOGinInfo;
}

@end