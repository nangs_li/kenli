//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "SaveQBIDbyEidModel.h"

#import "HandleServerReturnString.h"

#import "AFNetworkReachabilityManager.h"
#import "AFNetworking.h"
@implementation SaveQBIDbyEidModel

static SaveQBIDbyEidModel *sharedSaveQBIDbyEidModel;

+ (SaveQBIDbyEidModel *)shared {
  @synchronized(self) {
    if (!sharedSaveQBIDbyEidModel) {
      sharedSaveQBIDbyEidModel = [[SaveQBIDbyEidModel alloc] init];
      [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    }
    return sharedSaveQBIDbyEidModel;
  }
}

- (void)saveQBID:(NSString *)QBID
         success:(void (^)(id responseObject))success
         failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error,
                           NSString *errorMessage, int errorStatus,
                           NSString *alert, NSString *ok))failure {
  AFHTTPRequestOperationManager *manager =
      [AFHTTPRequestOperationManager manager];

  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  // manager.responseSerializer = [AFJSONResponseSerializer serializer];

  if (![self networkConnection]) {
    failure(nil, nil, @"Not NetWork Connection", 1, @"Alert", @"OK");
  }

  NSString *urllink = [NSString
      stringWithFormat:
          @"http://202.181.187.202:8080/advonews/vendor/%@/saveChatID",
          [[LoginRecordModel shared].myLoginRecord.eid stringValue]];

  DDLogVerbose(@"apiurlstring-->%@", urllink);
  [manager POST:urllink
      parameters:@{
        @"chatID" : QBID
      }
      success:^(AFHTTPRequestOperation *operation, id responseObject) {

        success(responseObject);

      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if ([operation.response statusCode] == 417) {
          failure(nil, nil, @"Duplicate Record", 3, @"Alert", @"OK");
        } else {
          NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];

          DDLogError(@"operation.request.HTTPBody-->%@", newStr);
          DDLogError(@"response-->Error: %@", error);
          failure(nil, nil, @"Server Not Reponse", 2, @"Alert", @"OK");
        }

      }];
}

#pragma check empty
- (BOOL)IsEmpty:(id)thing {
  return thing == nil || ([thing respondsToSelector:@selector(length)] &&
                          [(NSData *)thing length] == 0) ||
         ([thing respondsToSelector:@selector(count)] &&
          [(NSArray *)thing count] == 0);
}
@end