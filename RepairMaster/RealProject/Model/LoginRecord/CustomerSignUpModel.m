//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "CustomerSignUpModel.h"

#import "HandleServerReturnString.h"

#import "AFNetworkReachabilityManager.h"
#import "AFNetworking.h"
@implementation CustomerSignUpModel

static CustomerSignUpModel *sharedCustomerSignUpModel;

+ (CustomerSignUpModel *)shared {
  @synchronized(self) {
    if (!sharedCustomerSignUpModel) {
      sharedCustomerSignUpModel = [[CustomerSignUpModel alloc] init];
      [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    }
    return sharedCustomerSignUpModel;
  }
}

- (void)SignUpApiByUserName:(NSString *)username
                      Email:(NSString *)email
                   Password:(NSString *)password
                    success:(void (^)(LoginRecord *myLoginRecord))success
                    failure:(void (^)(AFHTTPRequestOperation *operation,
                                      NSError *error, NSString *errorMessage,
                                      int errorStatus, NSString *alert,
                                      NSString *ok))failure {
  NSDictionary *name = @{ @"first" : username };
  NSDictionary *emailDict = @{
    @"weight" : @"1",
    @"email" : email,
    @"name" : email
  };

  NSMutableArray *emailarray = [[NSMutableArray alloc] init];
  [emailarray addObject:emailDict];
  NSDictionary *InputJson2 = @{
    @"name" : name,
    @"password" : password,
    @"emails" : emailarray
  };

  AFHTTPRequestOperationManager *manager =
      [AFHTTPRequestOperationManager manager];

  manager.requestSerializer = [AFJSONRequestSerializer serializer];

  manager.responseSerializer = [AFJSONResponseSerializer serializer];

  if (![self networkConnection]) {
    failure(nil, nil, @"Not NetWork Connection", 1, @"Alert", @"OK");
  }

  DDLogVerbose(@"apiurlstring-->%@",
               @"http://202.181.187.202:8080/advonews/customer/register");

  [manager POST:@"http://202.181.187.202:8080/advonews/customer/register"
      parameters:InputJson2
      success:^(AFHTTPRequestOperation *operation, id responseObject) {

        int error = 0;
        [LoginRecordModel shared].myLoginRecord =
            [[LoginRecord alloc] initwithNSDictionary:responseObject:error];
#pragma mark save DBLogInfo into database
        [[[DBLoginRecord query] fetch] removeAll];
        DBLoginRecord *dblogInfo = [DBLoginRecord new];
        dblogInfo.eid = [LoginRecordModel shared].myLoginRecord.eid;
        [LoginRecordModel shared].myLoginRecord.username = email;
        [LoginRecordModel shared].myLoginRecord.password = password;
        dblogInfo.LoginRecrod = [NSKeyedArchiver
            archivedDataWithRootObject:[[LoginRecordModel shared]
                                               .myLoginRecord copy]];

        [dblogInfo commit];
        success([LoginRecordModel shared].myLoginRecord);

      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if ([operation.response statusCode] == 417) {
          failure(nil, nil, @"Duplicate Record", 3, @"Alert", @"OK");
        } else {
          NSString *newStr =
              [[NSString alloc] initWithData:operation.request.HTTPBody
                                    encoding:NSUTF8StringEncoding];

          DDLogError(@"operation.request.HTTPBody-->%@", newStr);
          DDLogError(@"response-->Error: %@", error);
          failure(nil, nil, @"Server Not Reponse", 2, @"Alert", @"OK");
        }
      }];
}

#pragma check empty
- (BOOL)IsEmpty:(id)thing {
  return thing == nil || ([thing respondsToSelector:@selector(length)] &&
                          [(NSData *)thing length] == 0) ||
         ([thing respondsToSelector:@selector(count)] &&
          [(NSArray *)thing count] == 0);
}
@end