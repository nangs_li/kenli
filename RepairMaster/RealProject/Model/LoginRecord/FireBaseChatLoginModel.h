//
//  
//  abc
//
//  Created by Li Nang Shing on 23/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//


#import "AFHTTPRequestOperation.h"
#import "BaseModel.h"
#import <Firebase/Firebase.h>
@interface FireBaseChatLoginModel : BaseModel


@property (nonatomic, strong) Firebase* firebase;
+ (FireBaseChatLoginModel *)shared ;

-(void)sigupUserUserName:(NSString *)username;

- (void)loginByQuickBloxOrSignUpQuickBlox;

@end
