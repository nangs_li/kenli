//
//  AgentLanguage.h
//  abc
//
//  Created by Li Nang Shing on 23/4/2015.
//  Copyright (c) 2015年 Facebook Inc. All rights reserved.
//

//#ifndef abc_AgentLanguage_h
//#define abc_AgentLanguage_h

//#endif
#import "JSONModel.h"
@protocol QBUUserCustomData
@end

@interface QBUUserCustomData : JSONModel
@property(strong, nonatomic) NSString* mypersonalphotourl;
@property(assign, nonatomic) BOOL chatroomshowlastseen;
@property(assign, nonatomic) BOOL chatroomshowreadreceipts;
@end

//@implementation AgentLanguage

//@end