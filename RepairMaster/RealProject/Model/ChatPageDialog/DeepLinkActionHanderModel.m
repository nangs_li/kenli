//
//  Photos.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DeepLinkActionHanderModel.h"
@implementation DeepLinkActionHanderModel

static DeepLinkActionHanderModel *sharedDeepLinkActionHanderModel;

+ (DeepLinkActionHanderModel *)shared {
  @synchronized(self) {
    if (!sharedDeepLinkActionHanderModel) {
      sharedDeepLinkActionHanderModel =
          [[DeepLinkActionHanderModel alloc] init];
      [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    }
    return sharedDeepLinkActionHanderModel;
  }
}

@end