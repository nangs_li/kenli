//
//  Photos.h
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

//#ifndef abc_Photos_h
//#define abc_Photos_h

//#endif
#import "BaseModel.h"
typedef NS_ENUM(NSUInteger, DeepLinkActionType) { GoToChatRoom = 0 };

@interface DeepLinkActionHanderModel : BaseModel
+ (DeepLinkActionHanderModel *)shared;
@property(strong, nonatomic) NSString *QBID;
@property(strong, nonatomic) NSString *username;

@end

//@implementation Photos

//@end