//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 hohojo. All rights reserved.
//

#import "CustomerSignupPage.h"

#import "CustomerLoginPage.h"
#import "UploadProductphoto.h"
#import "CustomerSignUpModel.h"
#import "SaveQBIDbyEidModel.h"
#import "FireBaseChatLoginModel.h"
@interface CustomerSignupPage ()

@end
@implementation CustomerSignupPage

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:YES];
}
- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  self.navigationItem.title = @"CustomerSignUp";
}
- (void)viewDidLoad {
  //[self performSelector:@selector(layout) withObject:nil afterDelay:2.0];
  [super viewDidLoad];
}

- (IBAction)btnLoginPressed:(id)sender {
  CustomerLoginPage *UIViewController =
      [[CustomerLoginPage alloc] initWithNibName:@"CustomerLoginPage"
                                          bundle:nil];
  [self.navigationController pushViewController:UIViewController animated:YES];
}
- (IBAction)btnSignupPressed:(id)sender {
  BOOL error = NO;
  NSString *usernameerrormessage = @"";
  NSString *passworderrormessage = @"";
  NSString *emailerrormessage = @"";

  if ([self IsEmpty:self.username.text]) {
    usernameerrormessage = @"Username can not empty";
    error = YES;
  }
  if ([self IsEmpty:self.password.text]) {
    passworderrormessage = @"Password can not empty";
    error = YES;
  }
  if ([self IsEmpty:self.email.text]) {
    emailerrormessage = @"Email can not empty";

    error = YES;
  }
  if (!error) {
    [self loadingAndStopLoadingAfterSecond:8];
       [LoginRecordModel shared].myLoginRecord.username =self.email.text;
      [LoginRecordModel shared].myLoginRecord.password =self.password.text;
    [[CustomerSignUpModel shared] SignUpApiByUserName:self.username.text
        Email:self.email.text
        Password:self.password.text
        success:^(LoginRecord *myLoginRecord) {
          [self customerLoginApi];
        }

        failure:^(AFHTTPRequestOperation *operation, NSError *error,
                  NSString *errorMessage, int errorStatus, NSString *alert,
                  NSString *ok) {
          [MBProgressHUD hideHUDForView:self.view animated:YES];
          UIAlertView *alertView =
              [[UIAlertView alloc] initWithTitle:alert
                                         message:errorMessage
                                        delegate:nil
                               cancelButtonTitle:ok
                               otherButtonTitles:nil];
          [alertView show];

        }];
  }

  if (error) {
    NSString *errormessage =
        [NSString stringWithFormat:@"%@\n%@\n%@", usernameerrormessage,
                                   passworderrormessage, emailerrormessage];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                    message:errormessage
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
  }
}
- (void)customerLoginApi {
    [LoginRecordModel shared].myLoginRecord.username =self.email.text;
    [LoginRecordModel shared].myLoginRecord.password =self.password.text;
  [[LoginRecordModel shared] LoginApiByEmail:self.email.text
      Password:self.password.text
      success:^(LoginRecord *myLoginRecord) {

          [[FireBaseChatLoginModel shared] loginByQuickBloxOrSignUpQuickBlox];
          [self loginSuccessMoveToTopPage];


      }

      failure:^(AFHTTPRequestOperation *operation, NSError *error,
                NSString *errorMessage, int errorStatus, NSString *alert,
                NSString *ok) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alert
                                                            message:errorMessage
                                                           delegate:nil
                                                  cancelButtonTitle:ok
                                                  otherButtonTitles:nil];
        [alertView show];

      }];
}

- (void)loginSuccessMoveToTopPage {
  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                                  message:@"SignUp Success"
                                                 delegate:nil
                                        cancelButtonTitle:@"OK"
                                        otherButtonTitles:nil];

  [alert show];
  [self.navigationController popToRootViewControllerAnimated:YES];
}
@end