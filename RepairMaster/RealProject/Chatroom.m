#import "ChatRoomSendMessageModel.h"
#import "Chatroom.h"
#import "GetChatMessageToJsqMessageArrayModel.h"
@interface Chatroom ()<ChatServiceDelegate>

@end

@implementation Chatroom

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
  }
  return self;
}
- (void)viewDidLoad {
  [super viewDidLoad];

  // Do any additional setup after loading the view.
  self.senderId = [LoginRecordModel shared].myLoginRecord.QBID;
  self.senderDisplayName = @"Peter";

  [ChatService shared];
  self.messageTopLabelDateFormatter = [[NSDateFormatter alloc] init];
  [self.messageTopLabelDateFormatter setDateFormat:@"EEE, d MMM"];
  self.messageDateLabelFormatter = [[NSDateFormatter alloc] init];
  [self.messageDateLabelFormatter setDateFormat:@"h:mm a"];
  self.Onlydayformat = [[NSDateFormatter alloc] init];
  [self.Onlydayformat setDateFormat:@"yyyy-MM-dd"];

  self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
  self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;

  _delegate = [AppDelegate getAppDelegate];
  //// titleView
  self.titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 60)];
  self.titleLabel = [[UILabel alloc]
      initWithFrame:CGRectMake(0, 10, self.titleView.frame.size.width, 20)];
  self.titleLabel.textAlignment = NSTextAlignmentCenter;

  self.typingLabel = [[UILabel alloc]
      initWithFrame:CGRectMake(0, 30, self.titleView.frame.size.width, 20)];

  self.typingLabel.textAlignment = NSTextAlignmentCenter;
  [self.typingLabel setFont:[UIFont systemFontOfSize:10]];
  [self.typingLabel setTextColor:[UIColor grayColor]];
  [self.titleView addSubview:self.titleLabel];
  [self.titleView addSubview:self.typingLabel];
  //// connectingserverview
  self.connectingServerView =
      [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 60)];

  self.connectingLabel = [[UILabel alloc] init];
  [self.connectingLabel setFrame:CGRectMake(50, 10, 150, 20)];
  self.connectLoading = [[UIActivityIndicatorView alloc]
      initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
  [self.connectLoading startAnimating];
  [self.connectLoading setFrame:CGRectMake(25, 10, 20, 20)];

  [self.connectingServerView addSubview:self.connectingLabel];
  [self.connectingServerView addSubview:self.connectLoading];
  if ([ChatService shared].connecttoserversuccess) {
    [self chatRoomConnectToServerSuccess];
  } else {
    [self chatDidAccidentallyDisconnectNotification];
  }

  [JSQMessagesCollectionViewCell registerMenuAction:@selector(customAction:)];
  [UIMenuController sharedMenuController].menuItems = @[
    [[UIMenuItem alloc] initWithTitle:@"Custom Action"
                               action:@selector(customAction:)]
  ];
  self.firstTimeEnterChatRoom = YES;
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [self setUpFixedLabelTextAccordingToSelectedLanguage];
  self.sdWebImageManager = [SDWebImageManager sharedManager];
  [self.sdWebImageManager.imageDownloader setMaxConcurrentDownloads:2];
  [[SDImageCache sharedImageCache] setShouldDecompressImages:NO];
  [[SDWebImageDownloader sharedDownloader] setShouldDecompressImages:NO];

  [[GetChatMessageToJsqMessageArrayModel shared]
      getChatMessageFromDBToJsqMessageArrayForDialog:self.dialog
                                            chatRoom:self];

  [self.navigationController setNavigationBarHidden:NO];
  [[NSNotificationCenter defaultCenter]
      addObserver:self
         selector:@selector(chatRoomConnectToServerSuccess)
             name:kNotificationChatRoomConnectToServerSuccess
           object:nil];
  [[NSNotificationCenter defaultCenter]
      addObserver:self
         selector:@selector(chatDidAccidentallyDisconnectNotification)
             name:kNotificationChatDidAccidentallyDisconnect
           object:nil];
  self.senderId = [LoginRecordModel shared].myLoginRecord.QBID;

  UIBarButtonItem *tmpButtonItem = [[UIBarButtonItem alloc]
      initWithTitle:JMOLocalizedString(@"ChatPageChatRoom.Back", nil)
              style:UIBarButtonItemStylePlain
             target:self
             action:@selector(backPressed:)];

  self.navigationItem.leftBarButtonItem = tmpButtonItem;

  self.showLoadEarlierMessagesHeader = YES;
  [[TWMessageBarManager sharedInstance] hideAllAnimated:YES];

  [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:YES];

  // Set title

  [ChatService shared].delegate = self;
  self.titleLabel.text = self.chatroomNameString;

  if ([ChatService shared].connecttoserversuccess) {
    [self.inputToolbar toggleSendButtonEnabled];
    self.inputToolbar.contentView.leftBarButtonItem.enabled = YES;

  } else {
    self.inputToolbar.contentView.rightBarButtonItem.enabled = NO;
    self.inputToolbar.contentView.leftBarButtonItem.enabled = NO;
  }
}
- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];

  self.navigationController.navigationBar.hidden = NO;
  if (![self IsEmpty:[ChatRoomSendMessageModel shared].chatroomchosenImages]) {
    if (self.checkHaveNotCountOfDialogsWithOccupants_IdsAllInDialogs) {
      [QBRequest createDialog:self.dialog
          successBlock:^(QBResponse *response, QBChatDialog *createdDialog) {

            self.dialog = createdDialog;
            self.checkHaveNotCountOfDialogsWithOccupants_IdsAllInDialogs = NO;

            [[ChatRoomSendMessageModel shared]
                sendChatRoomChosenImagesForDialog:self.dialog];

          }
          errorBlock:^(QBResponse *response) {
            DDLogError(@"QBResponse %@", response);
          }];
    } else {
      [[ChatRoomSendMessageModel shared]
          sendChatRoomChosenImagesForDialog:self.dialog];
    }

    self.unReadMessageIDArray = nil;
  }

  DBResultSet *r = [[[[DBQBChatMessage query]
      whereWithFormat:@" DialogID = %@", self.dialog.ID] orderBy:@"DateSent"]

      fetch];
  if (r.count == 0) {
    [[GetChatMessageToJsqMessageArrayModel shared]
        whenDBNotMessageAddMessagesFromQBToJSQMessageArrayForDialog:self.dialog
                                                           chatRoom:self];
  } else {
    [[GetChatMessageToJsqMessageArrayModel shared]
        addMessagesFromQBToJSQMessageArrayForDialog:self.dialog
                                           chatRoom:self];
  }

  if (self.firstTimeEnterChatRoom) {
    [self getImageFromDBQBChatMessageToMWPhotoBrower];
    self.firstTimeEnterChatRoom = NO;
    self.automaticallyScrollsToMostRecentMessage = NO;
    //// readAllReceiveMessage
    [self readAllReceivceMessage:5];
  }
}
- (void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];

  [self readAllReceivceMessage:1];

  [self setDialogLastMessageFromChatroomLastMessage];
  //[[ChatService
  // shared]removeUnreadLabelMessageFromDBmessagesArrayJsqMessageArrayForDialogId:self.dialog.ID];
  [[NSNotificationCenter defaultCenter] removeObserver:self];
}
#pragma mark Support Method
- (void)setUpFixedLabelTextAccordingToSelectedLanguage {
  self.connectingLabel.text =
      JMOLocalizedString(@"ChatPageDailog.Connecting...", nil);
  [self.inputToolbar.contentView.rightBarButtonItem
      setTitle:JMOLocalizedString(@"ChatPageChatRoom.Send", nil)
      forState:UIControlStateNormal];
  self.inputToolbar.contentView.rightBarButtonItem.titleLabel.font =
      [UIFont systemFontOfSize:13];
  [self.inputToolbar.contentView.textView
      setPlaceHolder:JMOLocalizedString(@"ChatPageChatRoom.New Message", nil)];
}
- (void)setDialogLastMessageFromChatroomLastMessage {
  if (![self IsEmpty:[[ChatService shared]
                             .chatroomJsqMessageArray
                         objectForKey:self.dialog.ID]]) {
    JSQMessage *jsqmessage = [[[ChatService shared]
                                   .chatroomJsqMessageArray
        objectForKey:self.dialog.ID] lastObject];

    self.dialog.lastMessageText = jsqmessage.Text;
    self.dialog.unreadMessagesCount = 0;
    self.dialog.lastMessageDate = jsqmessage.DateSent;

    DBResultSet *r = [[[DBQBChatDialog query]
        whereWithFormat:@" DialogID = %@", self.dialog.ID] fetch];

    for (DBQBChatDialog *dbqbchatdialog in r) {
      dbqbchatdialog.LastMessageText = jsqmessage.Text;
      dbqbchatdialog.QBChatDialog =
          [NSKeyedArchiver archivedDataWithRootObject:[self.dialog copy]];
      dbqbchatdialog.LastMessageDate = jsqmessage.DateSent;

      [dbqbchatdialog commit];
    }
  }
}
- (BOOL)IsEmpty:(id)thing {
  return thing == nil || ([thing respondsToSelector:@selector(length)] &&
                          [(NSData *)thing length] == 0) ||
         ([thing respondsToSelector:@selector(count)] &&
          [(NSArray *)thing count] == 0);
}

#pragma mark MWPhotoBrowserDelegate-------------------------------------------------------------------------------------------------------------
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
  return self.mwPhotosBrowerPhotoArray.count;
}

- (id<MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser
               photoAtIndex:(NSUInteger)index {
  if (index < self.mwPhotosBrowerPhotoArray.count)
    return [self.mwPhotosBrowerPhotoArray objectAtIndex:index];
  return nil;
}
- (void)getImageFromDBQBChatMessageToMWPhotoBrower {
  dispatch_queue_t backgroundQueue =
      dispatch_queue_create("com.mycompany.myqueue", 0);

  dispatch_barrier_async(backgroundQueue, ^{

    DBResultSet *r = [[[[DBQBChatMessage query]
        whereWithFormat:@" DialogID = %@ AND PhotoType =%@", self.dialog.ID,
                        @"1"] orderBy:@"Id"] fetch];

    self.mwPhotosBrowerPhotoArray = [[NSMutableArray alloc] init];
    for (DBQBChatMessage *p in r) {
      if (![self IsEmpty:[p.CustomParameters objectForKey:@"photourl"]]) {
        NSURL *url =
            [NSURL URLWithString:[p.CustomParameters objectForKey:@"photourl"]];
        [self.mwPhotosBrowerPhotoArray addObject:[MWPhoto photoWithURL:url]];
      }
    }

  });
}

#pragma mark TopBarLoginStatusView-----------------------------------------------------------------------------------------------------------------------
- (void)chatRoomConnectToServerSuccess {
  self.navigationItem.titleView = self.titleView;
}
- (void)chatDidAccidentallyDisconnectNotification {
  self.navigationItem.titleView = self.connectingServerView;
}
////getRecipienterFromQBServer myloginstatus
- (void)getRecipienterFromQBServer {
  [QBRequest userWithID:[self.recipientID integerValue]
      successBlock:^(QBResponse *response, QBUUser *user) {

        DDLogInfo(@"getRecipienterFromQBServer recipienterlastlogintime-->%@",
                  user.lastRequestAt);

        self.recipienterLastLoginTime = user.lastRequestAt;
        self.recipienterLastLoginTimeString =
            [self recipienterLastLoginTimeString:user.lastRequestAt
                                     NowDateTime:[NSDate date]];

        self.typingLabel.text = self.recipienterLastLoginTimeString;

        NSError *err = nil;

      }
      errorBlock:^(QBResponse *response) {
        DDLogError(@"QBResponse %@", response);
      }];
}
//// loginStringForTimeIntervalDateTime
- (NSString *)recipienterLastLoginTimeString:(NSDate *)dateTime
                                 NowDateTime:(NSDate *)nowDateTime {
  //  NSInteger DayInterval;
  // NSInteger DayModules;
  //// only between day

  NSString *dateTimestring = [self.Onlydayformat stringFromDate:dateTime];
  NSString *nowDateTimestring = [self.Onlydayformat stringFromDate:nowDateTime];
  DDLogInfo(@"dateTimestring-->%@", dateTimestring);
  DDLogInfo(@"nowDateTimestring-->%@", nowDateTimestring);
  NSDate *dateTimeOnlydayformat =
      [self.Onlydayformat dateFromString:dateTimestring];
  NSDate *nowDateOnlydayformat =
      [self.Onlydayformat dateFromString:nowDateTimestring];
  NSCalendar *gregorianCalendar = [[NSCalendar alloc]
      initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
  NSDateComponents *components =
      [gregorianCalendar components:NSCalendarUnitDay
                           fromDate:dateTimeOnlydayformat
                             toDate:nowDateOnlydayformat
                            options:NSCalendarWrapComponents];
  DDLogInfo(@"components.day-->%ld", (long)components.day);

  //// only between time
  NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];

  NSInteger interval = abs((int)[dateTime timeIntervalSinceDate:nowDateTime]);
  if (components.day >= 2) {
    [dateFormat setDateFormat:@"yyyy/MM/dd h:mm a"];
    return [NSString
        stringWithFormat:@"login in %@", [dateFormat stringFromDate:dateTime]];

  } else {
    if (components.day == 1) {
      [dateFormat setDateFormat:@"h:mm a"];
      return [NSString stringWithFormat:@"login in yesterday at %@",
                                        [dateFormat stringFromDate:dateTime]];
    }
    //// components.day==0
    else if (interval > 4) {
      [dateFormat setDateFormat:@"h:mm a"];
      return [NSString stringWithFormat:@"login in today at %@",
                                        [dateFormat stringFromDate:dateTime]];

    } else {
      return @"online";
    }
  }
}
- (BOOL)hidesBottomBarWhenPushed {
  return YES;
}
//// backPressed
- (void)chatDidReceiveUserIsTypingFromUserWithID:(NSUInteger)userID {
  // opponent is typing - update UI
  if ([self.recipienterLastLoginTimeString isEqualToString:@"online"]) {
    self.typingLabel.text = @"typing...";
    dispatch_time_t popTime =
        dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
      // Do something...
      self.typingLabel.text = self.recipienterLastLoginTimeString;
    });
  }
}
- (void)chatDidReceiveUserStopTypingFromUserWithID:(NSUInteger)userID {
  // opponent has stopped typing - update UI
  self.typingLabel.text = self.recipienterLastLoginTimeString;
}
- (void)backPressed:(UIBarButtonItem *)sender {
  self.messageCellTopLabelDate = nil;
  [self.inputToolbar.contentView.textView resignFirstResponder];
  [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark sendMessage-------------------------------------------------------------------------------------------------------------------------
- (void)sendMessageMethod {
  self.unReadMessageIDArray = nil;

  [[ChatRoomSendMessageModel shared]
      sendMessageInsertIntoDataBase:self.inputToolbar.contentView.textView.text
                          forDialog:self.dialog];
  // Reload table
  [JSQSystemSoundPlayer jsq_playMessageSentSound];
  [self.collectionView reloadData];
  [self scrollToBottomAnimated:YES];
}
//// - JSQMessagesViewController method overrides didPressSendButton
- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                  senderId:(NSString *)senderId
         senderDisplayName:(NSString *)senderDisplayName
                      date:(NSDate *)date {
  DDLogInfo(@"didPressSendButton");
  if (self.checkHaveNotCountOfDialogsWithOccupants_IdsAllInDialogs) {
    [QBRequest createDialog:self.dialog
        successBlock:^(QBResponse *response, QBChatDialog *createdDialog) {

          self.dialog = createdDialog;
          self.checkHaveNotCountOfDialogsWithOccupants_IdsAllInDialogs = NO;
          [self sendMessageMethod];
          [JSQSystemSoundPlayer jsq_playMessageSentSound];

          [self finishSendingMessageAnimated:YES];

        }
        errorBlock:^(QBResponse *response) {
          DDLogError(@"QBResponse %@", response);
        }];
  } else {
    [self sendMessageMethod];
    [JSQSystemSoundPlayer jsq_playMessageSentSound];

    [self finishSendingMessageAnimated:YES];
  }
}

- (void)didPressAccessoryButton:(UIButton *)sender {
  UIActionSheet *sheet =
      [[UIActionSheet alloc] initWithTitle:nil
                                  delegate:self
                         cancelButtonTitle:@"Cancel"
                    destructiveButtonTitle:nil
                         otherButtonTitles:@"Photo Library", nil];

  [sheet showFromToolbar:self.inputToolbar];
}
- (void)actionSheet:(UIActionSheet *)actionSheet
    didDismissWithButtonIndex:(NSInteger)buttonIndex {
  ChatRoomSelectPhoto *chatroomselectphoto = [[ChatRoomSelectPhoto alloc] init];
  if (buttonIndex == actionSheet.cancelButtonIndex) {
    return;
  }

  switch (buttonIndex) {
    case 0:
      [self.inputToolbar.contentView.textView resignFirstResponder];

      [self.navigationController pushViewController:chatroomselectphoto
                                           animated:YES];
      break;
  }
}

#pragma mark JSQMessages CollectionView DataSource----------------------------------------------------------------------------------------------
- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView
       messageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
  // DDLogInfo(@"messageDataForItemAtIndexPath");

  NSMutableArray *jsqmessages =
      [[ChatService shared]
              .chatroomJsqMessageArray objectForKey:self.dialog.ID];

  return [
      [[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID]
      objectAtIndex:indexPath.item];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:
                                           (JSQMessagesCollectionView *)
                                               collectionView
             messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
  /**
   *  You may return nil here if you do not want bubbles.
   *  In this case, you should set the background color of your collection view
   *cell's textView.
   *
   *  Otherwise, return your previously created bubble image data objects.
   */

  JSQMessage *message = [
      [[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID]
      objectAtIndex:indexPath.item];

  if (![self IsEmpty:self.unReadMessageIDArray]) {
    for (NSString *unReadMessageID in self.unReadMessageIDArray) {
      if ([message.MessageID isEqualToString:unReadMessageID]) {
        JSQMessagesBubbleImageFactory *bubbleFactory =
            [[JSQMessagesBubbleImageFactory alloc] init];

        if ([message.senderId isEqualToString:self.senderId]) {
          return [bubbleFactory
              outgoingMessagesBubbleImageWithColor:[UIColor lightGrayColor]];
        }

        return [bubbleFactory
            incomingMessagesBubbleImageWithColor:[UIColor lightGrayColor]];
      }
    }
  }

  DDLogInfo(@"messageBubbleImageDataForItemAtIndexPath");

  if ([message.senderId isEqualToString:self.senderId]) {
    return [ChatService shared].outgoingBubbleImageData;
  }

  return [ChatService shared].incomingBubbleImageData;
}

- (id<JSQMessageAvatarImageDataSource>)
                   collectionView:(JSQMessagesCollectionView *)collectionView
avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
  return nil;
}

- (NSAttributedString *)collectionView:
                            (JSQMessagesCollectionView *)collectionView
    attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath {
  /**
   *  This logic should be consistent with what you return from
   *`heightForCellTopLabelAtIndexPath:`
   *  The other label text delegate methods should follow a similar pattern.
   *
   *  Show a timestamp for every 3rd message
   */
  JSQMessage *message = [
      [[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID]
      objectAtIndex:indexPath.item];

  DDLogInfo(@"[_messageDateFormatter stringFromDate:message.date]-->%@",
            [_messageTopLabelDateFormatter stringFromDate:message.date]);

  return [[NSAttributedString alloc]
      initWithString:[_messageTopLabelDateFormatter
                         stringFromDate:message.date]];

  return nil;
}
//// not use it.
- (NSAttributedString *)collectionView:
                            (JSQMessagesCollectionView *)collectionView
    attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath {
  DDLogInfo(@"attributedTextForMessageBubbleTopLabelAtIndexPath");

  JSQMessage *message = [
      [[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID]
      objectAtIndex:indexPath.item];

  /**
   *  iOS7-style sender name labels
   */
  if ([message.senderId isEqualToString:self.senderId]) {
    return nil;
  }

  if (indexPath.item > 0) {
    JSQMessage *previousMessage = [[[ChatService shared]
                                        .chatroomJsqMessageArray
        objectForKey:self.dialog.ID] objectAtIndex:indexPath.item];
    if ([[previousMessage senderId] isEqualToString:message.senderId]) {
      return nil;
    }
  }

  /**
   *  Don't specify attributes to use the defaults.
   */
  return [[NSAttributedString alloc] initWithString:message.senderDisplayName];
}
//// not use it.
- (NSAttributedString *)collectionView:
                            (JSQMessagesCollectionView *)collectionView
    attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath {
  return nil;
}

//// - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
  NSMutableArray *chatroomJsqMessageArray =
      [[ChatService shared]
              .chatroomJsqMessageArray objectForKey:self.dialog.ID];
  DDLogDebug(@"numberOfItemsInSection %lu",
             (unsigned long)chatroomJsqMessageArray.count);
  return chatroomJsqMessageArray.count;
}

- (UICollectionViewCell *)collectionView:
                              (JSQMessagesCollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  /**
   *  Override point for customizing cells
   */
  JSQMessagesCollectionViewCell *cell =
      (JSQMessagesCollectionViewCell *)[super collectionView:collectionView
                                      cellForItemAtIndexPath:indexPath];

  DDLogInfo(@"cellForItemAtIndexPath-->%ld", (long)indexPath.item);
  JSQMessage *jsqmessage = [
      [[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID]
      objectAtIndex:indexPath.item];
  NSString *messageSenderId = [jsqmessage senderId];
  BOOL isOutgoingMessage = [messageSenderId isEqualToString:self.senderId];
  if (!jsqmessage.isMediaMessage) {
    cell.textView.textColor = [UIColor blackColor];
    cell.textView.linkTextAttributes = @{
      NSForegroundColorAttributeName : cell.textView.textColor,
      NSUnderlineStyleAttributeName :
          @(NSUnderlineStyleSingle | NSUnderlinePatternSolid)
    };
  }
  //// remove all tag 50 view.
  NSArray *viewsToRemove = [cell subviews];
  for (UIView *v in viewsToRemove) {
    if (v.tag == 50) {
      [v removeFromSuperview];
    }
  }
  //// ChatRoomCoverAgentView
  if (![self IsEmpty:[jsqmessage.CustomParameters
                         objectForKey:@"agentlistingid"]]) {
    ChatRoomCoverAgentView *myViewObject =
        [[[NSBundle mainBundle] loadNibNamed:@"ChatRoomCoverAgentView"
                                       owner:self
                                     options:nil] objectAtIndex:0];
    myViewObject.tag = 50;
    myViewObject.follower.text = @"534543";
    [myViewObject setFrame:CGRectMake(myViewObject.frame.origin.x, 20,
                                      myViewObject.frame.size.width,
                                      myViewObject.frame.size.height)];
    [cell addSubview:myViewObject];
    return cell;
  }
  CellDateLabelView *celldatelabelview =
      [[[NSBundle mainBundle] loadNibNamed:@"CellDateLabelView"
                                     owner:self
                                   options:nil] objectAtIndex:0];
  [celldatelabelview.datelabel
      setText:[self.messageDateLabelFormatter stringFromDate:jsqmessage.date]];

  //// set up rightsidemessagedatelabel
  if (isOutgoingMessage) {
    [celldatelabelview setupTickImageView:jsqmessage];
    //// add 0.1 white color overlay
    [celldatelabelview
        setFrame:CGRectMake(230, cell.frame.size.height - 20, 60, 10)];
    if (cell.mediaView != nil) {
      [celldatelabelview
          setFrame:CGRectMake(230, cell.frame.size.height - 30, 70, 10)];
      UIView *overlay = [[UIView alloc] initWithFrame:celldatelabelview.frame];
      [overlay
          setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0.1]];
      overlay.tag = 50;
      [overlay addSubview:celldatelabelview];
      [cell addSubview:overlay];
    }
  } else {
    //// set up leftidemessagedatelabel
    [celldatelabelview
        setFrame:CGRectMake(10, cell.frame.size.height - 20, 60, 10)];
    cell.datelabel.textAlignment = NSTextAlignmentRight;
    if (cell.mediaView != nil) {
      [celldatelabelview
          setFrame:CGRectMake(10, cell.frame.size.height - 30, 60, 10)];
      //// add 0.1 white color overlay
      UIView *overlay = [[UIView alloc] initWithFrame:celldatelabelview.frame];
      [overlay
          setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0.1]];
      overlay.tag = 50;
      [overlay addSubview:celldatelabelview];
      [cell addSubview:overlay];
    }
  }
  [cell addSubview:celldatelabelview];
  return cell;
}

#pragma mark JSQMessages collection view flow layout
- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                              layout:(JSQMessagesCollectionViewFlowLayout *)
                                         collectionViewLayout
    heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath {
  JSQMessage *message = [
      [[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID]
      objectAtIndex:indexPath.item];

  if (indexPath.item == 0) {
    self.messageCellTopLabelDate =
        [self.Onlydayformat stringFromDate:message.date];
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
  }

  if ([self.messageCellTopLabelDate
          isEqualToString:[self.Onlydayformat stringFromDate:message.date]]) {
    if (indexPath.item % 5 == 0) {
      return kJSQMessagesCollectionViewCellLabelHeightDefault;
    } else {
      return 0;
    }
  } else {
    self.messageCellTopLabelDate =
        [self.Onlydayformat stringFromDate:message.date];

    return kJSQMessagesCollectionViewCellLabelHeightDefault;
  }

  return 0;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                                       layout:
                                           (JSQMessagesCollectionViewFlowLayout
                                                *)collectionViewLayout
    heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath {
  /**
   *  iOS7-style sender name labels
   */

  return 0;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                                 layout:(JSQMessagesCollectionViewFlowLayout *)
                                            collectionViewLayout
    heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath {
  return 0;
}

#pragma mark Responding to collection view tap
- (BOOL)collectionView:(UICollectionView *)collectionView
      canPerformAction:(SEL)action
    forItemAtIndexPath:(NSIndexPath *)indexPath
            withSender:(id)sender {
  if (action == @selector(customAction:)) {
    return YES;
  }

  return [super collectionView:collectionView
              canPerformAction:action
            forItemAtIndexPath:indexPath
                    withSender:sender];
}

- (void)collectionView:(UICollectionView *)collectionView
         performAction:(SEL)action
    forItemAtIndexPath:(NSIndexPath *)indexPath
            withSender:(id)sender {
  if (action == @selector(customAction:)) {
    [self customAction:sender];
    return;
  }

  [super collectionView:collectionView
           performAction:action
      forItemAtIndexPath:indexPath
              withSender:sender];
}

- (void)customAction:(id)sender {
  DDLogInfo(@"Custom action received! Sender: %@", sender);

  [[[UIAlertView alloc] initWithTitle:@"Custom Action"
                              message:nil
                             delegate:nil
                    cancelButtonTitle:@"OK"
                    otherButtonTitles:nil] show];
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView
                             header:
                                 (JSQMessagesLoadEarlierHeaderView *)headerView
    didTapLoadEarlierMessagesButton:(UIButton *)sender {
  [[GetChatMessageToJsqMessageArrayModel shared]
      getEarlierChatMessageFromDBToJsqMessageArrayForDialog:self.dialog
                                                   chatRoom:self];
  DDLogInfo(@"Load earlier messages!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView
 didTapAvatarImageView:(UIImageView *)avatarImageView
           atIndexPath:(NSIndexPath *)indexPath {
  DDLogInfo(@"Tapped avatar!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView
    didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath {
  DDLogInfo(@"Tapped message bubble!");

  DDLogInfo(@"indexPath.row%ld", (long)indexPath.row);

  if ([self
          IsEmpty:[[[ChatService shared]
                          .chatroomJsqMessageArray objectForKey:self.dialog.ID]
                      objectAtIndex:indexPath.item]]) {
    return;
  }

  [self.inputToolbar.contentView.textView resignFirstResponder];

  JSQMessage *jsqmessage = [
      [[ChatService shared].chatroomJsqMessageArray objectForKey:self.dialog.ID]
      objectAtIndex:indexPath.item];

  if ([self IsEmpty:[jsqmessage.CustomParameters objectForKey:@"photourl"]]) {
    return;
  }

  id<JSQMessageMediaData> copyMediaData = jsqmessage.media;

  if ([copyMediaData isKindOfClass:[JSQPhotoMediaItem class]]) {
    dispatch_queue_t backgroundQueue =
        dispatch_queue_create("com.mycompany.myqueue", 0);

    dispatch_barrier_async(backgroundQueue, ^{
      int imageCellIndex = 0;
      self.mwPhotoBrowser = [[MWPhotoBrowser alloc] initWithDelegate:self];
      self.mwPhotoBrowser.displayActionButton = YES;
      self.mwPhotoBrowser.displayNavArrows = NO;
      self.mwPhotoBrowser.displaySelectionButtons = NO;
      self.mwPhotoBrowser.zoomPhotosToFill = YES;
      self.mwPhotoBrowser.alwaysShowControls = NO;
      self.mwPhotoBrowser.autoPlayOnAppear = NO;
      self.mwPhotoBrowser.enableGrid = YES;
      self.mwPhotoBrowser.startOnGrid = NO;

      for (MWPhoto *mwphoto in self.mwPhotosBrowerPhotoArray) {
        NSString *dbQBChatMessagePhotoUrlString =
            [jsqmessage.CustomParameters objectForKey:@"photourl"];

        if ([dbQBChatMessagePhotoUrlString
                isEqualToString:[mwphoto.photoURL absoluteString]]) {
          [self.mwPhotoBrowser setCurrentPhotoIndex:imageCellIndex];

          UINavigationController *nc = [[UINavigationController alloc]
              initWithRootViewController:self.mwPhotoBrowser];
          nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
          nc.navigationBar.titleTextAttributes =
              @{NSForegroundColorAttributeName : [UIColor whiteColor]};
          [self presentViewController:nc animated:YES completion:nil];
          break;
        }

        imageCellIndex++;
      }

    });
  }
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView
 didTapCellAtIndexPath:(NSIndexPath *)indexPath
         touchLocation:(CGPoint)touchLocation {
  [self.inputToolbar.contentView.textView resignFirstResponder];
  DDLogInfo(@"Tapped cell at %@!", NSStringFromCGPoint(touchLocation));
}

#pragma mark ChatServiceDelegate--------------------------------------------------------------------------------------------------------------
- (void)chatDidConnect {
  [ChatService shared].delegate = self;
  [ChatService shared].connecttoserversuccess = YES;

  [self.inputToolbar toggleSendButtonEnabled];
  self.inputToolbar.contentView.leftBarButtonItem.enabled = YES;

  DBResultSet *r = [[[[DBQBChatMessage query]
      whereWithFormat:@" DialogID = %@", self.dialog.ID] orderBy:@"DateSent"]

      fetch];

  if (r.count == 0) {
    [[GetChatMessageToJsqMessageArrayModel shared]
        whenDBNotMessageAddMessagesFromQBToJSQMessageArrayForDialog:self.dialog
                                                           chatRoom:self];

  } else {
    [[GetChatMessageToJsqMessageArrayModel shared]
        addMessagesFromQBToJSQMessageArrayForDialog:self.dialog
                                           chatRoom:self];
  }
}
- (void)chatDidFailWithStreamError:(NSError *)error {
  [MBProgressHUD hideHUDForView:self.view animated:YES];
  [ChatService shared].connecttoserversuccess = NO;

  self.inputToolbar.contentView.rightBarButtonItem.enabled = NO;
  self.inputToolbar.contentView.leftBarButtonItem.enabled = NO;
}
- (void)viewDidLayoutSubviews {
  [self scrollToBottomAnimated:NO];
}
- (BOOL)chatDidReceiveMessage:(QBChatMessage *)chatmessage {
  DDLogInfo(@"QBChatMessage *)message-->%@", chatmessage);

  if (chatmessage.senderID != self.dialog.recipientID) {
    return NO;
  }

  //  [[ChatService
  //  shared]removeUnreadLabelMessageFromDBmessagesArrayJsqMessageArrayForDialogId:self.dialog.ID];

  self.unReadMessageIDArray = nil;
  [[ChatService shared]
      whenReceiveMessageAddMessageToJsqmessageArray:chatmessage
                                        forDialogId:chatmessage.dialogID];
  //// photomessage  insert in to mwphotobrowerarray
  if (![self IsEmpty:[chatmessage.customParameters objectForKey:@"photourl"]]) {
    [self.mwPhotosBrowerPhotoArray
        addObject:[MWPhoto
                      photoWithURL:
                          [NSURL URLWithString:[chatmessage.customParameters
                                                   objectForKey:@"photourl"]]]];
  }

  [self chatRoomCollectionViewReloadOnly];
  [self chatRoomCollectionViewscrollToBottomYesAnimated];
  [self readAllReceivceMessage:1];
  AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
  AudioServicesPlaySystemSound(1003);
  return YES;
}
- (void)chatDidDeliverMessageWithID:(DBQBChatMessage *)message {
  if (![self IsEmpty:[[ChatService shared]
                             .chatroomJsqMessageArray
                         objectForKey:self.dialog.ID]]) {
    NSMutableArray *messagearray =
        [[ChatService shared]
                .chatroomJsqMessageArray objectForKey:self.dialog.ID];

    for (JSQMessage *jsqmessage in [messagearray reverseObjectEnumerator]) {
      if (!jsqmessage.DidDeliverStatus) {
        if ([jsqmessage.MessageID isEqualToString:message.MessageID]) {
          jsqmessage.DidDeliverStatus = YES;
        }
        if ([jsqmessage.DateSent compare:message.DateSent] ==
            NSOrderedDescending) {
          DDLogInfo(@"date1 is later than date2");

        } else if ([jsqmessage.DateSent compare:message.DateSent] ==
                   NSOrderedAscending) {
          DDLogInfo(@"date1 is earlier than date2");
          jsqmessage.DidDeliverStatus = YES;
        } else {
          DDLogInfo(@"dates are the same");
          jsqmessage.DidDeliverStatus = YES;
        }
      }
    }
  }

  [self.collectionView reloadData];
}
- (void)chatRoomCollectionViewReloadOnly {
  [self.collectionView reloadData];
}
- (void)chatRoomCollectionViewscrollToBottomYesAnimated {
  [self scrollToBottomAnimated:YES];
  [MBProgressHUD hideHUDForView:self.view animated:YES];
}
- (void)chatRoomGetImageFromDBQBChatMessageToMWPhotoBrower {
  AudioServicesPlaySystemSound(1003);
  [self getImageFromDBQBChatMessageToMWPhotoBrower];
}
- (void)chatDidReadMessageWithID:(DBQBChatMessage *)message {
  if (![self IsEmpty:[[ChatService shared]
                             .chatroomJsqMessageArray
                         objectForKey:self.dialog.ID]]) {
    NSMutableArray *messagearray =
        [[ChatService shared]
                .chatroomJsqMessageArray objectForKey:self.dialog.ID];

    for (JSQMessage *jsqmessage in [messagearray reverseObjectEnumerator]) {
      if (!jsqmessage.ReadStatus) {
        if ([jsqmessage.MessageID isEqualToString:message.MessageID]) {
          jsqmessage.ReadStatus = YES;
        }

        if ([jsqmessage.DateSent compare:message.DateSent] ==
            NSOrderedDescending) {
          DDLogInfo(@"date1 is later than date2");

        } else if ([jsqmessage.DateSent compare:message.DateSent] ==
                   NSOrderedAscending) {
          DDLogInfo(@"date1 is earlier than date2");
          jsqmessage.ReadStatus = YES;

        } else {
          DDLogInfo(@"dates are the same");
          jsqmessage.ReadStatus = YES;
        }
      }
    }
  }

  [self.collectionView reloadData];
}
//// read all message
- (void)readAllReceivceMessage:(int)totalReadReceivceMessageCount {
  int readReceivceMessageCount = 0;
  [QBRequest markMessagesAsRead:nil
                       dialogID:self.dialog.ID
                   successBlock:^(QBResponse *response) {

                   }
                     errorBlock:nil];

  if (![self IsEmpty:[[ChatService shared]
                             .chatroomJsqMessageArray
                         objectForKey:self.dialog.ID]]) {
    for (JSQMessage *jsqmessage in [[[ChatService shared]
                                         .chatroomJsqMessageArray
             objectForKey:self.dialog.ID] reverseObjectEnumerator]) {
      if ([[LoginRecordModel shared].myLoginRecord.QBID integerValue] !=
          jsqmessage.SenderID) {
        QBChatMessage *chatmessage = [NSKeyedUnarchiver
            unarchiveObjectWithData:jsqmessage.QBChatHistoryMessage];

        if ([[QBChat instance] readMessage:chatmessage]) {
          DDLogInfo(@"message markable YES");
        } else {
          DDLogError(@"readMessage false");
        }
        readReceivceMessageCount++;
        if (readReceivceMessageCount >= totalReadReceivceMessageCount) {
          break;
        }
      }
    }
  }
}

#pragma mark Text view
- (void)textViewDidBeginEditing:(UITextView *)textView {
  if (textView != self.inputToolbar.contentView.textView) {
    return;
  }

  [textView becomeFirstResponder];

  [self scrollToBottomAnimated:YES];
}
- (void)textViewDidChange:(UITextView *)textView {
  if (textView != self.inputToolbar.contentView.textView) {
    return;
  }
  if ([ChatService shared].connecttoserversuccess) {
    [self.inputToolbar toggleSendButtonEnabled];
  }
  [self.dialog sendUserIsTyping];
}
- (void)textViewDidEndEditing:(UITextView *)textView {
  if (textView != self.inputToolbar.contentView.textView) {
    return;
  }

  [textView resignFirstResponder];
  [self.dialog sendUserStoppedTyping];
}

@end
