//
//  MemberPerformanceViewController.m
//  FWD
//
//  Created by ios Developer 5 on 18/8/14.
//  Copyright (c) 2014 hohojo. All rights reserved.
//

#import "VendorSignupPage.h"
#import "VendorSignUpModel.h"
#import "VendorLoginPage.h"
#import "UploadProductphoto.h"
#import "SaveQBIDbyEidModel.h"
#import "FireBaseChatLoginModel.h"
@interface VendorSignupPage ()

@end
@implementation VendorSignupPage

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [[[AppDelegate getAppDelegate] getTabBarController] setTabBarHidden:YES];
}
- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];

  self.navigationItem.title = @"VendorSignUp";
}
- (void)viewDidLoad {
  //[self performSelector:@selector(layout) withObject:nil afterDelay:2.0];
  [super viewDidLoad];
}

- (IBAction)btnLoginPressed:(id)sender {
  VendorLoginPage *UIViewController =
      [[VendorLoginPage alloc] initWithNibName:@"VendorLoginPage" bundle:nil];
  [self.navigationController pushViewController:UIViewController animated:YES];
}
- (IBAction)btnSignupPressed:(id)sender {
  BOOL error = NO;
  NSString *usernameerrormessage = @"";
  NSString *passworderrormessage = @"";
  NSString *emailerrormessage = @"";

  if ([self IsEmpty:self.username.text]) {
    usernameerrormessage = @"Username can not empty";
    error = YES;
  }
  if ([self IsEmpty:self.password.text]) {
    passworderrormessage = @"Password can not empty";
    error = YES;
  }
  if ([self IsEmpty:self.email.text]) {
    emailerrormessage = @"Email can not empty";

    error = YES;
  }
  if (!error) {
    [self loadingAndStopLoadingAfterSecond:8];
      [LoginRecordModel shared].myLoginRecord.username =self.email.text;
      [LoginRecordModel shared].myLoginRecord.password =self.password.text;
    [[VendorSignUpModel shared] SignUpApiByUserName:self.username.text
        Email:self.email.text
        Password:self.password.text
        success:^(LoginRecord *myLoginRecord) {

          [self vendorLoginApi];
        }

        failure:^(AFHTTPRequestOperation *operation, NSError *error,
                  NSString *errorMessage, int errorStatus, NSString *alert,
                  NSString *ok) {
          [MBProgressHUD hideHUDForView:self.view animated:YES];
          UIAlertView *alertView =
              [[UIAlertView alloc] initWithTitle:alert
                                         message:errorMessage
                                        delegate:nil
                               cancelButtonTitle:ok
                               otherButtonTitles:nil];
          [alertView show];

        }];
  }

  if (error) {
    NSString *errormessage =
        [NSString stringWithFormat:@"%@\n%@\n%@", usernameerrormessage,
                                   passworderrormessage, emailerrormessage];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                    message:errormessage
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
  }
}
- (void)vendorLoginApi {
    
    [[LoginRecordModel shared] LoginApiByEmail:self.email.text
                                      Password:self.password.text
                                       success:^(LoginRecord *myLoginRecord) {
                                           
                                           [[FireBaseChatLoginModel shared] loginByQuickBloxOrSignUpQuickBlox];
                                           [self loginSuccessMoveToTopPage];
                                       }
     
                                       failure:^(AFHTTPRequestOperation *operation, NSError *error,
                                                 NSString *errorMessage, int errorStatus, NSString *alert,
                                                 NSString *ok) {
                                           [MBProgressHUD hideHUDForView:self.view animated:YES];
                                           UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alert
                                                                                               message:errorMessage
                                                                                              delegate:nil
                                                                                     cancelButtonTitle:ok
                                                                                     otherButtonTitles:nil];
                                           [alertView show];
                                           
                                       }];
}
- (void)loginSuccessMoveToTopPage {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                                    message:@"SignUp Success"
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    
    [alert show];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end