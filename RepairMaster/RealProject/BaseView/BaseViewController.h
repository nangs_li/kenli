//
//  BaseViewController.h
//  FWD
//
//  Created by Raymond Chan on 31/3/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HandleServerReturnString.h"
#import "MBProgressHUD.h"
#import "SCLAlertView.h"
@interface BaseViewController : UIViewController<MBProgressHUDDelegate>
@property(strong, nonatomic) AppDelegate *delegate;
@property(nonatomic) BOOL showNavRightButton;
@property(strong, nonatomic) MBProgressHUD *hud;
#pragma mark d object, jsonstring
@property(strong, nonatomic) HandleServerReturnString *HandleServerReturnString;

@property(strong, nonatomic) NSString *jsonstring;

#pragma mark - Public Methods

- (void)showDXAlertView;
- (void)btnBackPressed:(id)sender;
- (void)btnNavRightPressed:(id)sender;
- (void)btnSharePressed:(id)sender;
- (void)btnEditPressed:(id)sender;
- (void)btnSavePressed:(id)sender;
- (void)listSubviewsOfView:(UIView *)view;
- (void)loadingAndStopLoadingAfterSecond:(int)second;
- (BOOL)IsEmpty:(id)thing;
@end
