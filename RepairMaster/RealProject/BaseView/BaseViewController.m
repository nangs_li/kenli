//
//  BaseViewController.m
//  FWD
//
//  Created by Raymond Chan on 31/3/14.
//  Copyright (c) 2014 ken All rights reserved.
//

#import "BaseViewController.h"
#import "DXAlertView.h"

@interface BaseViewController ()
@end

@implementation BaseViewController
// init
- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
  }
  return self;
}

#pragma mark - APIEngine Delegate  Server Return String
- (void)requestDone:(NSMutableDictionary *)dict {
  DDLogVerbose(@"requestDone currentview:-->[%@]", [self class]);
}
#pragma mark - APIEngine Delegate  Server Return Fail String
- (void)requestFail:(NSMutableDictionary *)dict {
  // call apiConnectFailMessageWithDict
}

#pragma mark - View lifecycle
- (void)viewDidLoad {
  [super viewDidLoad];
  _delegate = [AppDelegate getAppDelegate];
  _hud.labelText = @"loading";
}

- (void)viewDidLayoutSubviews {
  DDLogInfo(@"viewDidLayoutSubviews currentview:-->[%@]", [self class]);
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];

  DDLogInfo(@"viewDidAppear currentview:-->[%@]", [self class]);
}
- (void)listSubviewsOfView:(UIView *)view {
  // Get the subviews of the view
  NSArray *subviews = [view subviews];

  // Return if there are no subviews
  if ([subviews count] == 0) return;  // COUNT CHECK LINE

  for (UIView *subview in subviews) {
    // Do what you want to do with the subview
    NSLog(@"subview-->%@", subview);

    // List the subviews of subview
    [self listSubviewsOfView:subview];
  }
}
#pragma mark - Form Actions
- (void)btnBackPressed:(id)sender {
  // previous page
  [self.navigationController popViewControllerAnimated:YES];
}

- (void)btnNavRightPressed:(id)sender {
}
- (void)btnEditPressed:(id)sender {
}

- (void)btnSavePressed:(id)sender {
}
#pragma mark - DXAlertView
- (void)showDXAlertView {
}
#pragma mark - loading
- (void)loadingAndStopLoadingAfterSecond:(int)second {
  _hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];

  _hud.delegate = self;
  _hud.labelText = @"loading";

  dispatch_time_t popTime =
      dispatch_time(DISPATCH_TIME_NOW, second * NSEC_PER_SEC);
  dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
    // Do something...
    [MBProgressHUD hideHUDForView:self.view animated:YES];
  });
}
#pragma check empty
- (BOOL)IsEmpty:(id)thing {
  if (thing == [NSNull null]) {
    return YES;
  }
  return thing == nil || ([thing respondsToSelector:@selector(length)] &&
                          [(NSData *)thing length] == 0) ||
         ([thing respondsToSelector:@selector(count)] &&
          [(NSArray *)thing count] == 0);
}
@end
