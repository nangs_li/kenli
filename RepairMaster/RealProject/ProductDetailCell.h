//
//  TableViewCell.h
//  S18
//
//  Created by ios Developer 5 on 4/8/14.
//  Copyright (c) 2014 ios Developer 5. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductDetailCell.h"
@interface ProductDetailCell : UITableViewCell
@property(strong, nonatomic) IBOutlet UILabel *productname;
@property(strong, nonatomic) IBOutlet UILabel *price;
@property(strong, nonatomic) IBOutlet UILabel *type;
@property(strong, nonatomic) IBOutlet UILabel *likecount;
@property(strong, nonatomic) IBOutlet UIImageView *productcoverphoto;

@end
