//
//  TDDatePickerController.h
//
//  Created by Nathan  Reed on 30/09/10.
//  Copyright 2010 Nathan Reed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import	"TDSemiModal.h"

@protocol TDDatePickerDelegate;

@interface TDTwoPicker : TDSemiModalViewController<UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak) id<TDDatePickerDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIPickerView *picker;
@property (nonatomic, retain) NSString* year;
@property (nonatomic, retain) NSString* month;
@property (strong, nonatomic) NSArray *yearArray;
@property (strong, nonatomic) NSArray *monthArray;
@property (nonatomic, retain) NSString* startorenddatepicker;
@property (weak) IBOutlet UIToolbar *toolbar;

-(IBAction)saveDateEdit:(id)sender;
-(IBAction)clearDateEdit:(id)sender;
-(IBAction)cancelDateEdit:(id)sender;

@end

@protocol TDDatePickerDelegate <NSObject>

- (void)tdDatePickerSetDate:(TDTwoPicker*)viewController;
- (void)tdDatePickerClearDate:(TDTwoPicker*)viewController;
- (void)tdDatePickerCancel:(TDTwoPicker*)viewController;
- (void)tdDatePicker:(UIPickerView *)picker didSelectRow:(NSInteger)row inComponent:(NSInteger)component;
@end

