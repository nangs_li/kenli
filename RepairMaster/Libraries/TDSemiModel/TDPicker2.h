//
//  TDDatePickerController.h
//
//  Created by Nathan  Reed on 30/09/10.
//  Copyright 2010 Nathan Reed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import	"TDSemiModal.h"

@protocol TDPicker2Delegate;

@interface TDPicker2 : TDSemiModalViewController<UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak) id<TDPicker2Delegate> delegate;
@property (strong, nonatomic) IBOutlet UIPickerView *picker;
@property (strong, nonatomic) NSArray *pickerData;
@property (weak) IBOutlet UIToolbar *toolbar;
@property (nonatomic, retain) NSString* selectpickerdata;
@property (nonatomic, assign) NSInteger selectindex;
-(IBAction)saveDateEdit:(id)sender;
-(IBAction)clearDateEdit:(id)sender;
-(IBAction)cancelDateEdit:(id)sender;

@end

@protocol TDPicker2Delegate <NSObject>

- (void)picker2SetDate:(TDPicker2*)viewController;
- (void)picker2ClearDate:(TDPicker2*)viewController;
- (void)picker2Cancel:(TDPicker2*)viewController;
- (void)picker2:(UIPickerView *)picker2 didSelectRow:(NSInteger)row inComponent:(NSInteger)component;
@end

