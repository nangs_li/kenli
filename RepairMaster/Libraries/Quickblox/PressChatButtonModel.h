//
//
//  abc
//
//  Created by Li Nang Shing on 23/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import "AFHTTPRequestOperation.h"
#import <Firebase/Firebase.h>
@interface PressChatButtonModel : BaseModel
@property (nonatomic, strong) Firebase* firebase;

@property (nonatomic, strong) NSMutableArray* chat;
+ (PressChatButtonModel *)shared;
/**
 *  callNewsFeedApisuccess
 *
 *  @param success errorStatus=0
 *  @param failure
 errorStatus=1 Not NetWork Connection
 errorStatus=20 Agent have not chat account.
 errorStatus=21 This account have not chat account.
 errorStatus=23 Chat Server Not Response

 */
- (void)pressChatButtonModelByQBID:(NSString *)QBID
                          username:(NSString *)username
                           success:(void (^)(Chatroom *chatRoom))success
                           failure:
                               (void (^)(AFHTTPRequestOperation *operation,
                                         NSError *error, NSString *errorMessage,
                                         RequestErrorStatus errorStatus,
                                         NSString *alert, NSString *ok))failure;

@end
