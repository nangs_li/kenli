//
//  Photos.h
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

//#ifndef abc_Photos_h
//#define abc_Photos_h

//#endif
#import "BaseModel.h"
@interface GetChatMessageToJsqMessageArrayModel : BaseModel
+ (GetChatMessageToJsqMessageArrayModel *)shared;

- (void)whenDBNotMessageAddMessagesFromQBToJSQMessageArrayForDialog:
            (QBChatDialog *)dialog
                                                           chatRoom:
                                                               (Chatroom *)
                                                                   chatRoom;
- (void)addMessagesFromQBToJSQMessageArrayForDialog:(QBChatDialog *)dialog
                                           chatRoom:(Chatroom *)chatRoom;

- (void)getChatMessageFromDBToJsqMessageArrayForDialog:(QBChatDialog *)dialog
                                              chatRoom:(Chatroom *)chatRoom;
//// getEarlierChatMessageFromDBToJsqmessagearray
- (void)
getEarlierChatMessageFromDBToJsqMessageArrayForDialog:(QBChatDialog *)dialog
                                             chatRoom:(Chatroom *)chatRoom;
- (void)syncMessagesForDialogId:(NSString *)dialogId;
@end

//@implementation Photos

//@end