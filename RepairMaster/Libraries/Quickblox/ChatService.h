//
//  ChatService.h
//  sample-chat
//
//  Created by Igor Khomenko on 10/21/13.
//  Copyright (c) 2013 Igor Khomenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "DBQBChatMessage.h"
#import "JSQMessages.h"

#define kNotificationChatDidAccidentallyDisconnect \
  @"kNotificationСhatDidAccidentallyDisconnect"
#define kNotificationChatDidReconnect @"kNotificationChatDidReconnect"
#define kNotificationDialogsUpdated @"kNotificationDialogsUpdated"
#define kNotificationChatRoomReloadCollectionView \
  @"kNotificationChatRoomReloadCollectionView"
#define kNotificationChatRoomConnectToServerSuccess \
  @"kNotificationChatRoomConnectToServerSuccess"

@protocol ChatServiceDelegate<NSObject>

- (BOOL)chatDidReceiveMessage:(QBChatMessage *)message;
- (void)chatDidDeliverMessageWithID:(DBQBChatMessage *)message;
- (void)chatDidReadMessageWithID:(DBQBChatMessage *)message;
- (void)chatDidFailWithStreamError:(NSError *)error;
- (void)chatDidConnect;
- (void)chatDidReceiveUserIsTypingFromUserWithID:(NSUInteger)userID;
- (void)chatDidReceiveUserStopTypingFromUserWithID:(NSUInteger)userID;
- (void)chatRoomCollectionViewReloadOnly;
- (void)chatRoomCollectionViewscrollToBottomYesAnimated;
- (void)chatRoomGetImageFromDBQBChatMessageToMWPhotoBrower;
- (void)getRecipienterFromQBServer;
@end
@interface ChatService : NSObject
@property(weak) id<ChatServiceDelegate> delegate;
+ (instancetype)shared;

#pragma mark TopBarLoginStatusView-----------------------------------------------------------------------------------------------------------------------
@property(readonly) BOOL isConnected;
@property(assign, nonatomic) BOOL connecttoserversuccess;
@property(nonatomic, readonly) QBUUser *currentUser;
@property(nonatomic, strong) NSTimer *presenceTimer;
@property(nonatomic, strong) NSTimer *reconnectTimer;
//// getrecipientertimer
@property(nonatomic, strong) NSTimer *getrecipientertimer;

#pragma mark Login/Logout-------------------------------------------------------------------------------------------------------------------------
- (void)quickBloxToTalLoginOnly;
- (void)loginWithUser:(QBUUser *)user
      completionBlock:(void (^)())completionBlock;
- (void)logout;

#pragma mark JSQMessageArray----------------------------------------------------------------------------------------------------------------------
@property(strong, nonatomic) SDWebImageManager *sdwebImagemanager;
//// all addmessage is jsqmessage
@property(nonatomic, strong) NSMutableDictionary *chatroomJsqMessageArray;

//// JSQMessagesBubbleImage
@property(strong, nonatomic) JSQMessagesBubbleImage *outgoingBubbleImageData;
@property(strong, nonatomic) JSQMessagesBubbleImage *incomingBubbleImageData;
- (void)addMessagesToJSQMessageArray:(NSArray *)messages
                         forDialogId:(NSString *)dialogId;

- (void)addMessagesWhenDBNotMessageToJSQMessageArray:(NSArray *)messages
                                         forDialogId:(NSString *)dialogId;

- (JSQMessage *)
addJSQMessageFromDBQBChatMessageToJSSQMessageArray:(DBQBChatMessage *)message
                                       forDialogId:(NSString *)dialogId;

- (void)addjsqPhotoMessageToJsqMessageArray:(JSQMessage *)jsqmessage
                                forDialogId:(NSString *)dialogId;

- (JSQMessage *)covertDBQBChatMessageToJsqMessage:
    (DBQBChatMessage *)dbhistorymessage;
- (void)whenReceiveMessageAddMessageToJsqmessageArray:
            (QBChatMessage *)chatmessage
                                          forDialogId:(NSString *)dialogId;
//// setUpJSQMessage
- (void)setUpJSQMessage:(JSQMessage *)jsqmessage
      fromQBChatMessage:(DBQBChatMessage *)dbqbchatmessage;
- (void)syncMessagesForDialogId:(NSString *)dialogId;
- (void)reloadChatRoomCollectionViewAfterSecond:(int)second;

#pragma mark unreadlabel
/*
//// unreadlabel method
- (void)addUnreadlabelforDialogId:(NSString *)dialogId
UnreadMessageCount:(int)UnreadMessageCount;

-(BOOL)checkDBhaveUnreadLabelMessageForDialogId:(NSString *)dialogId;

-(void)removeUnreadLabelMessageFromDBmessagesArrayJsqMessageArrayForDialogId:(NSString
*)dialogId;
*/

@end
