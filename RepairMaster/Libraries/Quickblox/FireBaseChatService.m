//
//  ChatService.m
//  sample-chat
//
//  Created by Igor Khomenko on 10/21/13.
//  Copyright (c) 2013 Igor Khomenko. All rights reserved.
//

#import "FireBaseChatService.h"

#import "DBQBChatDialog.h"
#import "DBQBUUser.h"
#import "DBQBChatMessage.h"
#import "Chatroom.h"
#import <SDWebImage/UIImageView+WebCache.h>


#import "ChatDialogModel.h"
typedef void (^CompletionBlock)();

typedef void (^CompletionBlockWithResult)(NSArray *);

@interface FireBaseChatService ()<QBChatDelegate>

@property(copy) CompletionBlock loginCompletionBlock;
@end

@implementation FireBaseChatService {
  BOOL _chatAccidentallyDisconnected;
}
+ (instancetype)shared {
  static id instance_ = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    instance_ = [[self alloc] init];
  });

  return instance_;
}

- (id)init {
  self = [super init];
  if (self) {
    [[QBChat instance] addDelegate:self];
    [QBChat instance].autoReconnectEnabled = YES;
    self.connecttoserversuccess = NO;
//
// [QBChat instance].streamManagementEnabled = YES;
//  [QBChat instance].streamResumptionEnabled = YES;
////init JSQMessage propert
    self.chatroomJsqMessageArray = [NSMutableDictionary dictionary];
    self.sdwebImagemanager = [SDWebImageManager sharedManager];
    [self.sdwebImagemanager.imageDownloader setMaxConcurrentDownloads:2];
    [[SDImageCache sharedImageCache] setShouldDecompressImages:NO];
    [[SDWebImageDownloader sharedDownloader] setShouldDecompressImages:NO];
     
      
      JSQMessagesBubbleImageFactory *bubbleFactory =
        [[JSQMessagesBubbleImageFactory alloc] init];

    self.outgoingBubbleImageData =
        [bubbleFactory outgoingMessagesBubbleImageWithColor:
                           [UIColor jsq_messageBubbleLightGrayColor]];
    self.incomingBubbleImageData =
        [bubbleFactory incomingMessagesBubbleImageWithColor:
                           [UIColor jsq_messageBubbleGreenColor]];
  }
  return self;
}




#pragma mark Login/Logout-------------------------------------------------------------------------------------------------------------------------
#pragma mark quickbloxtotalloginonly
- (void)quickBloxToTalLoginOnly {
   
                [QBRequest
         logInWithUserLogin:
         [NSString
          stringWithFormat:@"%010d", [[LoginRecordModel shared]
                                      .myLoginRecord.eid intValue]]
         password:[NSString stringWithFormat:@"%010d", [[LoginRecordModel shared]
                                                        .myLoginRecord
                                                        .eid intValue]]
         successBlock:^(QBResponse *response, QBUUser *user) {
             DDLogInfo(@"currentUser.ID-->%lu", (unsigned long)user.ID);
             DDLogInfo(@"user.password-->%@", user.password);
             
             QBUUser *currentUser = [QBUUser user];
             
             currentUser.ID = user.ID;
             currentUser.login =
             [NSString stringWithFormat:@"%010d",
              [[LoginRecordModel shared]
               .myLoginRecord
               .eid intValue]];  // your
             // current
             // user's ID
            
             currentUser.password = [NSString
                                     stringWithFormat:@"%010d", [[LoginRecordModel shared]
                                                                 .myLoginRecord.eid intValue]];
             [LoginRecordModel shared].myLoginRecord.QBID = [NSString stringWithFormat:@"%li",  user.ID];;
             
             [[[DBLoginRecord query] fetch] removeAll];
             DBLoginRecord *dbLoginRecord = [DBLoginRecord new];
             dbLoginRecord.Date = [NSDate date];
             
             dbLoginRecord.LoginRecrod = [NSKeyedArchiver
                                          archivedDataWithRootObject:[[LoginRecordModel shared]
                                                                      .myLoginRecord copy]];
             dbLoginRecord.eid = [LoginRecordModel shared].myLoginRecord.eid;
             [dbLoginRecord commit];

             [[ChatService shared] loginWithUser:currentUser
                                 completionBlock:^{
                                     
                                    
                                 }];
             
         }
         errorBlock:^(QBResponse *response) {
             DDLogError(@"[QBRequest logInWithUserLogin %@", response);
             dispatch_time_t popTime =
             dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC);
             dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                 [self quickBloxToTalLoginOnly];
                 
             });
         }];
        
   
}
- (void)loginWithUser:(QBUUser *)user
      completionBlock:(void (^)())completionBlock {
  self.loginCompletionBlock = completionBlock;

  [[QBChat instance] loginWithUser:user];
}
- (BOOL)isConnected {
  return [QBChat instance].isLoggedIn;
}
- (QBUUser *)currentUser {
    return [[QBChat instance] currentUser];
}
- (void)logout {
  [[QBChat instance] logout];

  [ChatService shared].connecttoserversuccess = NO;
}






#pragma mark JSQMessageArray----------------------------------------------------------------------------------------------------------------------
- (NSMutableArray *)jsqMessagsForDialogId:(NSString *)dialogId {
  NSMutableArray *jsqmessages =
      [self.chatroomJsqMessageArray objectForKey:dialogId];

  return jsqmessages;
}
- (void)addMessagesToJSQMessageArray:(NSArray *)messages
                         forDialogId:(NSString *)dialogId {
  NSMutableArray *chatroomjsqemessagearray = [[NSMutableArray alloc] init];

  [self reloadChatRoomCollectionViewAfterSecond:4];
  [self reloadChatRoomCollectionViewAfterSecond:7];
  [self reloadChatRoomCollectionViewAfterSecond:10];

  for (QBChatMessage *message in messages) {
//// check duplicate message
      if ([[[DBQBChatMessage query]
            whereWithFormat:@" MessageID = %@ ", message.ID] count] == 0) {
      DBQBChatMessage *dbhistorymessage = [DBQBChatMessage new];

      dbhistorymessage.MessageID = message.ID;
      dbhistorymessage.DialogID = dialogId;
      dbhistorymessage.QBID = [LoginRecordModel shared].myLoginRecord.QBID;
      dbhistorymessage.eid =
          [[LoginRecordModel shared].myLoginRecord.eid intValue];
      dbhistorymessage.RecipientID = message.recipientID;
      dbhistorymessage.SenderID = message.senderID;
      dbhistorymessage.DateSent = message.dateSent;
      dbhistorymessage.InsertDBDateTime = [NSDate date];
      if ([self IsEmpty:[message.customParameters objectForKey:@"photourl"]]) {
        dbhistorymessage.PhotoType = NO;
      } else {
        dbhistorymessage.PhotoType = YES;
      }
      dbhistorymessage.QBChatHistoryMessage =
          [NSKeyedArchiver archivedDataWithRootObject:message];

      dbhistorymessage.Text = message.text;
      dbhistorymessage.CustomParameters = message.customParameters;
      dbhistorymessage.Attachments = message.attachments;
      dbhistorymessage.NumberofDeliverStatus = 2;

      dbhistorymessage.DidDeliverStatus = YES;
      dbhistorymessage.ErrorStatus = NO;
      dbhistorymessage.DrawStatus = NO;
      dbhistorymessage.ReadStatus = NO;
      dbhistorymessage.Retrycount = 0;
      [dbhistorymessage commit];

      [chatroomjsqemessagearray
          addObject:[self covertDBQBChatMessageToJsqMessage:dbhistorymessage]];
    }
  }

//// add chatroomJsqMessageArray to chatroomJsqMessageArray
  NSMutableArray *chatroomJsqMessageArray =
      [self.chatroomJsqMessageArray objectForKey:dialogId];

  if (chatroomJsqMessageArray != nil) {
    [chatroomJsqMessageArray addObjectsFromArray:chatroomjsqemessagearray];
  } else {
    [self.chatroomJsqMessageArray setObject:chatroomjsqemessagearray
                                     forKey:dialogId];
  }
}
- (void)addMessagesWhenDBNotMessageToJSQMessageArray:(NSArray *)messages
                                       forDialogId:(NSString *)dialogId {
  NSMutableArray *chatroomjsqemessagearray = [[NSMutableArray alloc] init];
  NSMutableArray *chathistortmessagearray = [[NSMutableArray alloc] init];

  [QBRequest countOfMessagesForDialogID:dialogId
      extendedRequest:nil
      successBlock:^(QBResponse *response, NSUInteger count) {
        [self reloadChatRoomCollectionViewAfterSecond:4];
        [self reloadChatRoomCollectionViewAfterSecond:7];
        BOOL ReadStatus = YES;
        if (count <= 1) {
          ReadStatus = NO;
        }
        for (QBChatMessage *message in messages) {
          DBResultSet *r = [[[DBQBChatMessage query]
              whereWithFormat:@" MessageID = %@ ", message.ID] fetch];
          if (r.count == 0) {
            DBQBChatMessage *dbhistorymessage = [DBQBChatMessage new];

            dbhistorymessage.MessageID = message.ID;
            dbhistorymessage.DialogID = dialogId;
            dbhistorymessage.QBID =
                [LoginRecordModel shared].myLoginRecord.QBID;
            dbhistorymessage.eid =
                [[LoginRecordModel shared].myLoginRecord.eid intValue];
            dbhistorymessage.RecipientID = message.recipientID;
            dbhistorymessage.SenderID = message.senderID;
            dbhistorymessage.DateSent = message.dateSent;
            dbhistorymessage.InsertDBDateTime = [NSDate date];

            dbhistorymessage.QBChatHistoryMessage =
                [NSKeyedArchiver archivedDataWithRootObject:message];
            if ([self IsEmpty:[message.customParameters
                                  objectForKey:@"photourl"]]) {
              dbhistorymessage.PhotoType = NO;
            } else {
              dbhistorymessage.PhotoType = YES;
            }
            dbhistorymessage.Text = message.text;
            dbhistorymessage.CustomParameters = message.customParameters;
            dbhistorymessage.Attachments = message.attachments;
            dbhistorymessage.NumberofDeliverStatus = 2;
            dbhistorymessage.DidDeliverStatus = YES;

            dbhistorymessage.ErrorStatus = NO;
            dbhistorymessage.DrawStatus = YES;
            dbhistorymessage.ReadStatus = ReadStatus;
            dbhistorymessage.Retrycount = 0;
            [dbhistorymessage commit];

            [chathistortmessagearray addObject:dbhistorymessage];
            [chatroomjsqemessagearray
                addObject:
                    [self covertDBQBChatMessageToJsqMessage:dbhistorymessage]];
          }
        }

//// add chatroomJsqMessageArray to self.chatroomJsqMessageArray
        NSMutableArray *chatroomJsqMessageArray =
            [self.chatroomJsqMessageArray objectForKey:dialogId];

        if (chatroomJsqMessageArray != nil) {
          [chatroomJsqMessageArray
              addObjectsFromArray:chatroomjsqemessagearray];
        } else {
          [self.chatroomJsqMessageArray setObject:chatroomjsqemessagearray
                                           forKey:dialogId];
        }

        
          [self reloadChatRoomCollectionViewAfterSecond:0];
      }
      errorBlock:^(QBResponse *response){
           DDLogError(@"QBResponse %@",response);
      }];
}
- (JSQMessage *)covertDBQBChatMessageToJsqMessage:
    (DBQBChatMessage *)dbhistorymessage {
  JSQMessage *jsqmessage;
//// check is photo type message
  if (![self IsEmpty:[dbhistorymessage.CustomParameters
                         objectForKey:@"photourl"]]) {
    JSQPhotoMediaItem *photoItem =
        [[JSQPhotoMediaItem alloc] initWithImage:nil];
    if ([[LoginRecordModel shared].myLoginRecord.QBID integerValue] ==
        dbhistorymessage.SenderID) {
      photoItem.appliesMediaViewMaskAsOutgoing = YES;
    } else {
      photoItem.appliesMediaViewMaskAsOutgoing = NO;
    }
    NSURL *url = [NSURL URLWithString:[dbhistorymessage.CustomParameters
                                          objectForKey:@"photourl"]];

    jsqmessage = [[JSQMessage alloc]
         initWithSenderId:[NSString stringWithFormat:@"%@", @(dbhistorymessage
                                                                  .SenderID)]
        senderDisplayName:@"None"
                     date:dbhistorymessage.DateSent
                    media:photoItem];
    [self.sdwebImagemanager downloadImageWithURL:url
        options:0
        progress:^(NSInteger receivedSize, NSInteger expectedSize) {
          // progression tracking code
        }
        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,
                    BOOL finished, NSURL *imageURL) {
          if (image) {
            photoItem.image = image;

           
                UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
             
          }
        }];

  } else {
//// check is text type message
    NSString *Text = dbhistorymessage.Text;
    if ([self IsEmpty:dbhistorymessage.Text]) {
      Text = @"";
    }
    jsqmessage = [[JSQMessage alloc]
         initWithSenderId:[NSString stringWithFormat:@"%@", @(dbhistorymessage
                                                                  .SenderID)]
        senderDisplayName:@"None"
                     date:dbhistorymessage.DateSent
                     text:Text];
  }

  [[ChatService shared] setUpJSQMessage:jsqmessage
                      fromQBChatMessage:dbhistorymessage];
  return jsqmessage;
}
- (JSQMessage *)
addJSQMessageFromDBQBChatMessageToJSSQMessageArray:(DBQBChatMessage *)message
                                       forDialogId:(NSString *)dialogId {
//// add jsqmessage to chatroomJsqMessageArray

  JSQMessage *jsqmessage = [[JSQMessage alloc]
       initWithSenderId:[NSString stringWithFormat:@"%@", @(message.SenderID)]
      senderDisplayName:@"None"
                   date:message.DateSent
                   text:message.Text];
  [self setUpJSQMessage:jsqmessage fromQBChatMessage:message];
  NSMutableArray *chatroomJsqMessageArray =
      [self.chatroomJsqMessageArray objectForKey:dialogId];

  if (chatroomJsqMessageArray != nil) {
    [chatroomJsqMessageArray addObject:jsqmessage];
  } else {
    NSMutableArray *messages = [NSMutableArray array];
    [messages addObject:jsqmessage];
    [self.chatroomJsqMessageArray setObject:messages forKey:dialogId];
  }

  return jsqmessage;
}
- (void)whenReceiveMessageAddMessageToJsqmessageArray:
            (QBChatMessage *)chatmessage forDialogId:(NSString *)dialogId {
  [self reloadChatRoomCollectionViewAfterSecond:5];
  [self reloadChatRoomCollectionViewAfterSecond:9];
  NSMutableArray *chatroomjsqemessagearray = [[NSMutableArray alloc] init];
  DBResultSet *r = [[[DBQBChatMessage query]
      whereWithFormat:@" MessageID = %@ ", chatmessage.ID] fetch];
  if (r.count == 0) {
    DBQBChatMessage *dbhistorymessage = [DBQBChatMessage new];

    dbhistorymessage.MessageID = chatmessage.ID;
    dbhistorymessage.DialogID = chatmessage.dialogID;
    dbhistorymessage.QBID = [LoginRecordModel shared].myLoginRecord.QBID;
    dbhistorymessage.eid =
        [[LoginRecordModel shared].myLoginRecord.eid intValue];
    dbhistorymessage.RecipientID = chatmessage.recipientID;
    dbhistorymessage.SenderID = chatmessage.senderID;
    dbhistorymessage.DateSent = chatmessage.dateSent;
    dbhistorymessage.InsertDBDateTime = [NSDate date];

    dbhistorymessage.QBChatHistoryMessage =
        [NSKeyedArchiver archivedDataWithRootObject:chatmessage];
    if ([self
            IsEmpty:[chatmessage.customParameters objectForKey:@"photourl"]]) {
      dbhistorymessage.PhotoType = NO;
    } else {
      dbhistorymessage.PhotoType = YES;
    }
    dbhistorymessage.Text = chatmessage.text;
    dbhistorymessage.CustomParameters = chatmessage.customParameters;
    dbhistorymessage.Attachments = chatmessage.attachments;
    dbhistorymessage.NumberofDeliverStatus = 2;

    dbhistorymessage.DidDeliverStatus = YES;
    dbhistorymessage.ErrorStatus = NO;
    dbhistorymessage.DrawStatus = NO;
    dbhistorymessage.ReadStatus = NO;
    dbhistorymessage.Retrycount = 0;
    [dbhistorymessage commit];

//// add chatroomJsqMessageArray to chatroomJsqMessageArray
    NSMutableArray *chatroomJsqMessageArray =
        [self.chatroomJsqMessageArray objectForKey:dialogId];

    if (chatroomJsqMessageArray != nil) {
      [chatroomJsqMessageArray
          addObject:[self covertDBQBChatMessageToJsqMessage:dbhistorymessage]];
    } else {
      [self.chatroomJsqMessageArray setObject:chatroomjsqemessagearray
                                       forKey:dialogId];
    }
  }
}

- (void)addjsqPhotoMessageToJsqMessageArray:(JSQMessage *)jsqmessage
                                forDialogId:(NSString *)dialogId {
  NSMutableArray *chatroomJsqMessageArray =
      [self.chatroomJsqMessageArray objectForKey:dialogId];

  if (chatroomJsqMessageArray != nil) {
    [chatroomJsqMessageArray addObject:jsqmessage];
  } else {
    NSMutableArray *messages = [NSMutableArray array];
    [messages addObject:jsqmessage];
    [self.chatroomJsqMessageArray setObject:messages forKey:dialogId];
  }
    
    
}
- (void)chatDidNotSendMessage:(QBChatMessage *)message error:(NSError *)error {
  DBResultSet *r = [[[DBQBChatMessage query]
      whereWithFormat:@" MessageID = %@ ", message.ID] fetch];

  for (DBQBChatMessage *qbchatmessage in r) {
    qbchatmessage.ErrorStatus = YES;
    [qbchatmessage commit];
  }
}

- (void)setUpJSQMessage:(JSQMessage *)jsqmessage
      fromQBChatMessage:(DBQBChatMessage *)dbqbchatmessage {
    jsqmessage.MessageID = dbqbchatmessage.MessageID;
    jsqmessage.DialogID = dbqbchatmessage.DialogID;
    jsqmessage.QBID = dbqbchatmessage.QBID;
    jsqmessage.eid = dbqbchatmessage.eid;
    jsqmessage.RecipientID = dbqbchatmessage.RecipientID;
    jsqmessage.SenderID = dbqbchatmessage.SenderID;
    jsqmessage.DateSent = dbqbchatmessage.DateSent;
    jsqmessage.InsertDBDateTime = dbqbchatmessage.InsertDBDateTime;
    jsqmessage.PhotoType = dbqbchatmessage.PhotoType;
    jsqmessage.QBChatHistoryMessage = dbqbchatmessage.QBChatHistoryMessage;
    
    jsqmessage.Text = dbqbchatmessage.Text;
    jsqmessage.CustomParameters = dbqbchatmessage.CustomParameters;
    jsqmessage.Attachments = dbqbchatmessage.Attachments;
    jsqmessage.NumberofDeliverStatus = dbqbchatmessage.NumberofDeliverStatus;
    jsqmessage.DidDeliverStatus = dbqbchatmessage.DidDeliverStatus;
    jsqmessage.ErrorStatus = dbqbchatmessage.ErrorStatus;
    jsqmessage.DrawStatus = dbqbchatmessage.DrawStatus;
    jsqmessage.ReadStatus = dbqbchatmessage.ReadStatus;
    jsqmessage.Retrycount = dbqbchatmessage.Retrycount;
}
////check empty
- (BOOL)IsEmpty:(id)thing {
    return thing == nil || ([thing respondsToSelector:@selector(length)] &&
                            [(NSData *)thing length] == 0) ||
    ([thing respondsToSelector:@selector(count)] &&
     [(NSArray *)thing count] == 0);
}

- (void)reloadChatRoomCollectionViewAfterSecond:(int)second {
    dispatch_time_t popTime =
    dispatch_time(DISPATCH_TIME_NOW, second * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        if ([self.delegate
             respondsToSelector:@selector(chatRoomCollectionViewReloadOnly)]) {
            [self.delegate chatRoomCollectionViewReloadOnly];
        }
    });
}





#pragma mark  QBChatDelegate-----------------------------------------------------------------------------------------------------------------------
- (void)chatDidLogin {
  [[NSNotificationCenter defaultCenter]
      postNotificationName:kNotificationChatRoomConnectToServerSuccess
                    object:nil];
  [self.reconnectTimer invalidate];

  self.reconnectTimer =
      [NSTimer scheduledTimerWithTimeInterval:3
                                       target:self
                                     selector:@selector(tryConnectServer)
                                     userInfo:nil
                                      repeats:YES];

  if (_chatAccidentallyDisconnected || [ChatDialogModel shared].dialogs.count > 0) {
    _chatAccidentallyDisconnected = NO;
  }

  if (self.loginCompletionBlock != nil) {
    self.loginCompletionBlock();
    self.loginCompletionBlock = nil;
  }

  [ChatService shared].connecttoserversuccess = YES;

  if ([self.delegate respondsToSelector:@selector(chatDidLogin)]) {
    [self.delegate chatDidLogin];
  }

  [[ChatDialogModel shared] requestDialogsWithCompletionBlock:^{
    [[NSNotificationCenter defaultCenter]
        postNotificationName:kNotificationDialogsUpdated
                      object:nil];
    [[ChatDialogModel shared] updateChatBadgeNumber];

  }];
}
- (void)chatDidFailWithError:(NSInteger)code {
  // relogin here

  [[ChatService shared] loginWithUser:[ChatService shared].currentUser
                      completionBlock:^{

                      }];
}
- (void)chatDidFailWithStreamError:(NSError *)error {
  [ChatService shared].connecttoserversuccess = NO;

  if ([self.delegate
          respondsToSelector:@selector(chatDidFailWithStreamError:)]) {
    [self.delegate chatDidFailWithStreamError:error];
  }
}
- (void)chatDidReadMessageWithID:(NSString *)messageID dialogID:(NSString *)dialogID readerID:(NSUInteger)readerID {
  DBResultSet *r =
      [[[[DBQBChatMessage query] whereWithFormat:@" MessageID = %@", messageID]
          limit:1] fetch];
  DBQBChatMessage *chatDidReadMessage;
  for (DBQBChatMessage *chatDidReadMessageFromDB in r) {
    chatDidReadMessage = chatDidReadMessageFromDB;

    DBResultSet *r2 = [[[DBQBChatMessage query]
        whereWithFormat:@" ReadStatus =%@ AND DialogID=%@ ", @"0",
                        chatDidReadMessageFromDB.DialogID] fetch];

    for (DBQBChatMessage *QBChatMessageFromDB in r2) {
      if ([QBChatMessageFromDB.DateSent
              compare:chatDidReadMessageFromDB.DateSent] ==
          NSOrderedDescending) {
        DDLogInfo(@"date1 is later than date2");

      } else if ([QBChatMessageFromDB.DateSent
                     compare:chatDidReadMessageFromDB.DateSent] ==
                 NSOrderedAscending) {
        DDLogInfo(@"date1 is earlier than date2");
        QBChatMessageFromDB.ReadStatus = YES;
        [QBChatMessageFromDB commit];
      } else {
        DDLogInfo(@"date1 are the same");

        QBChatMessageFromDB.ReadStatus = YES;
        [QBChatMessageFromDB commit];
      }
    }
  }

  if ([self.delegate respondsToSelector:@selector(chatDidReadMessageWithID:)]) {
    [self.delegate chatDidReadMessageWithID:chatDidReadMessage];
  }
}
- (void)chatDidDeliverMessageWithID:(NSString *)messageID dialogID:(NSString *)dialogID toUserID:(NSUInteger)userId{
  DBResultSet *r = [[[DBQBChatMessage query]
      whereWithFormat:@" MessageID = %@", messageID] fetch];
  DBQBChatMessage *chatDidDeliverMessage;
  for (DBQBChatMessage *chatDidReadMessageFromDB in r) {
    chatDidDeliverMessage = chatDidReadMessageFromDB;

    DBResultSet *r2 = [[[DBQBChatMessage query]
        whereWithFormat:@" DidDeliverStatus =%@ AND DialogID=%@ ", @"0",
                        chatDidReadMessageFromDB.DialogID] fetch];

    for (DBQBChatMessage *QBChatMessageFromDB in r2) {
      if ([QBChatMessageFromDB.MessageID isEqualToString:messageID]) {
        QBChatMessageFromDB.DidDeliverStatus = YES;
        [QBChatMessageFromDB commit];
      }

      if ([QBChatMessageFromDB.DateSent
              compare:chatDidReadMessageFromDB.DateSent] ==
          NSOrderedDescending) {
        DDLogInfo(@"date1 is later than date2");

      } else if ([QBChatMessageFromDB.DateSent
                     compare:chatDidReadMessageFromDB.DateSent] ==
                 NSOrderedAscending) {
        DDLogInfo(@"date1 is earlier than date2");
        QBChatMessageFromDB.DidDeliverStatus = YES;
        [QBChatMessageFromDB commit];
      } else {
        DDLogInfo(@"dates are the same");

        QBChatMessageFromDB.DidDeliverStatus = YES;
        [QBChatMessageFromDB commit];
      }
    }
  }

  if ([self.delegate
          respondsToSelector:@selector(chatDidDeliverMessageWithID:)]) {
    [self.delegate chatDidDeliverMessageWithID:chatDidDeliverMessage];
  }
}
- (void)chatDidReceiveMessage:(QBChatMessage *)message {
  NSString *dialogId = message.dialogID;

// notify observers
//// check duplicate message
  DBResultSet *r = [[[DBQBChatMessage query]
      whereWithFormat:@" MessageID = %@ ", message.ID] fetch];
  if (r.count != 0) {
    return;
  }

  BOOL processed = NO;
  if ([self.delegate respondsToSelector:@selector(chatDidReceiveMessage:)]) {
    processed = [self.delegate chatDidReceiveMessage:message];
  }

  if (!processed) {
    /*
      if ([AppDelegate getAppDelegate].applicationState ==
        UIApplicationStateInactive) {
      [[NSNotificationCenter defaultCenter]
          postNotificationName:@"openChatroomfromDailogId"
                        object:nil
                      userInfo:@{
                        @"dialog_id" : dialogId
                      }];
      [AppDelegate getAppDelegate].applicationState = UIApplicationStateActive;
    } else {
        */
////IS Not IN ChatRoom save message into database     
    DBResultSet *r = [[[DBQBChatMessage query]
        whereWithFormat:@" MessageID = %@ ", message.ID] fetch];
    if (r.count == 0) {
      DBQBChatMessage *dbhistorymessage = [DBQBChatMessage new];

      dbhistorymessage.MessageID = message.ID;
      dbhistorymessage.DialogID = dialogId;
      dbhistorymessage.QBID = [LoginRecordModel shared].myLoginRecord.QBID;
      dbhistorymessage.eid =
          [[LoginRecordModel shared].myLoginRecord.eid intValue];
      dbhistorymessage.RecipientID = message.recipientID;
      dbhistorymessage.SenderID = message.senderID;
      dbhistorymessage.DateSent = message.dateSent;
      dbhistorymessage.InsertDBDateTime = [NSDate date];
      if ([self IsEmpty:[message.customParameters objectForKey:@"photourl"]]) {
        dbhistorymessage.PhotoType = NO;
      } else {
        dbhistorymessage.PhotoType = YES;
      }
      dbhistorymessage.QBChatHistoryMessage =
          [NSKeyedArchiver archivedDataWithRootObject:message];

      dbhistorymessage.Text = message.text;
      dbhistorymessage.CustomParameters = message.customParameters;
      dbhistorymessage.Attachments = message.attachments;
      dbhistorymessage.NumberofDeliverStatus = 2;

      dbhistorymessage.DidDeliverStatus = YES;
      dbhistorymessage.ErrorStatus = NO;
      dbhistorymessage.DrawStatus = NO;
      dbhistorymessage.ReadStatus = NO;
      dbhistorymessage.Retrycount = 0;
      [dbhistorymessage commit];
////IS Photo Message Download
      if (![self IsEmpty:[message.customParameters objectForKey:@"photourl"]]) {
        NSURL *url = [NSURL
            URLWithString:[message.customParameters objectForKey:@"photourl"]];

        [self.sdwebImagemanager downloadImageWithURL:url
            options:0
            progress:^(NSInteger receivedSize, NSInteger expectedSize) {
              // progression tracking code
            }
            completed:^(UIImage *image, NSError *error,
                        SDImageCacheType cacheType, BOOL finished,
                        NSURL *imageURL) {
              if (image) {
              
                    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
               
              }
            }];
      }
    }

    [[ChatDialogModel shared] requestDialogUpdateWithId:dialogId
                    completionBlock:^{

                      [[NSNotificationCenter defaultCenter]
                          postNotificationName:kNotificationDialogsUpdated
                                        object:nil];
                      [[ChatDialogModel shared] updateChatBadgeNumber];

                    }];

    [[TWMessageBarManager sharedInstance] hideAllAnimated:YES];
    [[TWMessageBarManager sharedInstance]
        showMessageWithTitle:@"New message"
                 description:message.text
                        type:TWMessageBarMessageTypeInfo
                    callback:^{
                      [[NSNotificationCenter defaultCenter]
                          postNotificationName:@"openChatroomfromDailogId"
                                        object:nil
                                      userInfo:@{
                                        @"dialog_id" : dialogId
                                      }];

                    }];
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);

    // 1003  = ReceivedMessage.caf
    AudioServicesPlaySystemSound(1003);
    // }
  }
}





#pragma mark TopBarLoginStatusView---------------------------------------------------------------------------------------------------------------------
- (void)chatDidReceiveUserIsTypingFromUserWithID:(NSUInteger)userID {
  if ([self.delegate
          respondsToSelector:@selector(
                                 chatDidReceiveUserIsTypingFromUserWithID:)]) {
    [self.delegate chatDidReceiveUserIsTypingFromUserWithID:userID];
  }
}
- (void)chatDidReceiveUserStopTypingFromUserWithID:(NSUInteger)userID {
  if ([self.delegate
          respondsToSelector:
              @selector(chatDidReceiveUserStopTypingFromUserWithID:)]) {
    [self.delegate chatDidReceiveUserStopTypingFromUserWithID:userID];
  }
}
- (void)tryConnectServer {
  DDLogInfo(@"tryConnectServer");
  // Start sending presences

  if ([[QBChat instance] sendPresence]) {
   // DDLogInfo(@"sendPresencesuccess");
  } else {
 //   DDLogInfo(@"sendPresencefail");
  }

  if ([self isConnected] && _chatAccidentallyDisconnected) {
    [[QBChat instance] addDelegate:self];
    [QBChat instance].autoReconnectEnabled = YES;

    //  [QBChat instance].streamManagementEnabled = YES;
    //  [QBChat instance].streamResumptionEnabled = YES;

    _chatAccidentallyDisconnected = NO;
    [[QBChat instance] forceReconnect];

    [self chatDidLogin];
  }

  [self getRecipienterFromQBServer];
}
- (void)chatDidAccidentallyDisconnect {
  //  [[QBChat instance] removeAllDelegates];

  [ChatService shared].connecttoserversuccess = NO;
  _chatAccidentallyDisconnected = YES;

  DDLogInfo(@"chatDidAccidentallyDisconnect");

  [[NSNotificationCenter defaultCenter]
      postNotificationName:kNotificationChatDidAccidentallyDisconnect
                    object:nil];
  /*
   [[TWMessageBarManager sharedInstance]
   showMessageWithTitle:@"Alert"
   description:@"You have lost the Internet connection"
   type:TWMessageBarMessageTypeInfo];
   */
}
- (void)getRecipienterFromQBServer {
  DDLogInfo(@"getRecipienterFromQBServer");
  if ([self.delegate
          respondsToSelector:@selector(getRecipienterFromQBServer)]) {
    [self.delegate getRecipienterFromQBServer];
  }
}
- (void)sendPresence {
  if ([[QBChat instance] sendPresence]) {
    DDLogInfo(@"sendPresence-->");
  }
}
@end
