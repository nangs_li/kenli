//
//  Photos.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Chatroom.h"
#import "DBQBChatMessage.h"
#import "GetChatMessageToJsqMessageArrayModel.h"
@implementation GetChatMessageToJsqMessageArrayModel

static GetChatMessageToJsqMessageArrayModel
    *sharedGetChatMessageToJsqMessageArrayModel;

+ (GetChatMessageToJsqMessageArrayModel *)shared {
  @synchronized(self) {
    if (!sharedGetChatMessageToJsqMessageArrayModel) {
      sharedGetChatMessageToJsqMessageArrayModel =
          [[GetChatMessageToJsqMessageArrayModel alloc] init];
      [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    }
    return sharedGetChatMessageToJsqMessageArrayModel;
  }
}
#pragma mark getMessageFromQuickBloxToChatServiceJSQMessageArrayAndDB-----------------------------------------------------------------------------------
//// DBhaveNotMessageSyncMessages
- (void)whenDBNotMessageAddMessagesFromQBToJSQMessageArrayForDialog:
            (QBChatDialog *)dialog
                                                           chatRoom:
                                                               (Chatroom *)
                                                                   chatRoom {
  NSMutableDictionary *extendedRequest = [[NSMutableDictionary alloc] init];
  extendedRequest[@"sort_desc"] = @"_id";

  QBResponsePage *page = [QBResponsePage responsePageWithLimit:100 skip:0];
  DDLogInfo(@"start get message from dialogid throug quickblox");
  [QBRequest messagesWithDialogID:dialog.ID
      extendedRequest:extendedRequest
      forPage:page
      successBlock:^(QBResponse *response, NSArray *messages,
                     QBResponsePage *page) {
        DDLogInfo(
            @"start get message from dialogid throug quickblox : sucessBlock "
            @"excuting");
        if (messages.count > 0) {
          NSArray *messagesarray =
              [[messages reverseObjectEnumerator] allObjects];
          [[ChatService shared]
              addMessagesWhenDBNotMessageToJSQMessageArray:messagesarray
                                               forDialogId:dialog.ID];
        }
      }
      errorBlock:^(QBResponse *response) {
        DDLogError(@"QBResponse %@", response);
        if (chatRoom.checkHaveNotCountOfDialogsWithOccupants_IdsAllInDialogs) {
          [MBProgressHUD hideHUDForView:chatRoom.view animated:YES];
        }

      }];
  DDLogInfo(@"start get message from dialogid throug quickblox : exit ");
}
//// syncMessages
- (void)addMessagesFromQBToJSQMessageArrayForDialog:(QBChatDialog *)dialog
                                           chatRoom:(Chatroom *)chatRoom {
  NSMutableDictionary *extendedRequest = [[NSMutableDictionary alloc] init];
  DBResultSet *r = [[[[[DBQBChatMessage query]
      whereWithFormat:@" DialogID = %@ AND RecipientID =%@", dialog.ID,
                      [LoginRecordModel shared].myLoginRecord.QBID]
      orderByDescending:@"DateSent"] limit:1] fetch];

  for (DBQBChatMessage *dbqbchatmessage in r) {
    extendedRequest[@"date_sent[gte]"] =
        @([dbqbchatmessage.DateSent timeIntervalSince1970] + 1);
  }
  extendedRequest[@"sort_asc"] = @"_id";

  QBResponsePage *page = [QBResponsePage responsePageWithLimit:100 skip:0];
  DDLogInfo(@"start get message from dialogid throug quickblox");
  [QBRequest messagesWithDialogID:dialog.ID
      extendedRequest:extendedRequest
      forPage:page
      successBlock:^(QBResponse *response, NSArray *messages,
                     QBResponsePage *page) {
        DDLogInfo(
            @"start get message from dialogid throug quickblox : sucessBlock "
            @"excuting");

        if (messages.count > 0) {
          QBChatMessage *firstMessageFromQBMessageArray = [messages lastObject];

          if ([[[DBQBChatMessage query]
                  whereWithFormat:@" MessageID = %@",
                                  firstMessageFromQBMessageArray.ID] count] ==
              0) {
            [[ChatService shared] addMessagesToJSQMessageArray:messages
                                                   forDialogId:dialog.ID];
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
            AudioServicesPlaySystemSound(1003);
            [chatRoom chatRoomCollectionViewReloadOnly];
            [chatRoom readAllReceivceMessage:5];
            [chatRoom chatRoomCollectionViewscrollToBottomYesAnimated];
            [chatRoom getImageFromDBQBChatMessageToMWPhotoBrower];
          }
        }

        [MBProgressHUD hideHUDForView:chatRoom.view animated:YES];

      }
      errorBlock:^(QBResponse *response) {
        DDLogError(@"QBResponse %@", response);
        if (chatRoom.checkHaveNotCountOfDialogsWithOccupants_IdsAllInDialogs) {
          [MBProgressHUD hideHUDForView:chatRoom.view animated:YES];
        }

      }];
  DDLogInfo(@"start get message from dialogid throug quickblox : exit ");
}
#pragma mark getMessageFromDataBaseToChatServiceJSQMessageArray-----------------------------------------------------------------------------------
//// getChatMessageFromDBToJsqmessagearray
- (void)getChatMessageFromDBToJsqMessageArrayForDialog:(QBChatDialog *)dialog
                                              chatRoom:(Chatroom *)chatRoom {
  if (chatRoom.checkHaveNotCountOfDialogsWithOccupants_IdsAllInDialogs) {
    return;
  }

  DBResultSet *r =
      [[[[[DBQBChatMessage query] whereWithFormat:@" DialogID = %@", dialog.ID]
          orderByDescending:@"Id"] limit:20]

          fetch];
  NSArray *r2 = [[r reverseObjectEnumerator] allObjects];

  NSMutableArray *jsqmessagearray = [[NSMutableArray alloc] init];
  NSMutableArray *messagearray = [[NSMutableArray alloc] init];
  //// getChatMessageFromDBToJsqmessagearray

  SDWebImageManager *manager = [SDWebImageManager sharedManager];
  [manager.imageDownloader setMaxConcurrentDownloads:2];

  [[SDImageCache sharedImageCache] setShouldDecompressImages:NO];
  [[SDWebImageDownloader sharedDownloader] setShouldDecompressImages:NO];
  for (DBQBChatMessage *p in r2) {
    [messagearray addObject:p];
    JSQMessage *jsqmessage;
    //// Is  Photo Message
    if (![self IsEmpty:[p.CustomParameters objectForKey:@"photourl"]]) {
      NSURL *url =
          [NSURL URLWithString:[p.CustomParameters objectForKey:@"photourl"]];

      JSQPhotoMediaItem *photoItem;
      //// Photo Message  Photo Exists
      if ([manager diskImageExistsForURL:url]) {
        photoItem = [[JSQPhotoMediaItem alloc] initWithImage:nil];

        photoItem = [[JSQPhotoMediaItem alloc]
            initWithImage:
                [manager.imageCache
                    imageFromDiskCacheForKey:[p.CustomParameters
                                                 objectForKey:@"photourl"]]];

      } else {
        //// Photo Message  Photo Not Exists Download start
        photoItem = [[JSQPhotoMediaItem alloc] initWithImage:nil];

        [manager downloadImageWithURL:url
            options:0
            progress:^(NSInteger receivedSize, NSInteger expectedSize) {
              // progression tracking code
            }
            completed:^(UIImage *image, NSError *error,
                        SDImageCacheType cacheType, BOOL finished,
                        NSURL *imageURL) {
              if (image) {
                photoItem.image = image;

                UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
              }
            }];
      }

      if ([[LoginRecordModel shared].myLoginRecord.QBID integerValue] ==
          p.SenderID) {
        photoItem.appliesMediaViewMaskAsOutgoing = YES;
      } else {
        photoItem.appliesMediaViewMaskAsOutgoing = NO;
      }
      jsqmessage = [[JSQMessage alloc]
           initWithSenderId:[NSString stringWithFormat:@"%@", @(p.SenderID)]
          senderDisplayName:@"None"
                       date:p.DateSent
                      media:photoItem];
      [[ChatService shared] setUpJSQMessage:jsqmessage fromQBChatMessage:p];
    } else {
      //// Is   Message      Not Photo Message
      NSString *text = p.Text;
      if ([self IsEmpty:p.Text]) {
        text = @"";
      }

      jsqmessage = [[JSQMessage alloc]
           initWithSenderId:[NSString stringWithFormat:@"%@", @(p.SenderID)]
          senderDisplayName:@"None"
                       date:p.DateSent
                       text:text];
      [[ChatService shared] setUpJSQMessage:jsqmessage fromQBChatMessage:p];
    }
    [jsqmessagearray addObject:jsqmessage];
  }
  if (![self
          IsEmpty:[[ChatService shared]
                          .chatroomJsqMessageArray objectForKey:dialog.ID]]) {
    [[ChatService shared].chatroomJsqMessageArray removeObjectForKey:dialog.ID];
  }

  [[ChatService shared]
          .chatroomJsqMessageArray setObject:jsqmessagearray
                                      forKey:dialog.ID];
  [chatRoom chatRoomCollectionViewReloadOnly];
  [chatRoom chatRoomCollectionViewscrollToBottomYesAnimated];

  dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC);
  dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {

    [chatRoom chatRoomCollectionViewscrollToBottomYesAnimated];
  });
}
//// getEarlierChatMessageFromDBToJsqmessagearray
- (void)
getEarlierChatMessageFromDBToJsqMessageArrayForDialog:(QBChatDialog *)dialog
                                             chatRoom:(Chatroom *)chatRoom {
  if (chatRoom.checkHaveNotCountOfDialogsWithOccupants_IdsAllInDialogs) {
    return;
  }

  if ([self IsEmpty:[[ChatService shared]
                            .chatroomJsqMessageArray objectForKey:dialog.ID]]) {
    return;
  }
  NSMutableArray *jsqmessagsForDialogId =
      [[ChatService shared].chatroomJsqMessageArray objectForKey:dialog.ID];
  DBResultSet *r =
      [[[[[[DBQBChatMessage query] whereWithFormat:@" DialogID = %@", dialog.ID]
          orderByDescending:@"Id"] offset:(int)jsqmessagsForDialogId.count]
          limit:20]

          fetch];
  NSArray *r2 = [[r reverseObjectEnumerator] allObjects];
  NSMutableArray *jsqmessagearray = [[NSMutableArray alloc] init];
  //// getEarlierChatMessageFromDBToJsqmessagearray
  if (r2.count == 0) {
    [[[UIAlertView alloc] initWithTitle:@"Alert"
                                message:@"Not Earlier Message"
                               delegate:self
                      cancelButtonTitle:@"OK!"
                      otherButtonTitles:nil] show];

    return;
  }
  SDWebImageManager *manager = [SDWebImageManager sharedManager];
  [manager.imageDownloader setMaxConcurrentDownloads:2];

  [[SDImageCache sharedImageCache] setShouldDecompressImages:NO];
  [[SDWebImageDownloader sharedDownloader] setShouldDecompressImages:NO];

  for (DBQBChatMessage *p in r2) {
    JSQMessage *jsqmessage;
    //// Is photomessage  add to jsqmessagearray
    if (![self IsEmpty:[p.CustomParameters objectForKey:@"photourl"]]) {
      NSURL *url =
          [NSURL URLWithString:[p.CustomParameters objectForKey:@"photourl"]];

      JSQPhotoMediaItem *photoItem;

      if ([manager diskImageExistsForURL:url]) {
        photoItem = [[JSQPhotoMediaItem alloc]
            initWithImage:
                [manager.imageCache
                    imageFromDiskCacheForKey:[p.CustomParameters
                                                 objectForKey:@"photourl"]]];

      } else {
        photoItem = [[JSQPhotoMediaItem alloc] initWithImage:nil];

        [manager downloadImageWithURL:url
            options:0
            progress:^(NSInteger receivedSize, NSInteger expectedSize) {
              // progression tracking code
            }
            completed:^(UIImage *image, NSError *error,
                        SDImageCacheType cacheType, BOOL finished,
                        NSURL *imageURL) {
              if (image) {
                photoItem.image = image;

                UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
              }
            }];
      }

      if ([[LoginRecordModel shared].myLoginRecord.QBID integerValue] ==
          p.SenderID) {
        photoItem.appliesMediaViewMaskAsOutgoing = YES;
      } else {
        photoItem.appliesMediaViewMaskAsOutgoing = NO;
      }
      DDLogInfo(@"[p.CustomParameters objectForKey:@photourl]]-->%@",
                [p.CustomParameters objectForKey:@"photourl"]);

      jsqmessage = [[JSQMessage alloc]
           initWithSenderId:[NSString stringWithFormat:@"%@", @(p.SenderID)]
          senderDisplayName:@"None"
                       date:p.DateSent
                      media:photoItem];
      [[ChatService shared] setUpJSQMessage:jsqmessage fromQBChatMessage:p];
    } else {
      //// Is Message Not Photo Message   add to jsqmessagearray
      NSString *text = p.Text;
      if ([self IsEmpty:p.Text]) {
        text = @"";
      }

      jsqmessage = [[JSQMessage alloc]
           initWithSenderId:[NSString stringWithFormat:@"%@", @(p.SenderID)]
          senderDisplayName:@"None"
                       date:p.DateSent
                       text:text];
      [[ChatService shared] setUpJSQMessage:jsqmessage fromQBChatMessage:p];
    }
    [jsqmessagearray addObject:jsqmessage];
    //      (@"DBQBChatDialog-->%@",p);
  }

  if (![self
          IsEmpty:[[ChatService shared]
                          .chatroomJsqMessageArray objectForKey:dialog.ID]]) {
    NSMutableArray *jsqmessagsForDialogId =
        [[ChatService shared].chatroomJsqMessageArray objectForKey:dialog.ID];

    [jsqmessagearray addObjectsFromArray:jsqmessagsForDialogId];
    [[ChatService shared]
            .chatroomJsqMessageArray setObject:jsqmessagearray
                                        forKey:dialog.ID];
  }

  [[ChatService shared] reloadChatRoomCollectionViewAfterSecond:0];
}

- (void)syncMessagesForDialogId:(NSString *)dialogId {
  dispatch_async(
      dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // 耗时的操作
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager.imageDownloader setMaxConcurrentDownloads:2];

        [[SDImageCache sharedImageCache] setShouldDecompressImages:NO];
        [[SDWebImageDownloader sharedDownloader] setShouldDecompressImages:NO];
        NSMutableDictionary *extendedRequest =
            [[NSMutableDictionary alloc] init];
        DBResultSet *r = [[[[[DBQBChatMessage query]
            whereWithFormat:@" DialogID = %@ AND RecipientID =%@", dialogId,
                            [LoginRecordModel shared].myLoginRecord.QBID]
            orderByDescending:@"DateSent"] limit:1] fetch];
        if (r.count == 0) {
          return;
        }
        for (DBQBChatMessage *dbqbchatmessage in r) {
          extendedRequest[@"date_sent[gte]"] =
              @([dbqbchatmessage.DateSent timeIntervalSince1970] + 1);
        }

        extendedRequest[@"sort_asc"] = @"_id";

        QBResponsePage *page =
            [QBResponsePage responsePageWithLimit:100 skip:0];
        DDLogInfo(@"start get message from dialogid throug quickblox");
        [QBRequest messagesWithDialogID:dialogId
            extendedRequest:extendedRequest
            forPage:page
            successBlock:^(QBResponse *response, NSArray *messages,
                           QBResponsePage *page) {
              DDLogInfo(@"start get message from dialogid throug quickblox : "
                        @"sucessBlock "
                        @"excuting");

              if (messages.count > 0) {
                for (QBChatMessage *message in messages) {
                  //// check duplicate message
                  DBResultSet *r = [[[DBQBChatMessage query]
                      whereWithFormat:@" MessageID = %@ ", message.ID] fetch];
                  if (r.count == 0) {
                    DBQBChatMessage *dbhistorymessage = [DBQBChatMessage new];

                    dbhistorymessage.MessageID = message.ID;
                    dbhistorymessage.DialogID = dialogId;
                    dbhistorymessage.QBID =
                        [LoginRecordModel shared].myLoginRecord.QBID;
                    dbhistorymessage.eid =
                        [[LoginRecordModel shared].myLoginRecord.eid intValue];
                    dbhistorymessage.RecipientID = message.recipientID;
                    dbhistorymessage.SenderID = message.senderID;
                    dbhistorymessage.DateSent = message.dateSent;
                    dbhistorymessage.InsertDBDateTime = [NSDate date];
                    if ([self IsEmpty:[message.customParameters
                                          objectForKey:@"photourl"]]) {
                      dbhistorymessage.PhotoType = NO;
                    } else {
                      dbhistorymessage.PhotoType = YES;
                    }
                    dbhistorymessage.QBChatHistoryMessage =
                        [NSKeyedArchiver archivedDataWithRootObject:message];

                    dbhistorymessage.Text = message.text;
                    dbhistorymessage.CustomParameters =
                        message.customParameters;
                    dbhistorymessage.Attachments = message.attachments;
                    dbhistorymessage.NumberofDeliverStatus = 2;

                    dbhistorymessage.DidDeliverStatus = YES;
                    dbhistorymessage.ErrorStatus = NO;
                    dbhistorymessage.DrawStatus = NO;
                    dbhistorymessage.ReadStatus = NO;
                    dbhistorymessage.Retrycount = 0;
                    [dbhistorymessage commit];
                    if (![self IsEmpty:[message.customParameters
                                           objectForKey:@"photourl"]]) {
                      NSURL *url =
                          [NSURL URLWithString:[message.customParameters
                                                   objectForKey:@"photourl"]];
                      [manager downloadImageWithURL:url
                          options:0
                          progress:^(NSInteger receivedSize,
                                     NSInteger expectedSize) {
                            // progression tracking code
                          }
                          completed:^(UIImage *image, NSError *error,
                                      SDImageCacheType cacheType, BOOL finished,
                                      NSURL *imageURL) {
                            if (image) {
                              UIImageWriteToSavedPhotosAlbum(image, nil, nil,
                                                             nil);
                            }
                          }];
                    }
                  }
                }
              }
            }
            errorBlock:^(QBResponse *response) {
              DDLogError(@"QBResponse %@", response);
            }];
      });
  DDLogInfo(@"start get message from dialogid throug quickblox : exit ");
}

@end