//
//  AgentProfile.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//
#import <Foundation/Foundation.h>
#import "PressChatButtonModel.h"

#import "ChatRoom.h"
#import "HandleServerReturnString.h"
@implementation PressChatButtonModel

static PressChatButtonModel *sharedPressChatButtonModel;

+ (PressChatButtonModel *)shared {
  @synchronized(self) {
    if (!sharedPressChatButtonModel) {
      sharedPressChatButtonModel = [[PressChatButtonModel alloc] init];
      [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    }
    return sharedPressChatButtonModel;
  }
}

- (void)
pressChatButtonModelByQBID:(NSString *)QBID
                  username:(NSString *)username
                   success:(void (^)(Chatroom *chatRoom))success
                   failure:(void (^)(AFHTTPRequestOperation *operation,
                                     NSError *error, NSString *errorMessage,
                                     RequestErrorStatus errorStatus,
                                     NSString *alert, NSString *ok))failure {
  if (![self networkConnection]) {
    failure(nil, nil, @"Not NetWork Connection", NotRecordFind, @"Alert",
            @"OK");
  }

  if ([self IsEmpty:QBID]) {
    failure(nil, nil, @"Agent have not chat account.", 20, @"Alert", @"OK");
    UIAlertView *alert =
        [[UIAlertView alloc] initWithTitle:@"Alert"
                                   message:@"Agent have not chat account."
                                  delegate:nil
                         cancelButtonTitle:@"Ok"
                         otherButtonTitles:nil];

    [alert show];

    return;
  }

  if ([self IsEmpty:[LoginRecordModel shared].myLoginRecord.QBID]) {
    failure(nil, nil, @"This account have not chat account..", 21, @"Alert",
            @"OK");
    UIAlertView *alert = [[UIAlertView alloc]
            initWithTitle:@"Alert"
                  message:@"This account have not chat account."
                 delegate:nil
        cancelButtonTitle:@"Ok"
        otherButtonTitles:nil];
    [alert show];

    return;
  }
  QBChatDialog *chatDialog =
      [[QBChatDialog alloc] initWithDialogID:@"42444252"
                                        type:QBChatDialogTypePrivate];

  NSMutableArray *selectedUsersIDs = [NSMutableArray array];
  [selectedUsersIDs addObject:QBID];
  chatDialog.occupantIDs = selectedUsersIDs;

  Chatroom *vc = [Chatroom messagesViewController];
  vc.chatroomNameString = username;
  NSNumberFormatter *NumberFormatter = [[NSNumberFormatter alloc] init];
  NumberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
  vc.recipientID = [NumberFormatter numberFromString:QBID];

  NSMutableDictionary *extendedRequest = [[NSMutableDictionary alloc] init];
  DDLogDebug(@"username %@", username);
  DDLogDebug(
      @"occupants_ids[all] %@",
      [NSString stringWithFormat:@"%@,%@", QBID,
                                 [LoginRecordModel shared].myLoginRecord.QBID]);
  extendedRequest[@"occupants_ids[all]"] =
      [NSString stringWithFormat:@"%@,%@", QBID,
                                 [LoginRecordModel shared].myLoginRecord.QBID];

  [QBRequest countOfDialogsWithExtendedRequest:extendedRequest
      successBlock:^(QBResponse *response, NSUInteger count) {

        DDLogInfo(@"countOfDialogsWithExtendedRequestcount-->%lu",
                  (unsigned long)count);

        if (count >= 1) {
          //// enterchatroomfromchatbuttondialoghavetargetID case
          vc.checkHaveNotCountOfDialogsWithOccupants_IdsAllInDialogs = NO;
          [QBRequest createDialog:chatDialog
              successBlock:^(QBResponse *response,
                             QBChatDialog *createdDialog) {

                DDLogInfo(@"dialog@@@%@ StopStop", createdDialog);
                vc.dialog = createdDialog;
                success(vc);

              }
              errorBlock:^(QBResponse *response) {
                DDLogError(@"QBResponse %@", response);
                failure(nil, nil, @"Chat Server Not Response", 23, @"Alert",
                        @"OK");

              }];

        } else {
          vc.checkHaveNotCountOfDialogsWithOccupants_IdsAllInDialogs = YES;
          vc.dialog = chatDialog;
          success(vc);
        }

      }
      errorBlock:^(QBResponse *response) {
        DDLogError(@"QBResponse %@", response);
        failure(nil, nil, @"Chat Server Not Response", 23, @"Alert", @"OK");
      }];
}

@end