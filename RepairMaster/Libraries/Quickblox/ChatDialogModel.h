//
//  Photos.h
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

//#ifndef abc_Photos_h
//#define abc_Photos_h

//#endif
#import "BaseModel.h"

@interface ChatDialogModel : BaseModel
+ (ChatDialogModel *)shared;
typedef void (^CompletionBlock)();
#pragma mark requestDialogsWithCompletionBlock----------------------------------------------------------------------------------------------------
@property(nonatomic, strong) NSMutableArray *users;
@property(nonatomic, readonly) NSDictionary *usersAsDictionary;
@property(nonatomic, strong) NSMutableArray *dialogs;
@property(nonatomic, readonly) NSMutableDictionary *dialogsAsDictionary;
@property(copy) CompletionBlock getDialogsCompletionBlock;

- (void)requestDialogsWithCompletionBlock:(void (^)())completionBlock;
//// not use
- (void)requestDialogUpdateWithId:(NSString *)dialogId
                  completionBlock:(void (^)())completionBlock;

- (void)pressChatButtonModelByQBID:(NSString *)QBID
                          username:(NSString *)username
                           success:(void (^)(Chatroom *chatRoom))success
                           failure: (void (^)(AFHTTPRequestOperation *operation,
          NSError *error, NSString *errorMessage,
          RequestErrorStatus errorStatus,
          NSString *alert, NSString *ok))failure;

- (void)updateChatBadgeNumber;
@end

//@implementation Photos

//@end