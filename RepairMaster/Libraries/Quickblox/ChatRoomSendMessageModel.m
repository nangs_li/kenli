//
//  Photos.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ChatRoomSendMessageModel.h"

@implementation ChatRoomSendMessageModel

static ChatRoomSendMessageModel *sharedChatRoomSendMessageModel;

+ (ChatRoomSendMessageModel *)shared {
  @synchronized(self) {
    if (!sharedChatRoomSendMessageModel) {
      sharedChatRoomSendMessageModel = [[ChatRoomSendMessageModel alloc] init];
      [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    }
    return sharedChatRoomSendMessageModel;
  }
}

#pragma mark Send message-------------------------------------------------------------------------------------------------------------------
- (void)sendMessageInsertIntoDataBase:(NSString *)messageText
                            forDialog:(QBChatDialog *)dialog {
  // create a message
  QBChatMessage *message = [QBChatMessage markableMessage];

  message.text = messageText;
  NSMutableDictionary *params = [NSMutableDictionary dictionary];
  params[@"save_to_history"] = @"1";

  [message setCustomParameters:params];

  // 1-1 Chat

  // send message
  message.recipientID = [dialog recipientID];
  message.senderID = [QBSession currentSession].currentUser.ID;

  ////save message into DB
  DBQBChatMessage *dbhistorymessage = [DBQBChatMessage new];

  [dialog sendMessage:message
      completionBlock:^(NSError *error) {
        if (![self IsEmpty:error]) {
          DDLogError(@"sendMessageerror-->2%@", error);

          dbhistorymessage.NumberofDeliverStatus = 1;
          dbhistorymessage.ErrorStatus = YES;
          dbhistorymessage.SendToServerDate = [NSDate date];
        } else {
          dbhistorymessage.NumberofDeliverStatus = 2;
          dbhistorymessage.ErrorStatus = NO;
          dbhistorymessage.SendToServerDate = [NSDate date];
        }

        dbhistorymessage.MessageID = message.ID;
        dbhistorymessage.DialogID = dialog.ID;
        dbhistorymessage.QBID = [LoginRecordModel shared].myLoginRecord.QBID;
        dbhistorymessage.eid =
            [[LoginRecordModel shared].myLoginRecord.eid intValue];
        dbhistorymessage.RecipientID = [dialog recipientID];
        dbhistorymessage.SenderID = [QBSession currentSession].currentUser.ID;
        dbhistorymessage.DateSent = message.dateSent;
        dbhistorymessage.InsertDBDateTime = [NSDate date];
        dbhistorymessage.PhotoType = NO;
        dbhistorymessage.QBChatHistoryMessage =
            [NSKeyedArchiver archivedDataWithRootObject:[message copy]];

        dbhistorymessage.Text = messageText;
        dbhistorymessage.CustomParameters = message.customParameters;
        dbhistorymessage.Attachments = message.attachments;
        dbhistorymessage.DidDeliverStatus = NO;
        dbhistorymessage.DrawStatus = YES;
        dbhistorymessage.ReadStatus = NO;
        dbhistorymessage.Retrycount = 0;
        [dbhistorymessage commit];

        JSQMessage *jsqmessage = [[ChatService shared]
            addJSQMessageFromDBQBChatMessageToJSSQMessageArray:dbhistorymessage
                                                   forDialogId:dialog.ID];

      }];
}
- (void)sendCoverViewMessageInsertIntoDataBaseForDialog:(QBChatDialog *)dialog {
  // create a message
  QBChatMessage *message = [QBChatMessage markableMessage];

  message.text = @"agentlistingid";
  NSMutableDictionary *params = [NSMutableDictionary dictionary];
  params[@"save_to_history"] = @"1";

  [message setCustomParameters:params];

  // 1-1 Chat
  // send message
  message.recipientID = [dialog recipientID];
  message.senderID = [QBSession currentSession].currentUser.ID;

  ////save message into DB
  DBQBChatMessage *dbhistorymessage = [DBQBChatMessage new];
  [dialog sendMessage:message
      completionBlock:^(NSError *error) {
        if (![self IsEmpty:error]) {
          DDLogError(@"sendMessageerror-->2%@", error);

          dbhistorymessage.NumberofDeliverStatus = 1;
          dbhistorymessage.ErrorStatus = YES;
          dbhistorymessage.SendToServerDate = [NSDate date];
        } else {
          dbhistorymessage.NumberofDeliverStatus = 2;
          dbhistorymessage.ErrorStatus = NO;
          dbhistorymessage.SendToServerDate = [NSDate date];
        }

        dbhistorymessage.MessageID = message.ID;
        dbhistorymessage.DialogID = dialog.ID;
        dbhistorymessage.QBID = [LoginRecordModel shared].myLoginRecord.QBID;
        dbhistorymessage.eid =
            [[LoginRecordModel shared].myLoginRecord.eid intValue];
        dbhistorymessage.RecipientID = [dialog recipientID];
        dbhistorymessage.SenderID = [QBSession currentSession].currentUser.ID;
        dbhistorymessage.DateSent = message.dateSent;
        dbhistorymessage.InsertDBDateTime = [NSDate date];
        dbhistorymessage.PhotoType = NO;
        dbhistorymessage.QBChatHistoryMessage =
            [NSKeyedArchiver archivedDataWithRootObject:[message copy]];

        dbhistorymessage.Text = @"agentlistingid";
        dbhistorymessage.CustomParameters = message.customParameters;
        dbhistorymessage.Attachments = message.attachments;
        dbhistorymessage.DidDeliverStatus = NO;
        dbhistorymessage.DrawStatus = YES;
        dbhistorymessage.ReadStatus = NO;
        dbhistorymessage.Retrycount = 0;
        [dbhistorymessage commit];
        DDLogInfo(@"addMessagemessage-->%@", message);
        JSQMessage *jsqmessage = [[ChatService shared]
            addJSQMessageFromDBQBChatMessageToJSSQMessageArray:dbhistorymessage
                                                   forDialogId:dialog.ID];

      }];
}
#pragma mark Send Photo message-------------------------------------------------------------------------------------------------------------------
//// sendchatroomchosenImages
- (void)sendChatRoomChosenImagesForDialog:(QBChatDialog *)dialog {
  if ([self IsEmpty:self.chatroomchosenImages]) {
    return;
  } else {
    for (int i = 0; i < [self.chatroomchosenImages count]; i++) {
      if ([self.chatroomchosenImages[i]
              isEqual:[UIImage imageNamed:@"emptyphoto"]] ||
          [self.chatroomchosenImages[i]
              isEqual:[UIImage imageNamed:@"collectionviewaddbutton"]]) {
        DDLogDebug(@"if not equal to image");

      } else {
        //// resize image size<124000
        NSData *imageData =
            UIImageJPEGRepresentation(self.chatroomchosenImages[i], 1);
        int j = 100;
        //    DDLogInfo(@"imageData.length-->%lu", (unsigned
        //    long)imageData.length);
        while (imageData.length > 124000) {
          j = j - 5;
          imageData =
              UIImageJPEGRepresentation(self.chatroomchosenImages[i], j / 100);
          // max representation to 5% must break;
          if (6 > j) {
            break;
          }
        }
        DDLogDebug(@"imageData.length-->%lu", (unsigned long)imageData.length);

        QBChatMessage *qbchatmessage = [QBChatMessage markableMessage];
        qbchatmessage.text = [NSString stringWithFormat:@"%d", i];
        // here
        JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc]
            initWithImage:self.chatroomchosenImages[i]];

        if ([[LoginRecordModel shared].myLoginRecord.QBID integerValue] ==
            qbchatmessage.senderID) {
          photoItem.appliesMediaViewMaskAsOutgoing = YES;
        } else {
          photoItem.appliesMediaViewMaskAsOutgoing = NO;
        }
        //// show image to chatroom
        JSQMessage *jsqmessage = [[JSQMessage alloc]
             initWithSenderId:[NSString
                                  stringWithFormat:@"%@",
                                                   @(qbchatmessage.senderID)]
            senderDisplayName:@"None"
                         date:qbchatmessage.dateSent
                        media:photoItem];

        [[ChatService shared] addjsqPhotoMessageToJsqMessageArray:jsqmessage
                                                      forDialogId:dialog.ID];
        //// upload file
        [QBRequest TUploadFile:imageData
            fileName:@"photo.png"
            contentType:@"image/png"
            isPublic:YES
            successBlock:^(QBResponse *response, QBCBlob *uploadedBlob) {
              DDLogDebug(@"responsesuccess-->: %@", response);
              // Create chat message with attach

              NSURL *url = [NSURL URLWithString:uploadedBlob.publicUrl];
              [[ChatService shared]
                      .sdwebImagemanager downloadImageWithURL:url
                  options:0
                  progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                    // progression tracking code
                  }
                  completed:^(UIImage *image, NSError *error,
                              SDImageCacheType cacheType, BOOL finished,
                              NSURL *imageURL) {
                    if (image) {
                      photoItem.image = image;

                      UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
                    }
                  }];

              NSMutableDictionary *params = [NSMutableDictionary dictionary];
              params[@"save_to_history"] = @"1";

              params[@"photourl"] = uploadedBlob.publicUrl;
              [qbchatmessage setCustomParameters:params];
              [self sendMessageMethodWithPhoto:qbchatmessage
                                    jsqMessage:jsqmessage
                                     forDialog:dialog];
              [[ChatService shared] reloadChatRoomCollectionViewAfterSecond:0];
              if (![self IsEmpty:[ChatService shared].delegate]) {
                [[ChatService shared]
                        .delegate
                            chatRoomGetImageFromDBQBChatMessageToMWPhotoBrower];
              }

            }
            statusBlock:^(QBRequest *request, QBRequestStatus *status) {
              DDLogDebug(@"request-->: %@", request);
              //   DDLogInfo(@"status.percentOfCompletion-->: %f",
              //   status.percentOfCompletion);

            }
            errorBlock:^(QBResponse *response) {
              DDLogError(@"QBResponse %@", response);

            }];
      }
    }
    [[ChatService shared] reloadChatRoomCollectionViewAfterSecond:0];

    if (![self IsEmpty:[ChatService shared].delegate]) {
      [[ChatService shared]
              .delegate chatRoomCollectionViewscrollToBottomYesAnimated];
    }

    [JSQSystemSoundPlayer jsq_playMessageSentSound];
    DDLogInfo(@"scrollToBottomAnimated:YES");
    self.chatroomchosenImages = nil;
  }
}
//// send messgemothodwithphoto
- (void)sendMessageMethodWithPhoto:(QBChatMessage *)message
                        jsqMessage:(JSQMessage *)jsqmessage
                         forDialog:(QBChatDialog *)dialog {
  // create a message

  // 1-1 Chat
  // send message
  message.recipientID = [dialog recipientID];
  message.senderID = [QBSession currentSession].currentUser.ID;

  ////save message into DB
  DBQBChatMessage *dbhistorymessage = [DBQBChatMessage new];
  [dialog sendMessage:message
      completionBlock:^(NSError *error) {
        if (![self IsEmpty:error]) {
          DDLogError(@"sendMessageerror-->2%@", error);

          dbhistorymessage.NumberofDeliverStatus = 1;
          dbhistorymessage.ErrorStatus = YES;
          dbhistorymessage.SendToServerDate = [NSDate date];
        } else {
          dbhistorymessage.NumberofDeliverStatus = 2;
          dbhistorymessage.ErrorStatus = NO;
          dbhistorymessage.SendToServerDate = [NSDate date];
        }

        dbhistorymessage.MessageID = message.ID;
        dbhistorymessage.DialogID = dialog.ID;
        dbhistorymessage.QBID = [LoginRecordModel shared].myLoginRecord.QBID;
        dbhistorymessage.eid =
            [[LoginRecordModel shared].myLoginRecord.eid intValue];
        dbhistorymessage.RecipientID = [dialog recipientID];
        dbhistorymessage.SenderID = [QBSession currentSession].currentUser.ID;
        dbhistorymessage.DateSent = message.dateSent;
        dbhistorymessage.InsertDBDateTime = [NSDate date];
        dbhistorymessage.PhotoType = YES;
        dbhistorymessage.QBChatHistoryMessage =
            [NSKeyedArchiver archivedDataWithRootObject:[message copy]];

        message.text = @"image";
        dbhistorymessage.Text = message.text;
        dbhistorymessage.CustomParameters = message.customParameters;
        dbhistorymessage.Attachments = message.attachments;
        dbhistorymessage.DidDeliverStatus = NO;
        dbhistorymessage.DrawStatus = YES;
        dbhistorymessage.ReadStatus = NO;
        dbhistorymessage.Retrycount = 0;
        [dbhistorymessage commit];

        [[ChatService shared] setUpJSQMessage:jsqmessage
                            fromQBChatMessage:dbhistorymessage];
        DDLogInfo(@"addMessagemessage-->%@", message);
      }];
  // Reload table
}

@end