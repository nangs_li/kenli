//
//  Photos.m
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ChatDialogModel.h"
#import "DBQBChatDialog.h"
#import "DBQBUUser.h"
#import "GetChatMessageToJsqMessageArrayModel.h"
@implementation ChatDialogModel

static ChatDialogModel *sharedChatDialogModel;

+ (ChatDialogModel *)shared {
  @synchronized(self) {
    if (!sharedChatDialogModel) {
      sharedChatDialogModel = [[ChatDialogModel alloc] init];
      [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    }
    return sharedChatDialogModel;
  }
}

#pragma mark requestDialogsWithCompletionBlock----------------------------------------------------------------------------------------------------
- (void)requestDialogsWithCompletionBlock:(void (^)())completionBlock {
  self.getDialogsCompletionBlock = completionBlock;

  [QBRequest dialogsWithSuccessBlock:^(QBResponse *response,
                                       NSArray *dialogObjects,
                                       NSSet *dialogsUsersIDs) {

    if ([[AppDelegate getAppDelegate] IsEmpty:dialogObjects]) {
      if (self.getDialogsCompletionBlock != nil) {
        self.getDialogsCompletionBlock();
        self.getDialogsCompletionBlock = nil;
      }
    }

    self.dialogs = dialogObjects.mutableCopy;

    ////store QBChatDialog into local

    [[[[DBQBChatDialog query]
        whereWithFormat:@" eid = %@", [[LoginRecordModel shared]
                                              .myLoginRecord.eid stringValue]]
        fetch] removeAll];
    for (QBChatDialog *chatdialog in dialogObjects.mutableCopy) {
      DDLogInfo(@"chatDialog.recipientID : %ld", (long)chatdialog.recipientID);

      DBQBChatDialog *dbqbchatdialog = [DBQBChatDialog new];
      dbqbchatdialog.LastMessageDate = chatdialog.lastMessageDate;
      dbqbchatdialog.DialogID = chatdialog.ID;
      dbqbchatdialog.LastMessageText = chatdialog.lastMessageText;
      dbqbchatdialog.LastMessageUserID = chatdialog.lastMessageUserID;
      dbqbchatdialog.QBChatDialog =
          [NSKeyedArchiver archivedDataWithRootObject:[chatdialog copy]];
      dbqbchatdialog.eid =
          [[LoginRecordModel shared].myLoginRecord.eid intValue];

      dbqbchatdialog.QBID = [LoginRecordModel shared].myLoginRecord.QBID;
      [dbqbchatdialog commit];
      if (chatdialog.unreadMessagesCount > 0) {
        [[GetChatMessageToJsqMessageArrayModel shared]
            syncMessagesForDialogId:chatdialog.ID];
      }
    }
    QBGeneralResponsePage *page =
        [QBGeneralResponsePage responsePageWithCurrentPage:0 perPage:1000];
    //  page.perPage = 1000;
    [QBRequest usersWithIDs:[dialogsUsersIDs allObjects]
                       page:page
               successBlock:^(QBResponse *response, QBGeneralResponsePage *page,
                              NSArray *users) {
                 // call setusers

                 self.users = [users mutableCopy];

                 ////store DBQBUUser into local

                 [[[[DBQBUUser query]
                     whereWithFormat:@" eid = %@",
                                     [[LoginRecordModel shared]
                                             .myLoginRecord.eid stringValue]]
                     fetch] removeAll];

                 for (QBUUser *user in users) {
                   DBQBUUser *qbuuser = [DBQBUUser new];
                   qbuuser.InsertDataBaseDate = [NSDate date];
                   qbuuser.FullName = user.fullName;
                   qbuuser.CustomData = user.customData;
                   qbuuser.Login = user.login;
                   qbuuser.Password = user.password;
                   qbuuser.QBUUser =
                       [NSKeyedArchiver archivedDataWithRootObject:[user copy]];

                   qbuuser.eid =
                       [[LoginRecordModel shared].myLoginRecord.eid intValue];

                   qbuuser.QBID = [NSString
                       stringWithFormat:@"%li", (unsigned long)user.ID];
                   [qbuuser commit];
                 }

                 if (self.getDialogsCompletionBlock != nil) {
                   self.getDialogsCompletionBlock();
                   self.getDialogsCompletionBlock = nil;
                 }

               }
                 errorBlock:nil];

  }
                          errorBlock:nil];
}
- (void)requestDialogUpdateWithId:(NSString *)dialogId
                  completionBlock:(void (^)())completionBlock {
  self.getDialogsCompletionBlock = completionBlock;

  [QBRequest
       dialogsForPage:nil
      extendedRequest:@{
        @"_id" : dialogId
      }
         successBlock:^(QBResponse *response, NSArray *dialogObjects,
                        NSSet *dialogsUsersIDs, QBResponsePage *page) {

           BOOL found = NO;
           NSArray *dialogsCopy = [NSArray arrayWithArray:self.dialogs];
           for (QBChatDialog *dialog in dialogsCopy) {
             if ([dialog.ID isEqualToString:dialogId]) {
               [self.dialogs removeObject:dialog];
               found = YES;

               break;
             }
           }

           if (![self IsEmpty:dialogObjects.firstObject]) {
             [self.dialogs insertObject:dialogObjects.firstObject atIndex:0];
             [[[[DBQBChatDialog query]
                 whereWithFormat:@" DialogID = %@", dialogId] fetch] removeAll];

             QBChatDialog *serverreturnchatdialog = dialogObjects.firstObject;
             DBQBChatDialog *dbqbchatdialog = [DBQBChatDialog new];
             dbqbchatdialog.LastMessageDate =
                 serverreturnchatdialog.lastMessageDate;
             dbqbchatdialog.LastMessageUserID =
                 serverreturnchatdialog.lastMessageUserID;
             dbqbchatdialog.DialogID = serverreturnchatdialog.ID;
             dbqbchatdialog.QBChatDialog = [NSKeyedArchiver
                 archivedDataWithRootObject:[serverreturnchatdialog copy]];
             dbqbchatdialog.eid =
                 [[LoginRecordModel shared].myLoginRecord.eid intValue];

             dbqbchatdialog.QBID = [LoginRecordModel shared].myLoginRecord.QBID;
             [dbqbchatdialog commit];
             if (!found) {
               [QBRequest
                   usersWithIDs:[dialogsUsersIDs allObjects]
                           page:nil
                   successBlock:^(QBResponse *response,
                                  QBGeneralResponsePage *page, NSArray *users) {

                     [self.users addObjectsFromArray:users];
                     self.users = [self.users mutableCopy];
                     [[[[DBQBUUser query]
                         whereWithFormat:@" DialogID = %@", dialogId] fetch]
                         removeAll];

                     for (QBUUser *user in users) {
                       DBQBUUser *qbuuser = [DBQBUUser new];
                       qbuuser.InsertDataBaseDate = [NSDate date];
                       qbuuser.FullName = user.fullName;
                       qbuuser.CustomData = user.customData;
                       qbuuser.Login = user.login;
                       qbuuser.Password = user.password;
                       qbuuser.QBUUser = [NSKeyedArchiver
                           archivedDataWithRootObject:[user copy]];
                       qbuuser.DialogID = dialogId;
                       qbuuser.eid = [[LoginRecordModel shared]
                                          .myLoginRecord.eid intValue];

                       qbuuser.QBID = [NSString
                           stringWithFormat:@"%li", (unsigned long)user.ID];
                       [qbuuser commit];
                     }

                     if (self.getDialogsCompletionBlock != nil) {
                       self.getDialogsCompletionBlock();
                       self.getDialogsCompletionBlock = nil;
                     }

                   }
                     errorBlock:nil];
             } else {
               if (self.getDialogsCompletionBlock != nil) {
                 self.getDialogsCompletionBlock();
                 self.getDialogsCompletionBlock = nil;
               }
             }
           }
         }
           errorBlock:nil];
}
- (void)setUsers:(NSMutableArray *)users {
  _users = users;

  NSMutableDictionary *__usersAsDictionary = [NSMutableDictionary dictionary];
  for (QBUUser *user in users) {
    [__usersAsDictionary setObject:user forKey:@(user.ID)];
  }

  _usersAsDictionary = [__usersAsDictionary copy];
}
- (void)setDialogs:(NSMutableArray *)dialogs {
  _dialogs = dialogs;

  if (dialogs != nil && dialogs.count > 0) {
    NSMutableDictionary *__dialogsAsDictionary =
        [NSMutableDictionary dictionary];
    for (QBChatDialog *dialog in dialogs) {
      [__dialogsAsDictionary setObject:dialog forKey:dialog.ID];
    }

    _dialogsAsDictionary = [__dialogsAsDictionary mutableCopy];
  }
}
- (void)updateChatBadgeNumber {
  int number = 0;
  for (QBChatDialog *chatdialog in self.dialogs) {
    if (chatdialog.unreadMessagesCount > 0) {
      number = number + 1;

      //   DDLogInfo(@"DBQBChatDialog-->%@",p);
    }
  }

  if (number == 0) {
    [[[[[[AppDelegate getAppDelegate] getTabBarController] tabBar] items]
        objectAtIndex:2] setBadgeValue:nil];

  } else {
    [[[[[[AppDelegate getAppDelegate] getTabBarController] tabBar] items]
        objectAtIndex:2]
        setBadgeValue:[NSString stringWithFormat:@"%d", number]];
  }
}



@end