//
//  Photos.h
//  abc
//
//  Created by Li Nang Shing on 24/4/2015.
//  Copyright (c) 2015年 Ken All rights reserved.
//

//#ifndef abc_Photos_h
//#define abc_Photos_h

//#endif
#import "BaseModel.h"
#import "Chatroom.h"
@interface ChatRoomSendMessageModel : BaseModel
+ (ChatRoomSendMessageModel *)shared;
#pragma mark Send  message-------------------------------------------------------------------------------------------------------------------
- (void)sendMessageInsertIntoDataBase:(NSString *)messageText
                            forDialog:(QBChatDialog *)dialog;
- (void)sendMessage:(QBChatMessage *)message
    updateDBQBChatMessage:(DBQBChatMessage *)updatedbqbchatmessage
         updateJSQMessage:(JSQMessage *)updatejsqmessage
                 toDialog:(QBChatDialog *)dialog;

#pragma mark Send Photo message-------------------------------------------------------------------------------------------------------------------
@property(nonatomic, retain) NSMutableArray *chatroomchosenImages;
- (void)sendChatRoomChosenImagesForDialog:(QBChatDialog *)dialog;
- (void)sendMessageMethodWithPhoto:(QBChatMessage *)message
                        jsqMessage:(JSQMessage *)jsqmessage
                         forDialog:(QBChatDialog *)dialog;
@end
